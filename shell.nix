{
  system ? builtins.currentSystem
, pkgs ? import <nixpkgs> { inherit system; }
}:

with pkgs;
with import ./common.nix;

with pkgs.python37Packages;

(python37.withPackages (ps: dependencies ++ [ isort yapf mypy pylint pycodestyle pydocstyle ])).env

