from distutils.core import setup
from gmb.version import version

setup(
    name="GroupManagementBot",
    author="Edward Snowden",
    author_email="edwardsnowden@thevillage.chat",
    version=version,
    description='A description.',
    packages=["gmb", "gmb/api", "gmb/config", "gmb/database",
        "gmb/datastructs", "gmb/filter", "gmb/infofetchbot",
        "gmb/plugins", "gmb/pollingbot", "gmb/resource" ],
    scripts=['scripts/groupmanagementbot']
)
