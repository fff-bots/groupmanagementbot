DIA_C = dia
DIA_SRCS=$(shell find -type f -iname "*.dia")
DIA_OBJS=$(DIA_SRCS:.dia=.png)
PY_TESTS=$(shell find tests -name '__pycache__' -prune -o -type f -iname 'test*.py' -type f -print)

all: doc test check

test: $(PY_TESTS)
	python3 -m unittest $^

dependencies:
	sudo pip3 install $(shell cat requirements.txt)

dependencies_dev:
	sudo pip3 install $(shell cat requirements_dev.txt requirements.txt)

check:
	vulture --min-confidence 100 gmb
	mypy gmb
	pylint gmb

fix:
	isort -rc -p gmb gmb
	yapf -r -i gmb

%.png: %.dia
	$(DIA_C) $< -e $@ $(OUTPUT)

dia: $(DIA_OBJS)

codedoc:
	make -C doc/ html
	make -C doc/ latexpdf

doc: dia codedoc

clean:
	rm -Rf build dist GroupManagementBot.egg-info out/*
	find -name __pycache__ | xargs rm -Rf

release:
	git add gmb/version.py
	git commit -m "release "`./run --version | tail -n 1`
	git tag `./run --version | tail -n 1`
	git push
	git push --tags

release-again:
	git tag -f `./run --version | tail -n 1`
	git push
	git push -f --tags

.PHONY: dependencies dependencies_dev test check fix dia doc clean release release-again
