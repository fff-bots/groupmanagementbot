FROM debian:unstable

RUN apt-get update
RUN apt-get install -y python3 python3-pip
RUN apt-get install -y libmysqlclient-dev
RUN apt-get install -y vim

COPY ./requirements.txt /gmb/
RUN cd /gmb && pip3 install `cat requirements.txt`

WORKDIR /gmb
CMD ./run

VOLUME /gmb/config/ /gmb/out/ /gmb/status/

EXPOSE 5000

COPY . /gmb/
