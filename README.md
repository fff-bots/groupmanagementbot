# Groupmanagement Bot
This bot is to manage telegram group networks. The source code is available on the [the village gitlab](https://git.thevillage.chat/thevillage/groupmanagementbot) licensed under AGPLv3. A docker image is hostet in the [gitlab container registry](https://git.thevillage.chat/thevillage/groupmanagementbot/container_registry).

## Botfather Settings:
- disable group privacy
- allow groups
- enable inline mode

## Requirements:
- python3.7 with pip
- or docker

## Preconfigure
1. Install MySQL or MariaDB
2. Open MySQL
3. Create a new user
    - For example: gmbuser
    - [Tutorial](https://www.digitalocean.com/community/tutorials/how-to-create-a-new-user-and-grant-permissions-in-mysql)
4. Create a database
    - For example: gmb
    - `CREATE DATABASE gmb`
5. If you want to run the tests, create a second database
    - For example: gmb_test
    - `CREATE DATABASE gmb_test`

Notice:
- If your database is on a remote server, remember to allow [remote access on your database](https://stackoverflow.com/questions/16287559/mysql-adding-user-for-remote-access)

## Run without docker
1. Make sure you have all requirements
    - `pip install -r requirements.txt`
2. `mkdir config status out && cp config_template/config.json config/`
    - configure `config/config.json`
    - to get your telegram id you can use [@getuseridbot](https://t.me/getuseridbot)
3. `./run`
    - now add the bot to all chats as admin
    - write `/id` into all chats
    - add all chats with the command `/add_network` or `/add_partner`
    - stop the bot
5. `./run` or `./run_bg`
    - run the bot
    - `./run` will start the bot in foreground
    - `./run_bg` will start the bot in the background and echo the pid

## Run with Docker
`--network="host"` is only required if you run your MySQL server on the same machine as the docker container.

1. `docker run -it --rm --network="host" -v ~/gmb/config:/gmb/config -v ~/gmb/out:/gmb/out -v ~/gmb/status:/gmb/status registry.git.thevillage.chat/thevillage/groupmanagementbot`
    - creates the files in ~/gmb/config
    - then enter you db credentials in `db_config.json`
2. `docker run -it --rm --network="host" -v ~/gmb/config:/gmb/config -v ~/gmb/out:/gmb/out -v ~/gmb/status:/gmb/status registry.git.thevillage.chat/thevillage/groupmanagementbot ./run --config`
    - add your token
    - save and exit
3. `docker run -it --rm --network="host" -v ~/gmb/config:/gmb/config -v ~/gmb/out:/gmb/out -v ~/gmb/status:/gmb/status registry.git.thevillage.chat/thevillage/groupmanagementbot ./run --read-ids`
    - now add the bot to all chats as admin
    - write `/id` into all chats, that doesn't have a username, to get the ids
    - write down all chat-ids and chat-usernames you want to add
4. `docker run -it --rm --network="host" -v ~/gmb/config:/gmb/config -v ~/gmb/out:/gmb/out -v ~/gmb/status:/gmb/status registry.git.thevillage.chat/thevillage/groupmanagementbot ./run --config`
    - add all chats
    - optional: configure the rest
    - save and exit
5. `docker run -it --name gmb --network="host" -v ~/gmb/config:/gmb/config -v ~/gmb/out:/gmb/out -v ~/gmb/status:/gmb/status registry.git.thevillage.chat/thevillage/groupmanagementbot`
    - run the bot with a name
    - if it works stop it with ctrl+c
6. `docker start gmb`

## More Docker commands
### Update:
1. `docker pull registry.git.thevillage.chat/thevillage/groupmanagementbot`
2. `docker stop gmb`
3. `docker rm gmb`
4. `docker run -d --name gmb --network="host" -v ~/gmb/config:/gmb/config -v ~/gmb/out:/gmb/out -v ~/gmb/status:/gmb/status registry.git.thevillage.chat/thevillage/groupmanagementbot`

### Basic commands
- `docker stop gmb`
    - stops container
- `docker start gmb`
    - starts container
- `docker restart gmb`
    - restarts container
- `docker ps -a`
    - show status of all containers

### Manual docker
- `docker build -t registry.git.thevillage.chat/thevillage/groupmanagementbot .`
    - compile the docker image

### Show logs
- `docker logs gmb`
- `docker logs gmb -f`


# Contributing
1. Make a feature branch from development branch and there make your changes
2. Create a merge request to development branch
3. Optionally assign it to Edward Snowden
- If you add new files, that needs to be in the image, include them in .dockerignore
- If you add new telegram commands, add them to doc/telegram_commands.txt


# Making a Release (notices for Edward Snowden)
1. Create Merge Request from feature branch to development
2. Review changes
3. CI will start deploying the development branch in our testing environment
4. If unit tests run well and bot works, create new merge request from development to master
5. Increase the version number in gmb/version.py
6. Then run `make release`, that will commit the version change and tag this commit with the new version number and then pushes it
7. Merge the merge request
8. CI will start making a release and push it to docker hub
