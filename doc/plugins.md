# Plugins:
To write a plugin you need to add a folder into `gmb/plugins/`. Inside of the folder you need to create a file called `__init__.py`, where you import all your plugin files. For examples take a look at other plugins.

To make it easier to find the important methods and classes for plugin development, a module called `gmb.plugin_api` was added. Inside this module you have 2 modules: `gmb.plugin_api.database` for saving persistent data and `gmb.plugin_api.polling_bot_extension` for polling bot extensions.

## Database API
Plugins can generate database tables. For this you need to make a class that inherits from BaseModel and add one of the decorators to the class. BaseModel is an extended class from [peewee](http://docs.peewee-orm.com/en/latest/index.html)'s Model class.

There are 3 levels of database tables:
1. System config
2. Plugin config
3. Non persistent

They are created and deleted in this order. So if all plugin configs were cleared, all non persistent data would be cleared too. You can also refer to other tables, but make sure it is in the same or in a higher level. So you can make a table with non persistent level and add a dependency to a table with plugin config level, but not the other way around.

You have this decorators to create a table with no dependencies:
- `@register_system_config`
- `@register_plugin_config`
- `@register_non_persistent`

You can just add it before your class and the class will be automatically added to the database. Example:

```
from peewee import *
from gmb.plugin_api.database import BaseModel, register_plugin_config

@register_plugin_config
class LeaveChatConfig(BaseModel):
    leave_unknown_chats = BooleanField(null=False, default=True)
    leave_text = CharField(null=False, default="Ciao bella")
```

If your class depends on another class you can use these dependency decorators:
- `@register_system_config_with_dependencies(*otherclasses)`
- `@register_plugin_config_with_dependencies(*otherclasses)`
- `@register_non_persistent_with_dependencies(*otherclasses)`

This Example creates a table that depends von LeaveChatConfig:

```
from peewee import *
from gmb.plugin_api.database import BaseModel, register_plugin_config_with_dependencies

@register_plugin_config_with_dependencies(LeaveChatConfig)
class LeaveChatTemp(BaseModel):
    something = IntField(null=False, default=0)
    last_msg = CharField(null=False, default="")
    config = ForeignKeyField(LeaveChatConfig, null=True)
```

To see which datatypes you can use in the database, have a look at the [peewee docs](http://docs.peewee-orm.com/en/latest/peewee/models.html#models).


## PollingBot API

## Environment Resources

## Other
