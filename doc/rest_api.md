# Rest API
Currently supported methods are GET and DELETE. All database tables could be used as resources in the rest api. To see which database tables are available you can use the mysql command `show tables;`.

This are some examples, but they may be outdated in next releases:
- `admin`
- `chat`
- `chat_admin`
- `chatsconfig`
- `networkbannedusers`
- `partnerchat`
- `partnerchatadmin`
- `partnerchatsconfig`
- `partneruser`
- `user`

If errors occurs http error codes will be returned. Take a look at [http_codes.py](../gmb/rest_api/http_codes.py) to see all codes.

## Authentication:
Authentication method is basic auth. It is needed for the DELETE method and later for POST and PUT too. To add a user, use the configuration option `api_users`. Take a look at the [config templates for docker](../config_template/config.env) and the [json config template](../config_template/config.json).

## Options:
- To disable the API, add the `--without-api` parameter.
- Add `--flask-host` to change the Hostname and `--flask-port` to change the Port of the API.

## Methods:
- `{resource}` is the database table name
- `{id}` is the id of the resource
- `GET localhost/rest/{resource}`
    - Get all items of this resource
- `GET localhost/rest/{resource}/{id}`
    - Get the resource with this id
- `DELETE localhost/rest/{resource}/{id}`
    - Delete the resource with this id
    - Needs authentication
- `GET localhost/rest/{resource}/{id}/{subresource}`
    - Get the subresource of the resource with this id

## Examples:
- `curl localhost:5000/rest/chat/`
    - Get all network chats
- `curl localhost:5000/rest/chat/1234567890/chat_admins`
    - Get all chat admins of a chat
- `curl -u user1:passwd1 -X DELETE localhost:5000/rest/admin/123456789`
    - Removes the admin with the id 123456789
