import os
import sys
sys.path.insert(0, os.path.abspath("../../"))

# -- Project information -----------------------------------------------------

project = 'Groupmanagement Bot'
copyright = '2019, Edward Snowden <edwardsnowden@thevillage>'
author = 'Edward Snowden'


# -- General configuration ---------------------------------------------------

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx_autodoc_typehints',
]

templates_path = ['_templates']

exclude_patterns = []

language = 'de'


# -- Options for HTML output -------------------------------------------------

# html_theme = 'alabaster'
html_theme = 'classic'
html_theme_options = {
    'stickysidebar': 'True',
}

html_static_path = ['_static']


# -- AUTODOC -------------------------------------------------

latex_show_pagerefs = True
latex_show_urls = 'footnote'

latex_elements = {
    'printindex': r'\footnotesize\raggedright\printindex',
}

autoclass_content = 'both'

autodoc_default_options = {
    'member-order': 'bysource',
    'undoc-members': True,
    # 'private-members': True,
    'inherited-members': True,
    'imported-members': True,
    'show-inheritance': True,
}
