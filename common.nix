let
  pkgs = import <nixpkgs> { };
in 
with pkgs;
with pkgs.python37Packages;
let
  python-telegram-bot = buildPythonPackage rec {
    pname = "python-telegram-bot";
    version = "12.2.0";

    src = fetchPypi {
      inherit pname version;
      sha256 = "346d42771c2b23384c59f5f41e05bd7e801a0ce118d8dcb95209bb73d5f694c5";
    };

    propagatedBuildInputs = with pythonPackages; [ certifi cryptography future tornado ];

    doCheck = false;
  };

  dependencies = [ python-telegram-bot peewee mysqlclient ];
in {
  inherit dependencies python-telegram-bot;
}
