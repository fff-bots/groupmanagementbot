from __future__ import annotations

from enum import auto
from typing import Optional, List

from telegram import Bot, Chat, ChatMember, Update, User

from gmb.api.database import register_system_config
from gmb.api.datastructs import AbstractUser
from gmb.datastructs.factories import get_chat_member_info
from gmb.environment import Environment
from gmb.util import AutoInt, EnumField
from gmb.telegram_util import has_grouptype, GroupTypes, get_bot_and_update
from gmb.silent_mode import silent_and_not_private, silent_mode_fallback_reply
from gmb.custom_message.utils import send_tidy_error
from gmb.custom_message.tidy_mode import tidy


class Role(AutoInt):
    """All roles a user can have."""
    USER = auto()
    GROUP_ADMIN = auto()
    NETWORK_ADMIN = auto()
    BOT_ADMIN = auto()
    SUPERADMIN = auto()

    def increase(self: Role) -> Role:
        """Increases the permission level.

        Raises: ValueError if can't increase."""
        return Role(self.value + 1)

    def decrease(self: Role) -> Role:
        """Decreases the permission level.

        Raises: ValueError if can't decrease."""
        return Role(self.value - 1)


class PermissionLevel(AutoInt):
    """The equivalents of the roles that are saved in the database.

    Only contains the roles, that are saved in the database.
    """
    NETWORK_ADMIN = 0
    BOT_ADMIN = 1

    @staticmethod
    def get_lowest_level() -> PermissionLevel:
        """Returns the lowest permission level."""
        lowest: Optional[PermissionLevel] = None
        for perm in PermissionLevel:
            if lowest is None:
                lowest = perm
            elif perm.value < lowest.value:
                lowest = perm
        if lowest is None:
            raise Exception("PermissionLevel Enum is empty.")
        return lowest

    @staticmethod
    def get_highest_level() -> PermissionLevel:
        """Returns the highest permission level."""
        highest: Optional[PermissionLevel] = None
        for perm in PermissionLevel:
            if highest is None:
                highest = perm
            elif perm.value > highest.value:
                highest = perm
        if highest is None:
            raise Exception("PermissionLevel Enum is empty.")
        return highest

    @staticmethod
    def from_role(role: Role) -> PermissionLevel:
        """Converts a Role to a PermissionLevel."""
        if role == Role.NETWORK_ADMIN:
            return PermissionLevel.NETWORK_ADMIN
        if role == Role.BOT_ADMIN:
            return PermissionLevel.BOT_ADMIN
        raise ValueError("Role.{} is not a valid PermissionLevel.".format(role.name))

    def to_role(self) -> Role:
        """Converts a PermissionLevel to a Role."""
        if self == PermissionLevel.NETWORK_ADMIN:
            return Role.NETWORK_ADMIN
        if self == PermissionLevel.BOT_ADMIN:
            return Role.BOT_ADMIN
        raise ValueError("PermissionLevel.{} is not a valid Role.".format(self.name))

    def next_level(self: PermissionLevel) -> PermissionLevel:
        """Return the next permission level.

        Raises: ValueError if can't increase."""
        return PermissionLevel(self.value + 1)

    def previous_level(self: PermissionLevel) -> PermissionLevel:
        """Return the previous permission level.

        Raises: ValueError if can't decrease."""
        return PermissionLevel(self.value - 1)


@register_system_config
class Admin(AbstractUser):
    permission_level = EnumField(enum=PermissionLevel, null=False)

    def get_role(self) -> Role:
        """Returns the role."""
        return self.permission_level.to_role()

    def promote(self) -> bool:
        """Promotes the admin.

        If it returns false, than the admin has the highest permission level.
        """
        try:
            self.permission_level = self.permission_level.next_level()
        except ValueError:
            return False
        return True

    def demote(self):
        """Demotes the admin.

        If it returns false, than the admin already has the lowest permission level
        an admin can have. If you sill want to demote the user, you need to delete
        the admin object."""
        try:
            self.permission_level = self.permission_level.previous_level()
        except ValueError:
            return False
        return True

    @staticmethod
    def create_or_update_admin(bot: Bot, chat_id: int, user_id: int) -> Admin:
        """Creates an admin with lowest level."""
        item = Admin.try_get_by_id(user_id)
        user_info = get_chat_member_info(bot, chat_id, user_id)
        if item is None:
            perm_level = PermissionLevel.get_lowest_level()
            user = Admin.create(**user_info, permission_level=perm_level)
        else:
            perm_level = item.permission_level
            item.__data__ = {**user_info, "permission_level": perm_level}
        user.save()
        return user

    def has_permission_level(self, permission_level: PermissionLevel) -> bool:
        """Checks if the admin has the permission level or a higher one."""
        if permission_level <= self.permission_level:
            return True
        return False


def role_to_str(role: Role):
    return str(role).replace(Role.__name__ + ".", "")


def get_role(user: User, chat: Chat) -> Role:
    static_config = Environment.get().static_config
    if str(user.id) == str(static_config.master_admin_id):
        return Role.SUPERADMIN
    admin = Admin.try_get_by_id(user.id)
    if admin:
        return admin.get_role()
    if chat.all_members_are_administrators:
        return Role.GROUP_ADMIN
    if not has_grouptype(chat, GroupTypes.private):
        group_admins: List[ChatMember] = chat.get_administrators()
        for group_admin in group_admins:
            if group_admin.user == user:
                return Role.GROUP_ADMIN
    return Role.USER


def has_role(user: User, chat: Chat, role: Role) -> bool:
    return get_role(user, chat) >= role


def update_get_role(update: Update):
    try:
        return get_role(update.message.from_user, update.message.chat)
    except AttributeError:
        return None


def update_has_role(update: Update, role: Role):
    try:
        return has_role(update.message.from_user, update.message.chat, role)
    except AttributeError:
        return False


def check_role_callable_fallback(bot, update, role):
    """Sends an error message to the user that he doesn't have the right rights."""
    if not tidy() and silent_and_not_private(update):
        # In nicht privaten chats soll keine Antwort kommen
        silent_mode_fallback_reply(bot, update)
        return
    text = "Du hast keine Berechtigung. Du brauchst {}, hast aber nur {}!"
    my_role = update_get_role(update)
    text = text.format(role_to_str(role), role_to_str(my_role))
    if not role:
        text = "Du hast keine Rolle?"
    send_tidy_error(text, bot, update)


def check_role_callable(func, role: Role):
    def intern(*args, **kwargs):
        (bot, update) = get_bot_and_update(*args, **kwargs)
        if update_has_role(update, role):
            return func(*args, **kwargs)
        return check_role_callable_fallback(bot, update, role)

    return intern


def only_botadmins(func):
    return check_role_callable(func, Role.BOT_ADMIN)


def only_networkadmins(func):
    return check_role_callable(func, Role.NETWORK_ADMIN)


def only_superadmins(func):
    return check_role_callable(func, Role.SUPERADMIN)


def only_groupadmins(func):
    return check_role_callable(func, Role.GROUP_ADMIN)
