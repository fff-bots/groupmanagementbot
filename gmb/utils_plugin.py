from typing import List
import importlib
import os

from gmb.environment import Environment

class PluginLoader:
    def __init__(self, filename: str):
        dir_path = os.path.dirname(os.path.realpath(filename))
        self.plugins = os.listdir(dir_path)
        # get import path
        dir_name = os.path.basename(dir_path)
        dir_names: List[str] = [dir_name]
        while dir_name != "gmb":
            dir_path = os.path.dirname(dir_path)
            dir_name = os.path.basename(dir_path)
            dir_names.insert(0, dir_name)
        self.import_path: str = ".".join(dir_names)
        self.dir_names: List[str] = dir_names

    def import_plugins(self):
        # blacklist
        blacklist = Environment.get().static_config.plugin_blacklist.split(",")
        plugins = self.plugins
        for blacklisted in blacklist:
            if blacklisted in plugins:
                plugins.remove(blacklisted)
        for blacklisted in ["__pycache__", "__init__.py", ".mypy_cache"]:
            if blacklisted in plugins:
                plugins.remove(blacklisted)

        if len(self.dir_names) == 2:
            print("Loading Plugins:\n  {}".format(", ".join(plugins)))
            print("Disabled Plugins:\n  {}".format(", ".join(blacklist)))
        else:
            print("Loading Sub-Plugins:\n  {}".format(", ".join(plugins)))
            print("Disabled Sub-Plugins:\n  {}".format(", ".join(blacklist)))

        for plugin in plugins:
            importlib.import_module("{}.{}".format(self.import_path, plugin))
