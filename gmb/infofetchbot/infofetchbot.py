from __future__ import annotations

import datetime
import time
from threading import Event

from telegram import Bot

from gmb.datastructs.infofetchbot_config import InfoFetchConfig

from .extensions import InfoFetchExtensions


class InfoFetch:
    """Telegram Bot connection, that gets some information about chats."""

    def __init__(self: InfoFetch, bot: Bot):
        self.bot = bot
        self.exit = Event()

    def run(self: InfoFetch) -> None:
        while not self.exit.is_set():
            infofetch_config = InfoFetchConfig.get_entry()
            now = datetime.datetime.now()
            print("Start: InfoFetch ({})".format(now))
            for extension in InfoFetchExtensions.get_extensions():
                now2 = datetime.datetime.now()
                print("Start InfoFetchExt: {} ({})".format(
                    extension.__name__, now2))
                now2 = datetime.datetime.now()
                print("Finished InfoFetchExt: {} ({})".format(
                    extension.__name__, now2))
                extension.run(self.bot)
            now = datetime.datetime.now()
            print("Finished: InfoFetch ({})".format(now))
            print("Sleeping {} seconds".format(infofetch_config.update_time))
            self.exit.wait(infofetch_config.update_time)
        print("Stopped Infofetch")
