from __future__ import annotations

from telegram import Bot


class InfoFetchExt:
    @staticmethod
    def run(bot: Bot) -> None:
        raise NotImplementedError
