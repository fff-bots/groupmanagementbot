"""Methods to convert chats or users into there string representation."""

from html import escape
from typing import Optional, Union

from telegram import Chat, User

from gmb.api.datastructs import AbstractChat, AbstractUser


def list_char() -> str:
    """Returns the char that is used for lists."""
    return "•"


def __separation_char() -> str:
    return "➖"


def __separation_char_count() -> int:
    return 11


def separation_line() -> str:
    """Returns the string that is used as a separation line."""
    return __separation_char_count() * __separation_char()


def escaped_format(string: str, *items) -> str:
    """Performs the format function on the string with the given items and
    html-escapes the string of the items before."""
    new_items = []
    for item in items:
        new_items.append(escape(str(item)))
    return string.format(*new_items)


def __usr_to_str(uid: int, name: str, username: Optional[str] = None) -> str:
    msg = ""
    invite_link = "tg://user?id={}".format(uid)
    if username:
        tmp = '<a href="{}">{}</a> (@{})'
        msg = escaped_format(tmp, invite_link, name, username)
    else:
        tmp = '<a href="{}">{}</a>'
        msg = escaped_format(tmp, invite_link, name)
    return msg


def usr_to_str(obj: Union[User, AbstractUser, int],
               name: Optional[str] = None,
               username: Optional[str] = None) -> str:
    """Converts an user into a string."""
    if isinstance(obj, User):
        return usr_to_str(obj.id, obj.full_name)
    if isinstance(obj, AbstractUser):
        return __usr_to_str(obj.id, obj.first_name, obj.username)
    if name is None:
        raise TypeError(
            "usr_to_str() missing 1 required positional argument: 'name'")
    return __usr_to_str(obj, name, username)


def __chat_to_str(name: str,
                  username: Optional[str] = None,
                  invite_link: Optional[str] = None) -> str:
    msg = ""
    if invite_link:
        if username:
            tmp = '<a href="{}">{}</a> (@{})'
            msg = escaped_format(tmp, invite_link, name, username)
        else:
            tmp = '<a href="{}">{}</a>'
            msg = escaped_format(tmp, invite_link, name)
    else:
        if username:
            tmp = "{} (@{})"
            msg = escaped_format(tmp, name, username)
        else:
            tmp = "{}"
            msg = escaped_format(tmp, name)
    return msg


def chat_to_str(obj: Union[Chat, AbstractChat, int],
                name: Optional[str] = None,
                username: Optional[str] = None,
                invite_link: Optional[str] = None) -> str:
    """Converts a chat into a string."""
    if isinstance(obj, Chat):
        return __chat_to_str(obj.title)
    if isinstance(obj, AbstractChat):
        return __chat_to_str(obj.title, obj.username, obj.invite_link)
    if name is None:
        raise TypeError(
            "chat_to_str() missing 1 required positional argument: 'name'")
    return __chat_to_str(name, username, invite_link)
