from typing import Any, List, Type, Union, Tuple, Optional

from enum import Enum
from dataclasses import dataclass

import telegram
from telegram import Bot, Update, Message, Chat
from telegram.ext import Job

from gmb.api.database import BaseModel, register_system_config
from gmb.api.datastructs import AbstractChat
from gmb.environment import Environment


class GroupTypes(Enum):
    """Contains all group types."""
    private = "private"
    group = "group"
    supergroup = "supergroup"
    channel = "channel"


class ChatMemberStatus(Enum):
    """Contains all status of chatmembers."""
    CREATOR = "creator"
    ADMINISTRATOR = "administrator"
    MEMBER = "member"
    RESTRICTED = "restricted"
    LEFT = "left"
    KICKED = "kicked"


class MessageLifetime(Enum):
    """The lifetime of a message.

    Short is for error messages.
    Middle is for normal messages.
    """
    SHORT = 30
    MIDDLE = 5 * 60
    LONG = 20 * 60


def get_bot_and_update(*args, **kwargs) -> Tuple[Bot, Update]:
    """Return the bot and the update object from args and kwargs."""
    if "bot" in kwargs.keys():
        bot = kwargs["bot"]
    else:
        bot = None
    if "update" in kwargs.keys():
        update = kwargs["update"]
    else:
        update = None
    if bot is None and len(args) > 1:
        for i in range(2):
            if isinstance(args[i], Bot):
                bot = args[i]
                break
    if update is None and len(args) > 1:
        for i in range(3):
            if isinstance(args[i], Update):
                update = args[i]
                break
    return (bot, update)


def update_has_grouptype(update: Update, grouptype: GroupTypes) -> bool:
    """Returns true if the chat of the message in the update has the given group type."""
    if not update.effective_message:
        return False
    message = update.effective_message
    return message_has_grouptype(message, grouptype)


def message_has_grouptype(message: Message, grouptype: GroupTypes) -> bool:
    """Returns true if the chat of the message has the given group type."""
    if not message.chat:
        return False
    chat = message.chat
    return has_grouptype(chat, grouptype)


def chat_has_grouptype(chat: Chat, grouptype: GroupTypes) -> bool:
    """Returns true if the chat has the given group type."""
    return chat.type == str(grouptype.value)


def has_grouptype(obj: Union[Update, Message, Chat], grouptype: GroupTypes) -> bool:
    """Returns true if the group has the given group type.

    You can pass an update, a message or a chat."""
    if isinstance(obj, Update):
        return update_has_grouptype(obj, grouptype)
    if isinstance(obj, Message):
        return message_has_grouptype(obj, grouptype)
    if isinstance(obj, Chat):
        return chat_has_grouptype(obj, grouptype)
    return False


def __only_group_type(func, grouptype: GroupTypes):

    def intern(*args, **kwargs):
        (_bot, update) = get_bot_and_update(*args, **kwargs)
        if has_grouptype(update, grouptype):
            return func(*args, **kwargs)
        return None

    return intern


def is_private_group(obj: Union[Update, Message, Chat]):
    """Returns true, if the group type is private."""
    return has_grouptype(obj, GroupTypes.private)


def only_private_groups(func):
    """The command is only executed in private chats."""
    return __only_group_type(func, GroupTypes.private)


def text_without_command(update: Update):
    return " ".join(update.effective_message.text.split(" ")[1:])


def reset_config_member(cls: Type[BaseModel], member: str) -> Any:
    entry = cls.get_entry()
    entry.__data__[member] = cls.__dict__[member].field.default
    entry.save()
    return entry.__data__[member]


def set_config_member(cls: Type[BaseModel],
                      member: str,
                      update: Update,
                      type: Union[Type, None] = None) -> Any:
    """Returns the new value on success or None on error."""
    new_val = text_without_command(update)
    if type:
        try:
            new_val = type(new_val)
        except Exception:
            return None
    entry = cls.get_entry()
    entry.__data__[member] = new_val
    entry.save()
    return new_val


def build_menu(buttons: List[Any],
               n_cols: int,
               header_buttons: List[Any] = None,
               footer_buttons: List[Any] = None) -> List[List[Any]]:
    """Builds an menu.

    A button can be for example a InlineKeyboardButton. It ins/set_start_message husoerts the header_button at
    position 0 and appends the footer_button. It creates a list with lists and the inner
    lists have a lenth <= n_cols. That makes it possible to create a telegram menu just
    by passing in a list of buttons and a width.

    Args:
        buttons: The buttons added to the menu.
        n_cols: The maximum amount of buttons that are next to each other.
        header_buttons: Buttons that are inserted before the other buttons.
        footer_buttons: Buttons that are appended after all buttons.

    Returns:
        List[List[Any]]: The Menu.
    """
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, header_buttons)
    if footer_buttons:
        menu.append(footer_buttons)
    return menu


def replace_text(update: Update, text: str) -> str:
    """Replaces variables in the text.

    Replaced vars:
    chat_id => update.effective_chat.id
    chat_name => update.effective_chat.title or if empty
                 update.effective_chat.username or if empty
                 "unknown"

    Args:
        update: An update object that contains a message. This is used to replace some
            parts of the text with variables of this message.
        text: The text that should be changed.

    Returns:
        str: A text with some replaced variables.
    """
    chat = update.effective_chat
    if chat.title is not None:
        chat_name = chat.title
    elif chat.username is not None:
        chat_name = chat.username
    else:
        chat_name = "unknown"
    text = text.format(
        chat_id=chat.id,
        chat_name=chat_name,
    )
    return text


@dataclass
class DeleteMessageContext:
    """Parameters used by __delete_message Method."""
    message: Message
    delete_replied_to: bool = True


def __delete_message(_bot: Bot, job: Job):
    dmp = job.context
    msg = dmp.message
    delete_replied_to = dmp.delete_replied_to
    msg.delete()
    if delete_replied_to and msg.reply_to_message:
        msg.reply_to_message.delete()


def delete_timered(message: Message, time: int, delete_replied_to: bool = True):
    """Deletes a message after a time.

    If delete_replied_to is True (default), it will also delete the command, that has
    triggered this method.
    """
    print("Deleting msg in {} seconds.".format(time))
    update = Environment.get().get_updater()
    j = update.job_queue
    context = DeleteMessageContext(message, delete_replied_to)
    j.run_once(__delete_message, time, context=context)


def create_message_link(obj: Union[Chat, AbstractChat, int], message_id: int) -> Optional[str]:
    """Creates a t.me/c/ link to the message.

    Parameters:
        obj: A Chat or AbstractChat object or the id of the chat
        message_id: The id of the message
    """
    if isinstance(obj, (Chat, AbstractChat)):
        chat_id = str(obj.id)
    else:
        chat_id = str(obj)
    if chat_id.startswith("-100"):
        chat_id = chat_id[4:]
        return "https://t.me/c/{}/{}".format(chat_id, message_id)
    return None
