from __future__ import annotations

from typing import List, Type

from telegram.ext import Dispatcher

from gmb.abstract_extensions import Extensions

from .extension import HandlerPriority, PollBotExt, PriorityHandler
from .extensions import PollingBotExtensions


class PollingBot:
    """Bot for polling messages and interacting with them.

    This is the bot that interacts with the chats and its messages. It can with extended
    by PollBotExt classes that contains all functionalities of this bot. For example
    it uses the SmallCommandExt-Extension that contains handlers for /help and /license.
    If you would like to extend this bot, create a new class that implements PollBotExt
    and add it to the extensions list."""

    def add_handlers(self: PollingBot, dispatcher: Dispatcher) -> None:
        """Adds all handlers to the PollingBot.

        This method iterates over all extensions and adds there handlers.

        Args:
            dispatcher: The dispatcher of the bot.
        """
        print("Adding Handlers")
        prio_handlers: List[PriorityHandler] = []
        for ext in PollingBotExtensions.get_extensions():
            prio_handlers.extend(ext().get_handlers())
        prio_handlers = sorted(prio_handlers,
                               key=lambda prio_handler: prio_handler.priority)
        for prio_handler in prio_handlers:
            dispatcher.add_handler(prio_handler.handler,
                                   group=prio_handler.group)
