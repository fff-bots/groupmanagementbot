from __future__ import annotations

from typing import Type

from gmb.abstract_extensions import Extensions

from .extension import PollBotExt


class PollingBotExtensions(Extensions):
    pass


def register_pollingbot_extension(cls: Type[PollBotExt]) -> Type[PollBotExt]:
    print("Registered pollingbot extension: {}".format(cls.__name__))
    PollingBotExtensions.add_extension(cls)
    return cls
