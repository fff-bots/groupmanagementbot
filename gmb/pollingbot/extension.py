from __future__ import annotations

from dataclasses import dataclass
from enum import auto
from typing import List

from telegram.ext import Handler

from gmb.util import AutoInt


class HandlerPriority(AutoInt):
    """The priority of a handler."""
    VERY_HIGH = auto()
    HIGH = auto()
    NORMAL = auto()
    LOW = auto()
    VERY_LOW = auto()


@dataclass
class PriorityHandler:
    """A handler combined with a priority."""
    handler: Handler
    priority: HandlerPriority
    group: int = 0


class PollBotExt:
    """An interface that needs to be implemented by the PollBot extensions.

    The PollBot can be extended by classes that implements this interface.
    """

    def get_handlers(self: PollBotExt) -> List[PriorityHandler]:
        """Returns a list of PriorityHandlers that are added to the bot."""
        raise NotImplementedError("Should have implemented this")
