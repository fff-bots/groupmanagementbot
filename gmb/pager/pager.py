from __future__ import annotations
from typing import List, Optional, Dict, Any, Union
from functools import wraps
from telegram import Bot, Update, Message, Chat, InlineKeyboardButton, InlineKeyboardMarkup, CallbackQuery

from gmb.telegram_util import replace_text

from .paged_text import AbstractPagedText, CharLengthPagedText


def check_callback_data_len(callback_data: str):
    max_bytes = 64
    if len(bytes(str(callback_data), "utf-8")) > max_bytes:
        return False
    return True


class Pager():
    """Sends a paged text to telegram and controls it."""
    __pagers: Dict[int, 'Pager'] = {}
    __next_id = 0
    __cb_prefix = "pager"

    @staticmethod
    def get_pager(pager_id: int) -> Pager:
        """Returns the pager with the given id."""
        return Pager.__pagers[pager_id]

    @staticmethod
    def get_pager_by_update(update: Update) -> Pager:
        """Returns the pager related to the callback query from the update."""
        query: CallbackQuery = update.callback_query
        cb_data = query.data
        pager_id = int(cb_data.split(";")[1])
        return Pager.get_pager(pager_id)

    def __init__(self, paged_text: AbstractPagedText):
        self.paged_text = paged_text
        self.pager_id = Pager.__next_id
        Pager.__next_id += 1
        self.current_page_index = 0
        self.chat_id: Optional[int] = None
        self.message_id: Optional[int] = None
        Pager.__pagers[self.pager_id] = self
        self.extra: Any = None

    def add_send_message(self, message: Message, **extra: Any) -> None:
        """Adds the already send message and the extras to the pager."""
        self.message_id = message.message_id
        self.chat_id = message.chat.id
        self.extra = extra

    def update_message(self, bot: Bot, update: Update, page_index: int) -> None:
        """Updates the pager."""
        msg = update.effective_message
        keyboard = self.gen_keyboard(page_index)
        text = self.paged_text[page_index]
        text = replace_text(update, text)
        self.extra.pop("text", None)
        self.extra.pop("reply_markup", None)
        self.extra.pop("chat_id", None)
        msg.edit_text(text=text, reply_markup=keyboard, **self.extra)
        self.current_page_index = page_index

    def prev_page(self, bot: Bot, update: Update):
        """Updates the pager to the previous page."""
        page_index = self.current_page_index - 1
        if page_index < 0:
            query: CallbackQuery = update.callback_query
            query.answer("There is no previous page.")
            return
        self.update_message(bot, update, page_index)

    def next_page(self, bot: Bot, update: Update):
        """Updates the pager to the next page."""
        page_index = self.current_page_index + 1
        if page_index >= len(self.paged_text):
            query: CallbackQuery = update.callback_query
            query.answer("There is no next page.")
            return
        self.update_message(bot, update, page_index)

    def current_page(self, bot: Bot, update: Update):
        """Updates the pager to the current page."""
        query: CallbackQuery = update.callback_query
        page_id = self.current_page_index
        query.answer("This is page {}".format(page_id + 1))

    def gen_keyboard(self, page_index: int):
        """Generates a keyboard for the specified page."""
        prefix = "{};{}".format(self.__cb_prefix, self.pager_id)
        btns = []
        # prev
        prev_index = "{};prev_page".format(prefix)
        btns.append(InlineKeyboardButton("<", callback_data=str(prev_index)))
        # current
        current_page = page_index + 1
        current_index = "{};current_page".format(prefix)
        btns.append(InlineKeyboardButton("Page {}".format(current_page),
                                         callback_data=str(current_index)))
        # next
        next_index = "{};next_page".format(prefix)
        btns.append(InlineKeyboardButton(">", callback_data=str(next_index)))
        markup = InlineKeyboardMarkup([btns])
        return markup
