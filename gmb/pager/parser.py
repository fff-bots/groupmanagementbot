from __future__ import annotations

from enum import Enum, auto
from typing import Tuple, List, Union, Optional
from dataclasses import dataclass


class ItemType(Enum):
    BOLD = auto()
    ITALIC = auto()
    CODE = auto()
    PRE = auto()
    URL = auto()
    SONDERZEICHEN = auto()


@dataclass
class Sonderzeichen:
    zeichen: str
    length: int


sonderzeichen = [
    Sonderzeichen("&lt;", 1),
    Sonderzeichen("&gt;", 1),
    Sonderzeichen("&amp;", 1),
    Sonderzeichen("&quot;", 1)
]


def find_first_sonderzeichen(string: str) -> Optional[Tuple[Sonderzeichen, int]]:
    first_zeichen = None
    first_pos = None
    for zeichen in sonderzeichen:
        pos = string.find(zeichen.zeichen)
        if pos == -1:
            continue
        if first_pos is None or pos < first_pos:
            first_pos = pos
            first_zeichen = zeichen
    if first_pos is None:
        return None
    return (first_zeichen, first_pos)


item_types_tags = {
    ItemType.BOLD: ["b", "strong"],
    ItemType.ITALIC: ["i", "em"],
    ItemType.CODE: ["code"],
    ItemType.PRE: ["pre"],
    ItemType.URL: ["a"],
}


class IParserItem:
    """An element of the html structure."""
    def __len__(self):
        raise NotImplementedError()

    def __str__(self):
        raise NotImplementedError()

    def __getitem__(self, sliced: Union[int, slice]) -> IParserItem:
        raise NotImplementedError()


class ParserCollection(IParserItem):
    """A collection of IParserItems."""

    def __init__(self, items: Union[List[IParserItem], ParserCollection]):
        if isinstance(items, list):
            self.items = items.copy()
        elif isinstance(items, ParserCollection):
            self.items = items.items.copy()
        else:
            TypeError("items must be List[IParserItem] or ParserCollection")

    def __len__(self) -> int:
        return sum(len(item) for item in self.items)

    def __str__(self) -> str:
        out = ""
        for item in self.items:
            out += str(item)
        return out

    def append(self, item: IParserItem):
        """Appends an item to the list."""
        self.items.append(item)

    def extend(self, collection: Union[List[IParserItem], ParserCollection]):
        """Extends the collection."""
        if isinstance(collection, ParserCollection):
            self.items.extend(collection.items)
        if isinstance(collection, list):
            self.items.extend(collection)
        else:
            raise TypeError("collection mus be List[IParserItem] or ParserCollection")

    def __iadd__(self, second: ParserCollection) -> ParserCollection:
        self.extend(second)

    def __getitem__(self, sliced: Union[int, slice]) -> ParserCollection:
        if isinstance(sliced, int):
            pos = 0
            for item in self.items:
                if pos + len(item) > sliced:
                    return item[sliced - pos]
                pos += len(item)
            raise IndexError("index out of range")
        if isinstance(sliced, slice):
            start = sliced.start if sliced.start is not None else 0
            stop = sliced.stop if sliced.stop is not None else len(self)
            if start < 0 or stop < 0:
                raise NotImplementedError()
            if sliced.step is not None and sliced.step != 1:
                raise NotImplementedError()
            pos = 0
            liste = []
            started = False
            for item in self.items:
                if started:
                    liste.append(item[0: stop - pos])
                elif pos + len(item) > start:
                    liste.append(item[start - pos: stop - pos])
                    started = True
                pos += len(item)
                if pos > stop:
                    return ParserCollection(liste)
            return ParserCollection(liste)
        raise TypeError("indices must be integers or slices")


class ParserString(IParserItem):
    def __init__(self, string: str):
        self.string = string

    def __len__(self):
        return len(self.string)

    def __str__(self):
        return self.string

    def __getitem__(self, sliced: Union[int, slice]) -> ParserString:
        return ParserString(self.string[sliced])


class ParserSonderzeichen(IParserItem):
    def __init__(self, zeichen: Sonderzeichen):
        self.string = zeichen.zeichen
        self.length = zeichen.length

    def __len__(self):
        return self.length

    def __str__(self):
        return self.string

    def __getitem__(self, sliced: Union[int, slice]) -> ParserSonderzeichen:
        if isinstance(sliced, int):
            if sliced >= len(self):
                raise IndexError("index out of range")
            return self
        if isinstance(sliced, slice):
            if sliced.start == 0:
                return self
            return ParserSonderzeichen(Sonderzeichen("", 0))
        raise TypeError("indices must be integers or slices")


class ParserSonderzeichenString(ParserCollection):

    def __init__(self, string: str):
        super().__init__([])
        while True:
            tmp = find_first_sonderzeichen(string)
            if tmp is None:
                self.append(ParserString(string))
                break
            zeichen, pos = tmp
            pre_string = string[:pos]
            self.append(ParserString(pre_string))
            self.append(ParserSonderzeichen(zeichen))
            string = string[pos + len(zeichen.zeichen):]


class ParserTag(ParserCollection):
    def __init__(self, string: str, item_type: ItemType):
        if isinstance(string, str):
            super().__init__(ParserSonderzeichenString(string))
        elif isinstance(string, ParserCollection):
            super().__init__(string)
        self.item_type = item_type

    def __str__(self):
        tag = item_types_tags[self.item_type][0]
        return '<' + '{0}>{1}</{0}>'.format(tag, super().__str__())

    def __getitem__(self, sliced: Union[int, slice]) -> ParserTag:
        return ParserTag(super().__getitem__(sliced), self.item_type)


class ParserUrl(ParserCollection):
    def __init__(self, string: str, url: str):
        if isinstance(string, str):
            super().__init__(ParserSonderzeichenString(string))
        elif isinstance(string, ParserCollection):
            super().__init__(string)
        self.url = url

    def __str__(self):
        return '<a href="{}">{}</a>'.format(self.url, super().__str__())

    def __getitem__(self, sliced: Union[int, slice]) -> ParserTag:
        return ParserUrl(super().__getitem__(sliced), self.url)


@dataclass
class HTMLElement:
    tag: str
    attribute: Tuple[str, str]
    content: str
    start: int
    end: int


class HTMLParser:
    def __init__(self):
        pass

    def parse_attribute(self, attribute_str: Optional[str]) -> Optional[Tuple[str, str]]:
        if attribute_str is None:
            return None
        attribute_str = attribute_str.strip()
        name, value = attribute_str.split('=', 1)
        name = name.strip()
        value = value.strip()
        if value[0] == '"':
            value = value[1:len(value) - 1]
        return (name, value)

    def find_first_tag(self, string: str):
        start_open = None
        for i in range(len(string)):
            char = string[i]
            if char == "<":
                start_open = i
                break
        if start_open is None:
            return None
        start_end = None
        for i in range(start_open, len(string)):
            char = string[i]
            if char == ">":
                start_end = i
                break
        if start_end is None:
            raise TypeError("invalid html")
        start_tag_tmp = string[start_open + 1:start_end].strip()
        splitted = start_tag_tmp.split(" ", 1)
        if len(splitted) > 1:
            start_tag, attribute_str = splitted
        else:
            start_tag = splitted[0]
            attribute_str = None
        attribute = self.parse_attribute(attribute_str)
        end_open = None
        for i in range(len(string)):
            if string[i:].startswith("</{}".format(start_tag)):
                end_open = i
                break
        if end_open is None:
            raise TypeError("invalid html")
        end_end = None
        for i in range(end_open, len(string)):
            char = string[i]
            if char == ">":
                end_end = i
                break
        if end_end is None:
            raise TypeError("invalid html")
        content = string[start_end + 1: end_open]
        return HTMLElement(start_tag, attribute, content, start_open, end_end)

    def create_parser_item(self, element: HTMLElement) -> IParserItem:
        for item_type in item_types_tags:
            if element.tag.lower() in item_types_tags[item_type]:
                if item_type == ItemType.URL:
                    if element.attribute[0] == "href":
                        return ParserUrl(element.content, element.attribute[1])
                    else:
                        raise TypeError("invalid html: 'a' tag needs 'href' attribute")
                else:
                    return ParserTag(element.content, item_type)
        raise TypeError("unknown tags found")

    def parse(self, html: str) -> IParserItem:
        items = []
        while True:
            element = self.find_first_tag(html)
            if element is None:
                items.append(ParserSonderzeichenString(html))
                break
            if element.start > 0:
                items.append(ParserSonderzeichenString(html[:element.start]))
            items.append(self.create_parser_item(element))
            html = html[element.end + 1:]
        return ParserCollection(items)
