from typing import List
from .parser import HTMLParser, IParserItem

class IPageSplittingMethod:
    """Interface for methods that splits a text into pages."""

    def gen_paged_list(self, text: str) -> List[str]:
        """Returns a list with the pages of the text."""
        raise NotImplementedError()


class PageSplitByCharLength(IPageSplittingMethod):
    """Splits a text into pages by an amount of chars per page."""
    def __init__(self, chars_per_page: int):
        self.chars_per_page = chars_per_page

    def gen_amount_pages_by_length(self, len_text: int) -> int:
        """Returns the amount of pages for a length of text."""
        pages_f = len_text / self.chars_per_page
        pages = int(pages_f)
        if pages < pages_f:
            pages += 1
        return pages

    def gen_paged_list(self, text: str) -> List[str]:
        """Splits a message into pages of chars per page."""
        len_text = len(text)
        am_pag = self.gen_amount_pages_by_length(len_text)
        msgs = []
        for page_index in range(am_pag):
            first_index = self.chars_per_page * page_index
            last_index = self.chars_per_page * (page_index + 1)
            msgs.append(text[first_index:last_index])
        return msgs


class PageSplitByCharLengthHTML(IPageSplittingMethod):
    """Splits a text into pages by an amount of chars per page."""
    def __init__(self, chars_per_page: int):
        self.chars_per_page = chars_per_page

    def gen_amount_pages_by_length(self, len_text: int) -> int:
        """Returns the amount of pages for a length of text."""
        pages_f = len_text / self.chars_per_page
        pages = int(pages_f)
        if pages < pages_f:
            pages += 1
        return pages

    def gen_paged_list(self, text: str) -> List[str]:
        """Splits a message into pages of chars per page."""
        parser = HTMLParser()
        item: IParserItem = parser.parse(text)
        len_text = len(item)
        am_pag = self.gen_amount_pages_by_length(len_text)
        msgs = []
        for page_index in range(am_pag):
            first_index = self.chars_per_page * page_index
            last_index = self.chars_per_page * (page_index + 1)
            msgs.append(str(item[first_index:last_index]))
        return msgs
