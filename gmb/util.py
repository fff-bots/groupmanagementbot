import json
from enum import IntEnum, Enum
from peewee import SmallIntegerField


def read_json_file(filename: str):
    with open(filename, "r") as file:
        raw = file.read()
        out = json.loads(raw)
        return out


def write_json_file(filename: str, json_object):
    text = json.dumps(json_object)
    with open(filename, "w") as file:
        file.write(text)


def sanitice_location(directory: str) -> str:
    # todo: use library
    if directory is None or directory == "":
        return ""
    if len(directory) <= 0:
        return ""
    if directory[len(directory) - 1] != "/":
        directory = directory + "/"
    return directory


class AutoInt(IntEnum):
    """Base class for enums with automatic values that should be the count."""

    @staticmethod
    def _generate_next_value_(_name, _start, count, _last_values):
        return count

    def __bool__(self) -> bool:
        return True


class EnumField(SmallIntegerField):
    """This class enables a Enum like field for Peewee.

    This class is used from juancarlospaco.
    License notice: GPLv3
    Copyright notice: peewee-extra-fields  Copyright (C) 2019  juancarlospaco
    Link:
    https://github.com/juancarlospaco/peewee-extra-fields/blob/master/peewee_extra_fields/__init__.py#L1020
    """

    def __init__(self, enum, *args, **kwargs):
        if not issubclass(enum, Enum):
            raise TypeError((f"{self.__class__.__name__} Argument enum must be"
                             f" subclass of enum.Enum: {enum} {type(enum)}."))
        self.enum = enum
        super().__init__(*args, **kwargs)

    def db_value(self, member):
        return member.value

    def get_enum(self):
        return self.enum

    def python_value(self, value):
        enum = self.get_enum()
        return enum(value)

    def coerce(self, value):
        enum = self.get_enum()
        if value not in enum:
            raise ValueError((f"{self.__class__.__name__} the value must be "
                              f"member of the enum: {value}, {enum}."))
