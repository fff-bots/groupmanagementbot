from __future__ import annotations

import logging

import telegram
from telegram.ext import CommandHandler, Updater

from gmb.api.resource import Chats
from gmb.database.database import DatabaseSingleton
from gmb.database.schema import Schema, SchemaLevel
from gmb.rest_api.rest_api import RestApi
from gmb.environment import Environment
from gmb.infofetchbot.infofetchbot import InfoFetch
# from gmb.plugins import *
from gmb.plugins import import_plugins
from gmb.pollingbot.pollingbot import PollingBot, PollingBotExtensions
from gmb.version import version
from threading import Thread


def init_env() -> Environment:
    """Initializes the basic environment."""
    # load environment
    environment = Environment.init()
    # load plugins
    import_plugins()
    # initialize database
    DatabaseSingleton.initialize_default()
    # create all database tables
    # classes are added automatically through the decorators in schema module
    # Schema.drop_tables(SchemaLevel.PLUGIN_CONFIG)
    Schema.create_tables()
    return environment


def print_env() -> None:
    """Prints all loaded schemas and pollingbot extensions."""
    schemas = ""
    for level, deps in Schema.get_dependencies().get_sorted_by_level().items():
        lvl = str(level).replace(level.__class__.__name__ + ".", "")
        dependencies = ", ".join(dep.get_model().__name__ for dep in deps)
        schemas += "\n  Level {}: {}".format(lvl, dependencies)
    schemas = schemas[1:]
    print("Loaded database schemas:\n{}".format(schemas))
    extensions = ", ".join(ext.__name__
                           for ext in PollingBotExtensions.get_extensions())
    print("Loaded pollingbot extensions:\n  {}".format(extensions))


def init_logging(environment: Environment) -> None:
    """Initializes the logging."""
    log_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    if environment.static_config.debug_mode:
        logging.basicConfig(format=log_format, level=logging.DEBUG)
    else:
        logging.basicConfig(format=log_format, level=logging.INFO)
        # Example logging:
        # logging.debug('This is a debug message')
        # logging.info('This is an info message')
        # logging.warning('This is a warning message')
        # logging.error('This is an error message')
        # logging.critical('This is a critical message')


def init_bot(environment: Environment) -> None:
    """Initializes the bot and the updater and adds the bot and the updater to
    the environment."""
    config = environment.static_config
    # Init Bot
    token = config.token
    if token == "" or token is None:
        print(
            "Token is not set. Have a look at the README and configure the bot correctly."
        )
        exit()
    print("Initializing Bot")
    bot = telegram.Bot(token=token)
    environment.set_bot(bot)
    updater = Updater(token=token)
    environment.set_updater(updater)


def print_bot(environment: Environment) -> None:
    """Prints the bot and all chats."""
    bot = environment.get_bot()
    # me
    print(bot.getMe())
    Chats.print_chats()


def init_infofetch(environment: Environment) -> InfoFetch:
    """Initializes the infofetch bot and adds it to the environment."""
    # InfoFetch Bot
    bot = environment.get_bot()
    infofetch = InfoFetch(bot)
    return infofetch


def init_pollingbot(environment: Environment) -> PollingBot:
    """Initializes the polling bot and adds it to the environment."""
    # Create PollingBot
    polling_bot = PollingBot()
    # Add Handlers
    updater = environment.get_updater()
    polling_bot.add_handlers(updater.dispatcher)
    return polling_bot


def cmd_start(environment: Environment, infofetch: InfoFetch) -> None:
    """Command to start the bot.

    Starts the bot and its plugins.

    Args:
        config: The config for the bot.
    """
    # Start Polling
    print("Start polling")
    updater = environment.get_updater()
    updater.start_polling()
    if not environment.static_config.without_api:
        # if api is activated, its the main thread
        infofetch_thread = Thread(target=infofetch.run)
        infofetch_thread.start()
        RestApi.run(environment)
        print("Exiting")
        print("Stopped API")
        infofetch.exit.set()
    else:
        # if api is not activated, infofetch is the main thread
        try:
            infofetch.run()
        except KeyboardInterrupt:
            pass
        print("Exiting")
        print("Stopped Infofetch")
    updater.stop()
    print("Stopped Pollingbot")


# Main function
def main() -> None:
    """Main function."""
    print("GroupManagement Bot")
    print("Version: {}".format(version))
    environment = init_env()
    print_env()
    init_logging(environment)
    init_bot(environment)
    if not Environment.get().static_config.stop_after_init:
        print_bot(environment)
    infofetch = init_infofetch(environment)
    init_pollingbot(environment)
    if not environment.static_config.without_api:
        RestApi.init(environment)
    if environment.static_config.stop_after_init:
        return
    cmd_start(environment, infofetch)
