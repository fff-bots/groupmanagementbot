from peewee import BooleanField

from typing import Optional

from telegram import Update, Bot, Message

from gmb.api.database import BaseModel, register_system_config
from gmb.telegram_util import (GroupTypes, get_bot_and_update, has_grouptype,
                               is_private_group)
from gmb.custom_message.utils import send_custom_reply_timered
from gmb.custom_message.tidy_mode import tidy_and_not_private, MessageLifetime


@register_system_config
class SilentMode(BaseModel):
    """Silent mode is to have cleaner chats.

    If the silent mode is turned on, some messages won't answer in group chats to make
    them more clean.
    """
    silent = BooleanField(null=False, default=False)


def silent() -> bool:
    """Returns if silent mode is turned on."""
    return SilentMode.get_entry().silent


def silent_and_not_private(update: Update) -> bool:
    """Returns if silent mode is turned on and the group is not private."""
    return silent() and not is_private_group(update)


def silent_mode_fallback_reply(bot: Bot, update: Update) -> Optional[Message]:
    """Sends the error message if silent mode and tidy mode is turned on."""
    if tidy_and_not_private(update):
        text = "Dieser Befehl funktioniert nur im privaten Chat mit dem Bot."
        return send_custom_reply_timered(text, bot, update, MessageLifetime.SHORT)
    return None


def __only_group_type(func, grouptype: GroupTypes):
    def intern(*args, **kwargs):
        (bot, update) = get_bot_and_update(*args, **kwargs)
        if not silent():
            return func(*args, **kwargs)
        if has_grouptype(update, grouptype):
            return func(*args, **kwargs)
        return silent_mode_fallback_reply(bot, update)

    return intern


def silent_mode(func):
    """The command is only executed in private chats if silent mode is turned on."""
    return __only_group_type(func, GroupTypes.private)
