from __future__ import annotations

from typing import Any, Callable, Optional, Type

from peewee import (BigIntegerField, BooleanField, CharField, CompositeKey,
                    DoesNotExist, ForeignKeyField, IntegerField, TextField)

from gmb.api.database import (BaseModel, register_non_persistent,
                              register_non_persistent_with_dependencies)
from gmb.database.error_handler import handle_errors


class AbstractChatsConfig(BaseModel):
    id = BigIntegerField(primary_key=True, null=False)
    name = CharField(unique=True, max_length=255, null=False)
    prio = IntegerField(null=False)


class AbstractChat(BaseModel):
    """Abstract Database Chat Model.

    Warning: Only use inherited classes of AbstractChat. For an Example see NetworkChat.
    """

    id = BigIntegerField(primary_key=True, null=False)
    channel = BooleanField(null=False)
    title = CharField(null=False)
    username = CharField(null=True)
    name = CharField(null=False)
    fullname = CharField(null=False)
    photo = CharField(null=True)
    pinned_message = IntegerField(null=True)
    description = TextField(null=True)
    user_amount = IntegerField(null=False)
    invite_link = CharField(null=True)
    prio = IntegerField(null=True)


class AbstractUser(BaseModel):
    """Abstract Database User Model.

    Warning: Only use inherited classes of AbstractUser. For an Example see NetworkUser.
    """

    id = IntegerField(primary_key=True, null=False)
    username = CharField(null=True)
    first_name = CharField(null=True)
    picture = CharField(null=True)
    is_bot = BooleanField(null=True)


class AbstractChatAdmin(BaseModel):
    """Abstract Database User Model.

    Warning: Only use inherited classes of AbstractChatAdmin that are created with
             the decorator chat_admin_class. For an Example see NetworkChatAdmin.
    """

    @classmethod
    def _update_or_create_chat_admin(cls, admin, chat):
        # update or create für chat admin list sollte diese Methode nutzen:
        # http://docs.peewee-orm.com/en/latest/peewee/api.html#Model.get_or_create
        try:
            # TODO https://github.com/palantir/python-language-server/issues/552
            get_method = super(BaseModel, cls).get
            item = get_method(admin=admin, chat=chat)
        except DoesNotExist:
            create_method = super(BaseModel, cls).create
            item = create_method(admin=admin, chat=chat)
        return item

    @classmethod
    @handle_errors
    def update_or_create_chat_admin(cls, admin, chat):
        return cls._update_or_create_chat_admin(admin, chat)


def chat_admin_class(cls_admin: Type[AbstractUser],
                     cls_chat: Type[AbstractChat],
                     cls_table_name: Optional[str] = None):
    """Decorator to create a ChatAdmin class."""

    def decorator(cls: Type[AbstractChatAdmin]) -> Type[AbstractChatAdmin]:
        class _ChatAdmin(cls.__base__):
            admin = ForeignKeyField(cls_admin, backref="chat_admins", null=False)
            chat = ForeignKeyField(cls_chat, backref="chat_admins", null=False)

            class Meta:
                primary_key = CompositeKey("admin", "chat")
                table_name = cls_table_name or cls.__name__.lower()

        _ChatAdmin.__name__ = cls.__name__
        return _ChatAdmin

    return decorator
