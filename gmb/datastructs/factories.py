import os
import urllib.request
from typing import Callable, Any

import telegram
from telegram import Bot, Chat, User

from gmb.resource.extension import ResourceExt
from gmb.util import sanitice_location

from .model import AbstractChat, AbstractChatsConfig

TagFilter = Callable[[Chat, AbstractChatsConfig], str]
ResourceFactory = Callable[[Bot, ResourceExt, AbstractChatsConfig, str],
                           AbstractChat]


def get_chat_member_info(bot: Bot, chat_id: int, user_id: int) -> Any:
    chat_member = bot.get_chat_member(chat_id, user_id)
    picture = None
    user_dict = {
        "id": chat_member.user['id'],
        "username": chat_member.user['username'],
        "first_name": chat_member.user['first_name'],
        "picture": picture,
        "is_bot": chat_member.user['is_bot'],
    }
    return user_dict


def get_user_info(bot: Bot, user: User) -> Any:
    picture = None
    user_dict = {
        "id": user.id,
        "username": user.username,
        "first_name": user.first_name,
        "picture": picture,
        "is_bot": user.is_bot,
    }
    return user_dict


def create_chat_factory(get_name: TagFilter,
                        get_fullname: TagFilter) -> ResourceFactory:
    def create_chat(bot: Bot, resource_ext: ResourceExt,
                    chats_config: AbstractChatsConfig,
                    output_dir: str) -> AbstractChat:
        """Creats a chat.

        This method requests data from telegram of the chat with the given chat_id.
        Then it creates a chat object and requests data about the admins of the
        chat and other required information.

        Args:
            chat_id: Id or username of the chat.
            bot: The bot that makes the requests to Telegram.
            chat_prio: The priority of the chat.

        Returns:
            AbstractChat: The concrete AbstractChat object with the id chat_id.
        """
        abs_chat = resource_ext.get_chat_class()
        abs_user = resource_ext.get_user_class()
        abs_chat_admin = resource_ext.get_chat_admin_class()
        chat_id = chats_config.id
        chat_prio = chats_config.prio
        output_dir = sanitice_location(output_dir)
        res: Chat = bot.get_chat(chat_id)
        if res['pinned_message'] is not None:
            pinned_message = res['pinned_message']['message_id']
        else:
            pinned_message = None
        if res['photo'] is not None:
            pic = bot.getFile(res['photo']['small_file_id'])
            photo = pic['file_id'] + ".jpg"
            if not os.path.isfile(photo):
                urllib.request.urlretrieve(pic['file_path'],
                                           output_dir + photo)
        else:
            photo = None
        name = get_name(res, chats_config)
        fullname = get_fullname(res, chats_config)
        amount_users = bot.get_chat_members_count(chat_id)
        channel = res["type"] == "channel"
        if res["invite_link"] is not None:
            invite_link = res["invite_link"]
        else:
            try:
                invite_link = bot.export_chat_invite_link(chat_id)
            except telegram.error.TelegramError:
                print("Warning: The bot has no rights to get the invite link")
                tmp_name = res["username"]
                if tmp_name is None:
                    tmp_name = res["title"]
                    invite_link = None
                else:
                    invite_link = "https://t.me/" + res["username"]
                print("\t" + str(tmp_name) + " (" + str(res["id"]) + ")")
        admins = []
        if res['type'] != "channel":
            admins_raw = bot.get_chat_administrators(chat_id)
            for admin_raw in admins_raw:
                picture = None
                user_dict = {
                    "id": admin_raw['user']['id'],
                    "username": admin_raw['user']['username'],
                    "first_name": admin_raw['user']['first_name'],
                    "picture": picture,
                    "is_bot": admin_raw['user']['is_bot']
                }
                admin = abs_user.update_or_create("id", **user_dict)
                admins.append(admin)
        chat_dict = {
            "id": res['id'],
            "channel": channel,
            "title": res['title'],
            "username": res['username'],
            "name": name,
            "fullname": fullname,
            "photo": photo,
            "pinned_message": pinned_message,
            "description": res['description'],
            "user_amount": amount_users,
            "invite_link": invite_link,
            "prio": chat_prio
        }
        chat = abs_chat.update_or_create("id", **chat_dict)
        for admin in admins:
            abs_chat_admin.update_or_create_chat_admin(admin=admin, chat=chat)
        return chat

    return create_chat
