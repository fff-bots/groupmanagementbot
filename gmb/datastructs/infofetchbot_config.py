from peewee import IntegerField

from gmb.api.database import BaseModel, register_plugin_config


@register_plugin_config
class InfoFetchConfig(BaseModel):
    update_time = IntegerField(null=False, default=600)
    prio_standard = IntegerField(null=False, default=0)
