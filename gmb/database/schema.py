from __future__ import annotations

from dataclasses import dataclass
from enum import auto
from typing import Dict, List, Optional, Type, Union

from peewee import Model

from gmb.util import AutoInt

from .database import DatabaseSingleton


class ModelNotFound(Exception):
    """Is raised if a model could not be found."""

    def __init__(self: ModelNotFound, modelname: str):
        super().__init__()
        self.modelname = modelname

    def __str__(self: ModelNotFound) -> str:
        msg = "No model found with name: {}".format(self.modelname)
        return msg


class SchemaLevel(AutoInt):
    """On which level the database table lays."""
    SYSTEM_CONFIG = auto()
    PLUGIN_CONFIG = auto()
    NON_PERSISTENT = auto()


class IDependency:
    """Interface for a dependency."""

    def get_model(self) -> Type[Model]:
        """Returns the model type of the dependency."""
        raise NotImplementedError

    def get_schema_level_save(self) -> SchemaLevel:
        """Returns the schema level or raises an exception if it is none."""
        raise NotImplementedError

    def get_schema_level(self) -> Optional[SchemaLevel]:
        """Returns the schema level of the dependency."""
        raise NotImplementedError

    def get_dependencies(self) -> List[IDependency]:
        """Returns the dependencies of the dependency."""
        raise NotImplementedError

    def add_dependency(self, dependency: IDependency) -> None:
        """Adds a dependency if it is not already added."""
        raise NotImplementedError

    def set_schema_level(self, schema_level: SchemaLevel) -> None:
        """Sets the schema level of the dependency."""
        raise NotImplementedError

    def __gt__(self, value) -> bool:
        """Returns true if value is an dependency of self."""
        raise NotImplementedError


class Dependency(IDependency):
    """Implementation of a dependency."""

    def __init__(self,
                 model: Type[Model],
                 schema_level: Optional[SchemaLevel] = None):
        self.__model = model
        self.__schema_level = schema_level
        self.__dependencies: List[IDependency] = []

    def get_model(self) -> Type[Model]:
        return self.__model

    def get_schema_level_save(self) -> SchemaLevel:
        if not self.__schema_level:
            msg = "Dependency error: Dependency {} has no schema level"
            raise Exception(msg.format(self.__model))
        return self.__schema_level

    def get_schema_level(self) -> Optional[SchemaLevel]:
        return self.__schema_level

    def get_dependencies(self) -> List[IDependency]:
        return self.__dependencies

    def add_dependency(self, dependency: IDependency) -> None:
        if dependency not in self.__dependencies:
            self.__dependencies.append(dependency)

    def set_schema_level(self, schema_level: SchemaLevel) -> None:
        self.__schema_level = schema_level

    def __lt__(self, value) -> bool:
        if isinstance(value, IDependency):
            return value not in self.__dependencies
        return True


class Dependencies:
    """Container for all dependencies."""

    def __init__(self):
        self.models: List[IDependency] = []

    def get(self, model: Type[Model]) -> Optional[IDependency]:
        """Returns the related dependency if it exists."""
        for self_model in self.models:
            if self_model.get_model() == model:
                return self_model
        return None

    def add_model(self, model: Type[Model], schema_level: SchemaLevel,
                  dependencies: List[Type[Model]]) -> None:
        """Adds a model.

        Creates a dependency object from the model and adds the schema level
        and the dependencies to the new dependency object. If dependencies
        doesn't exist, they will be created with schema level None and no
        dependencies. If a model already exists in the list, for example one
        of those dummy dependencies with schema level None, it will be overridden.
        """
        self_model: Optional[IDependency] = self.get(model)
        if not self_model:
            # create model
            self_model = Dependency(model, schema_level)
            self.models.append(self_model)
        else:
            # override old values
            self_model.set_schema_level(schema_level)
        # add dependencies
        for dependency in dependencies:
            self_dependency = self.get(dependency)
            # create missing dependencies
            if not self_dependency:
                self_dependency = Dependency(dependency)
                self.models.append(self_dependency)
            self_model.add_dependency(self_dependency)

    def get_sorted_by_level(self) -> Dict[SchemaLevel, List[IDependency]]:
        dependencies: Dict[SchemaLevel, List[IDependency]] = {}
        for level in SchemaLevel:
            dependencies[level] = []
        for dependency in self.models:
            dependencies[dependency.get_schema_level_save()].append(dependency)
        return dependencies

    def get_sorted(self) -> List[IDependency]:
        return self.get_sorted_from()

    def get_sorted_from(self, schema_level: Optional[SchemaLevel] = None
                        ) -> List[IDependency]:
        dependencies: List[IDependency] = []
        for model in self.models:
            if model not in dependencies:
                self_schema_level = model.get_schema_level_save()
                if (not schema_level) or self_schema_level >= schema_level:
                    dependencies.append(model)
        dependencies.sort()
        return dependencies


class Schema:

    __models: Dependencies = Dependencies()

    @staticmethod
    def register(model: Type[Model],
                 level: SchemaLevel,
                 dependencies: List[Type[Model]] = []):
        """Adds a model to the schema.

        Adds a model to the schema on the specified schema level and also
        makes a notice for the dependencies, so that when the tables are
        created, they are created in the right order.
        """
        print("Registered model: {}".format(model.__name__))
        Schema.__models.add_model(model, level, dependencies)

    @staticmethod
    def create_tables():
        models: List[Type[Model]] = []
        for dependency in Schema.__models.get_sorted():
            models.append(dependency.get_model())
        DatabaseSingleton.get().create_tables(models)

    @staticmethod
    def drop_tables(level: SchemaLevel = SchemaLevel.NON_PERSISTENT):
        models: List[Type[Model]] = []
        for dependency in Schema.__models.get_sorted_from(level):
            models.append(dependency.get_model())
        DatabaseSingleton.get().drop_tables(models[::-1])

    @staticmethod
    def get_dependencies() -> Dependencies:
        return Schema.__models

    @staticmethod
    def get_resource_by_table_name(name: str) -> Type[Model]:
        """Returns the model with the given name.

        Raises: ModelNotFound, if there is no model with the given name.
        """
        for dependency in Schema.get_dependencies().get_sorted():
            model: Type[Model] = dependency.get_model()
            if model._meta.table_name.lower() == name.lower():
                return model
        raise ModelNotFound(name)

    @staticmethod
    def print():
        print("Current Models:")
        for model in Schema.__models.models:
            print(model.get_model(), model.get_schema_level(),
                  model.get_dependencies())


def register_system_config(cls: Type[Model]):
    Schema.register(cls, SchemaLevel.SYSTEM_CONFIG)
    return cls


def register_system_config_with_dependencies(*dependencies: Type[Model]):
    def decorator(cls):
        Schema.register(cls, SchemaLevel.SYSTEM_CONFIG, dependencies)
        return cls

    return decorator


def register_plugin_config(cls: Type[Model]):
    Schema.register(cls, SchemaLevel.PLUGIN_CONFIG)
    return cls


def register_plugin_config_with_dependencies(*dependencies: Type[Model]):
    def decorator(cls):
        Schema.register(cls, SchemaLevel.PLUGIN_CONFIG, dependencies)
        return cls

    return decorator


def register_non_persistent(cls: Type[Model]):
    Schema.register(cls, SchemaLevel.NON_PERSISTENT)
    return cls


def register_non_persistent_with_dependencies(*dependencies: Type[Model]):
    def decorator(cls):
        Schema.register(cls, SchemaLevel.NON_PERSISTENT, dependencies)
        return cls

    return decorator
