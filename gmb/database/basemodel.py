from __future__ import annotations

from peewee import DoesNotExist, Model
from typing import Type, Optional

from .database import DatabaseSingleton
from .error_handler import handle_errors


class IBaseModel:
    @classmethod
    def get_all(cls):
        """Returns a list with all table entries."""
        raise NotImplementedError()

    @classmethod
    def get_entry(cls):
        """Returns the first entry in the table."""
        raise NotImplementedError()

    @classmethod
    def try_get_by_id(cls, pk):
        raise NotImplementedError()

    @classmethod
    def update_or_create(cls, pk_name, **kwargs):
        """Updates an entry or creates a new one.

        Attributes:
            pk_name: Name of the primary key. (example: "id")
            **kwargs: Dict with all members of the entry.
                      (example: {"id": 7, "name": "Bernd"})
        """
        raise NotImplementedError()

    @classmethod
    def create(cls, **query):
        raise NotImplementedError()

    @classmethod
    def select(cls, *fields):
        raise NotImplementedError()

    @classmethod
    def get(cls, *query, **filters):
        raise NotImplementedError()

    @classmethod
    def delete(cls):
        raise NotImplementedError()

    @classmethod
    def get_by_id(cls, pk):
        raise NotImplementedError()


class BaseModel(Model, IBaseModel):
    """A base model that will use our database."""

    class Meta:
        database = DatabaseSingleton.get()

    @classmethod
    def _get_all(cls):
        items = []
        for item in super().select():
            items.append(item)
        return items

    @classmethod
    @handle_errors
    def get_all(cls):
        return cls._get_all()

    @classmethod
    def _get_entry(cls):
        for item in super().select():
            return item
        return super().create()

    @classmethod
    @handle_errors
    def get_entry(cls):
        return cls._get_entry()

    @classmethod
    def _try_get_by_id(cls, item_id):
        try:
            item = super().get_by_id(item_id)
            return item
        except DoesNotExist:
            return None

    @classmethod
    @handle_errors
    def try_get_by_id(cls, pk):
        return cls._try_get_by_id(pk)

    @classmethod
    def _update_or_create(cls, pk_name, **kwargs):
        # TODO: id statt pkname und kwargs einzeln übergeben sollte reichen
        # update or create für chat admin list sollte diese Methode nutzen:
        # http://docs.peewee-orm.com/en/latest/peewee/api.html#Model.get_or_create
        item = cls._try_get_by_id(kwargs[pk_name])
        if item is None:
            item = super().create(**kwargs)
        else:
            item.__data__ = {**kwargs}
        item.save()
        return item

    @classmethod
    @handle_errors
    def update_or_create(cls, pk_name, **kwargs):
        return cls._update_or_create(pk_name, **kwargs)

    @classmethod
    @handle_errors
    def create(cls, **query):
        return super().create(**query)

    @classmethod
    @handle_errors
    def select(cls, *fields):
        return super().select(*fields)

    @classmethod
    @handle_errors
    def get(cls, *query, **filters):
        return super().get(*query, **filters)

    @classmethod
    @handle_errors
    def delete(cls):
        return super().delete()

    @classmethod
    @handle_errors
    def get_by_id(cls, pk):
        return super().get_by_id(pk)
