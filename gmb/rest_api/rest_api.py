from __future__ import annotations

from typing import Type, Optional, Dict, Any, Union, Callable, List
from functools import wraps
from flask import Flask, request
from peewee import Model
from playhouse.shortcuts import model_to_dict
from dataclasses import dataclass

import json

from gmb.database.schema import Schema, ModelNotFound
from gmb.environment import Environment

from .http_codes import (HTTPCode, ApiException, MethodNotAllowed, NotFound,
                         InternalServerError, NotImplementedMethod, Unauthorized)


@dataclass
class ApiUser:
    """A user with a password."""
    username: str
    password: str


# TODO move to config module
class InvalidConfigFormatException(Exception):
    """Is raised when a config variable is formatted wrong."""

    def __init__(self, message: Optional[str] = None):
        super().__init__()
        self.message = message

    def __str__(self: ApiException) -> str:
        msg = self.__class__.__name__
        if self.message is None:
            return msg
        return "{}: {}".format(msg, self.message)


class RestApi:
    """Contains the api app."""
    app = Flask(__name__)
    api_users: List[ApiUser] = []

    @staticmethod
    def run(environment: Environment) -> None:
        env_conf = environment.static_config
        conf: Dict[str, Any] = {}
        if env_conf.flask_port is not None:
            try:
                conf["port"] = int(env_conf.flask_port)
            except TypeError:
                print("RestApi.init raised TypeError on converting port to integer.")
                msg = "Config key 'port' needs to be an integer or None."
                raise InvalidConfigFormatException(msg)
        if env_conf.flask_host is not None:
            conf["host"] = env_conf.flask_host
        RestApi.app.run(**conf)

    @staticmethod
    def init(environment: Environment):
        conf = environment.static_config
        if conf.api_users:
            for raw_user in conf.api_users.split(","):
                tmp = raw_user.split(":")
                if len(tmp) != 2:
                    msg = "Config key 'api_users' needs a comma separated list of basic auth users."
                    msg += " An basic auth user needs to be in the format user:password."
                    raise InvalidConfigFormatException(msg)
                RestApi.api_users.append(ApiUser(tmp[0], tmp[1]))


@dataclass
class ApiResponse:
    """All responses in the api should be ApiResponses."""

    body: Optional[Any]
    http_code: HTTPCode = HTTPCode.OK

    def __str__(self: ApiResponse) -> str:
        return json.dumps(self.body)

    @staticmethod
    def create_from_model(item: Model) -> ApiResponse:
        """Creates a API Response from an item of a model."""
        return ApiResponse(model_to_dict(item))

    @staticmethod
    def create_from_model_list(items: List[Model]) -> ApiResponse:
        """Creates a API Response from a list of items of a model."""
        out_items = [model_to_dict(item) for item in items]
        return ApiResponse(out_items)


def convert_exceptions(exception: Exception) -> ApiException:
    """Converts exceptions to API exceptions."""
    if isinstance(exception, ApiException):
        return exception
    if isinstance(exception, ModelNotFound):
        return NotFound(exception.modelname)
    return InternalServerError(exception)


def isadmin():
    """Checks if the user is an admin."""
    if request.authorization:
        user = request.authorization["username"]
        passwd = request.authorization["password"]
        if ApiUser(user, passwd) in RestApi.api_users:
            return True
    return False


def admin_required(func):
    """Decorator for methods that needs an admin."""
    @wraps(func)
    def decorator(*args, **kwargs):
        if not isadmin():
            raise Unauthorized()
        return func(*args, **kwargs)
    return decorator


def api_wrapper(func: Callable[..., ApiResponse]):
    """Catches all errors and wraps them in the api."""
    @wraps(func)
    def inner(*args, **kwargs):
        try:
            api_response = func(*args, **kwargs)
            return str(api_response), api_response.http_code.value
        except Exception as exception:
            print("Catched exception: {}".format(exception))
            api_exception = convert_exceptions(exception)
            print("Converted to: {}".format(api_exception))
            return str(api_exception), api_exception.http_code.value
    return inner


def rest_get_all(resource: str) -> ApiResponse:
    """Returns all items of a given resource."""
    model: Type[Model] = Schema.get_resource_by_table_name(resource)
    items = model.get_all()
    return ApiResponse.create_from_model_list(items)


def rest_subresource_get_all(resource: str, obj_id: int, subresource: str) -> ApiResponse:
    """Returns all items of a given resource."""
    model: Type[Model] = Schema.get_resource_by_table_name(resource)
    item = model.try_get_by_id(obj_id)
    if item is not None:
        try:
            subitems = [x for x in item.__getattribute__(subresource)]
            return ApiResponse.create_from_model_list(subitems)
        except AttributeError:
            raise NotFound(subresource)
    raise NotFound(resource, obj_id)


def rest_get_by_id(resource: str, *obj_id: int) -> ApiResponse:
    """Returns the item of a given resource with the given id."""
    model: Type[Model] = Schema.get_resource_by_table_name(resource)
    item = model.try_get_by_id(obj_id)
    if item is not None:
        return ApiResponse.create_from_model(item)
    raise NotFound(resource, *obj_id)


@admin_required
def rest_delete_by_id(resource: str, *obj_id: int) -> ApiResponse:
    """Some documentation."""
    model: Type[Model] = Schema.get_resource_by_table_name(resource)
    if model.try_get_by_id(obj_id) is None:
        return NotFound(resource, *obj_id)
    model.delete_by_id(obj_id)
    return ApiResponse(None, HTTPCode.NoContent)


@RestApi.app.route("/rest/<string:resource>/", methods=['POST', 'GET', 'PUT', 'DELETE'])
@api_wrapper
def rest_resource(resource: str) -> ApiResponse:
    """Contains methods on resource level."""
    if request.method == 'GET':
        # limit resources
        # request.args.get("startIndex")
        # request.args.get("size")
        return rest_get_all(resource)
    if request.method == 'POST':
        # create new
        # return 201 code
        raise NotImplementedMethod(request.method, resource)
    raise MethodNotAllowed(request.method)


@RestApi.app.route("/rest/<string:resource>/<int(signed=True):obj_id1>/<string:subres>",
                   methods=['POST', 'GET', 'PUT', 'DELETE'])
@api_wrapper
def rest_subresource(resource: str, obj_id1: int, subres: str) -> ApiResponse:
    """Contains methods for targeting a specific resource."""
    if request.method == 'GET':
        return rest_subresource_get_all(resource, obj_id1, subres)
    if request.method == 'POST':
        # update or create
        # user = request.form['username']
        raise NotImplementedMethod(request.method, resource)
    raise MethodNotAllowed(request.method)


@RestApi.app.route("/rest/<string:resource>/<int(signed=True):obj_id>",
                   methods=['POST', 'GET', 'PUT', 'DELETE'])
@api_wrapper
def rest_resource_id(resource: str, obj_id: int) -> ApiResponse:
    """Contains methods for targeting a specific resource."""
    if request.method == 'GET':
        return rest_get_by_id(resource, obj_id)
    if request.method == 'POST' or request.method == 'PUT':
        # update or create
        # user = request.form['username']
        raise NotImplementedMethod(request.method, resource)
    if request.method == 'DELETE':
        # delete resource
        return rest_delete_by_id(resource, obj_id)
    raise MethodNotAllowed(request.method)


@RestApi.app.route("/rest/<string:resource>/<int(signed=True):obj_id1>/<int(signed=True):obj_id2>",
                   methods=['POST', 'GET', 'PUT', 'DELETE'])
@api_wrapper
def rest_resource_multi_id(resource: str, obj_id1: int, obj_id2: int) -> ApiResponse:
    """Contains methods for targeting a specific resource."""
    if request.method == 'GET':
        return rest_get_by_id(resource, obj_id1, obj_id2)
    if request.method == 'POST' or request.method == 'PUT':
        # update or create
        # user = request.form['username']
        raise NotImplementedMethod(request.method, resource)
    if request.method == 'DELETE':
        # delete resource
        return rest_delete_by_id(resource, obj_id1, obj_id2)
    raise MethodNotAllowed(request.method)
