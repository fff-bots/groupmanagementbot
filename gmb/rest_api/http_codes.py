from __future__ import annotations

from typing import Optional

from enum import Enum

from gmb.environment import Environment


class HTTPCode(Enum):
    OK = 200
    # Successful, with content
    Created = 201
    # Successful, created resource, maybe the location header
    # contains the address of the new resource
    Accepted = 202
    # Accepted, but will be processed later (async)
    NoContent = 204
    # Successful, but without content

    MovedPermanently = 301
    # This and all future requests have a new address
    MovedTemporarily = 302
    # This has a new address

    BadRequest = 400
    # Wrong data, Client Error
    Unauthorized = 401
    # User is not logged in
    Forbidden = 403
    # User has wrong permission
    NotFound = 404
    # Resource not found
    MethodNotAllowed = 405
    # Method is not supported (PUT, POST)

    InternalServerError = 500
    # Unexpected error in server
    NotImplemented = 501
    # Method is not implemented


class ApiException(Exception):
    def __init__(self, http_code: HTTPCode, message: Optional[str] = None):
        super().__init__()
        self.http_code = http_code
        self.message = message

    def __str__(self: ApiException) -> str:
        msg = self.__class__.__name__
        if self.message is None:
            return msg
        return "{}: {}".format(msg, self.message)


class InternalServerError(ApiException):
    """Is thrown when an internal server error occurs."""

    def __init__(self: InternalServerError, exception: Exception):
        if isinstance(exception, Exception):
            exc_name = exception.__class__.__name__
            msg = "Exception {}: {}".format(exc_name, str(exception))
        elif isinstance(exception, type):
            exc_name = exception.__name__
            msg = "Exception {}: {}".format(exc_name, str(exception))
        else:
            msg = "Unknown exception."
        print("INFO: API Exception: {}".format(msg))
        environment = Environment.get()
        env_conf = environment.static_config
        debug = bool(env_conf.debug_mode)
        if not debug:
            msg = "An internal server error occurred, please contact an administrator."
        super().__init__(HTTPCode.InternalServerError, msg)


class Unauthorized(ApiException):
    """Is thrown then a user is not authorized to execute a method."""

    def __init__(self: Unauthorized, message: Optional[str] = "Not authorized."):
        super().__init__(HTTPCode.Unauthorized, message)


class MethodNotAllowed(ApiException):
    """Is thrown when a method is used that is not allowed."""

    def __init__(self: MethodNotAllowed, method: str):
        msg = "Method {} not allowed.".format(method)
        super().__init__(HTTPCode.MethodNotAllowed, msg)


class NotImplementedMethod(ApiException):
    """Is thrown when a method is used that is not allowed."""

    def __init__(self: NotImplementedMethod, method: str, resource: str):
        msg = "Method {} not implemented in resource {}.".format(method, resource)
        super().__init__(HTTPCode.NotImplemented, msg)


class NotFound(ApiException):
    """Is thrown when an item with a specific id was not found."""

    def __init__(self: NotFound, resource: str, *item_ids: Optional[int]):
        if len(item_ids) >= 2:
            tmps = ["id {} '{}'".format(i + 1, item_ids[i]) for i in range(len(item_ids))]
            tmp = " and ".join(tmps)
            msg = "Resource '{}' with {} not found.".format(resource, tmp)
        elif len(item_ids) == 1:
            msg = "Resource '{}' with id '{}' not found.".format(resource, item_ids[0])
        else:
            msg = "Resource '{}' not found.".format(resource)
        super().__init__(HTTPCode.NotFound, msg)
