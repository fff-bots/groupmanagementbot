from __future__ import annotations

import time
from typing import List, Type

from telegram import Bot

from gmb.api.datastructs import AbstractChatsConfig
from gmb.api.infofetch import (InfoFetchExt, InfoFetchExtensions,
                               register_infofetch_extension)
from gmb.api.resource import ResourceExt, ResourceExtensions, clean_all


@register_infofetch_extension
class CleanResourcesExtension(InfoFetchExt):
    @staticmethod
    def run(bot: Bot):
        clean_all(bot)
