from __future__ import annotations

from typing import List, Optional, Tuple

from telegram import Bot, Chat, Update, User
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.permissions import (Admin, only_botadmins, only_superadmins, PermissionLevel,
                             update_has_role, get_role, check_role_callable_fallback,
                             Role, has_role)
from gmb.utils_design import usr_to_str
from gmb.environment import Environment
from gmb.custom_message.utils import send_custom_reply, send_tidy_error, send_tidy_reply


@register_pollingbot_extension
class PermissionsConfigExt(PollBotExt):
    def get_handlers(self: PermissionsConfigExt) -> List[PriorityHandler]:
        return [
            PriorityHandler(CommandHandler('add_admin', self.add_admin),
                            HandlerPriority.NORMAL),
            PriorityHandler(CommandHandler('remove_admin', self.remove_admin),
                            HandlerPriority.NORMAL),
            PriorityHandler(CommandHandler('promote', self.promote),
                            HandlerPriority.NORMAL),
            PriorityHandler(CommandHandler('demote', self.demote),
                            HandlerPriority.NORMAL),
            PriorityHandler(CommandHandler('list_admins', self.list_admins),
                            HandlerPriority.NORMAL),
        ]

    @staticmethod
    def get_user(update: Update) -> Optional[User]:
        # reply
        try:
            return update.message.reply_to_message.from_user
        except AttributeError:
            pass
        return None

    @staticmethod
    def get_user_or_error(bot: Bot, update: Update) -> Optional[User]:
        user = PermissionsConfigExt.get_user(update)
        if not user:
            msg = "Please reply to the user that should be an admin."
            send_custom_reply(msg, bot, update)
            return None
        return user

    @only_botadmins
    def add_admin(self: PermissionsConfigExt, bot: Bot,
                  update: Update) -> None:
        user = PermissionsConfigExt.get_user_or_error(bot, update)
        if user:
            found = Admin.try_get_by_id(user.id)
            if found:
                msg = "User already added to admins."
                send_custom_reply(msg, bot, update)
            else:
                Admin.create_or_update_admin(bot, update.effective_chat.id, user.id)
                msg = "Added user {} ({}) to admins."
                username = user.username or "{} {}".format(
                    user.first_name, user.last_name)
                msg = msg.format(username, user.id)
                send_custom_reply(msg, bot, update)

    @only_botadmins
    def remove_admin(self: PermissionsConfigExt, bot: Bot,
                     update: Update) -> None:
        user = PermissionsConfigExt.get_user_or_error(bot, update)
        if user:
            found = Admin.try_get_by_id(user.id)
            if found:
                query = Admin.delete().where(Admin.id == user.id)
                query.execute()
                msg = "Removed user {} ({}) from admins."
                username = user.username or "{} {}".format(
                    user.first_name, user.last_name)
                msg = msg.format(username, user.id)
                send_custom_reply(msg, bot, update)
            else:
                msg = "User already removed from admins."
                send_custom_reply(msg, bot, update)

    def promote(self, bot: Bot, update: Update) -> None:
        """Increases the admin_level."""
        chat = update.message.chat
        other_user = PermissionsConfigExt.get_user_or_error(bot, update)
        if other_user is None:
            return
        try:
            # you can only promote users up to one rank under you
            # so you need to be two ranks higher
            users_role = get_role(other_user, chat)
            # bugfix: network admins shouldn't promote users to network admins
            # the reason for this behaviour is, that if you promote a user it skips
            # the group admin level
            if users_role == Role.USER:
                needed_role = get_role(other_user, chat).increase().increase().increase()
            else:
                needed_role = get_role(other_user, chat).increase().increase()
        except ValueError:
            # user has highest rank...
            msg = "The user already has highest possible rank."
            send_custom_reply(msg, bot, update)
            return
        if not update_has_role(update, needed_role):
            check_role_callable_fallback(bot, update, needed_role)
            return
        admin = Admin.try_get_by_id(other_user.id)
        if admin:
            if not admin.promote():
                msg = "Can't promote admin."
                send_custom_reply(msg, bot, update)
                return
            admin.save()
        else:
            # automatically adds permission level network admin
            admin = Admin.create_or_update_admin(bot, chat.id, other_user.id)
        msg = "User is now {}".format(admin.get_role().name)
        send_custom_reply(msg, bot, update)

    def demote(self, bot: Bot, update: Update) -> None:
        """Decreases the admin_level."""
        chat = update.message.chat
        # get other user
        other_user = PermissionsConfigExt.get_user_or_error(bot, update)
        if other_user is None:
            return
        # get role the user needs to demote the other user
        try:
            # you can only demote users up to one rank under you
            # so you need to be one rank higher
            needed_role = get_role(other_user, chat).increase()
        except ValueError:
            # if other user is superadmin, then there is no higher rank
            # user has highest rank...
            msg = "Can't demote this user, because it has highest rank."
            send_custom_reply(msg, bot, update)
            return
        # check if user has permissions to demote other user
        if not update_has_role(update, needed_role):
            check_role_callable_fallback(bot, update, needed_role)
            return
        admin = Admin.try_get_by_id(other_user.id)
        # if other user is lower than lowest PermissionLevel,
        # than there is no admin and nothing happens
        if not admin:
            msg = "User is not an admin and can't be demoted."
            send_custom_reply(msg, bot, update)
            return
        # if other user is lowest PermissionLevel, he can't be demoted, but removed from admins
        if admin.permission_level == PermissionLevel.get_lowest_level():
            query = Admin.delete().where(Admin.id == admin.id)
            query.execute()
            tmp = "User removed from admins. It's new rank is now {}"
            msg = tmp.format(get_role(other_user, chat).name)
            send_custom_reply(msg, bot, update)
            return
        # demote other user
        admin.demote()
        admin.save()
        msg = "User is now {}".format(admin.get_role().name)
        send_custom_reply(msg, bot, update)

    def list_admins(self, bot: Bot, update: Update) -> None:
        """Lists all admins."""
        admins = Admin.get_all()
        if not admins:
            msg = "No admins."
            send_tidy_error(msg, bot, update, disable_web_page_preview=True)
            return
        msg = "Admins:"
        admins = sorted(admins, key=lambda chat: -1 * chat.permission_level)
        master_id = Environment.get().static_config.master_admin_id
        superuser: Optional[Admin] = None
        for user in admins:
            if str(user.id) == str(master_id):
                superuser = user
                break
        if superuser:
            admins.remove(superuser)
            msg += "\n- {} (SUPERADMIN)".format(usr_to_str(superuser))
        else:
            msg += "\n- {} (SUPERADMIN)".format(usr_to_str(master_id, "Unknown"))
        for user in admins:
            msg += "\n- {} ({})".format(usr_to_str(user), user.get_role().name)
        send_tidy_reply(msg, bot, update, disable_web_page_preview=True)
