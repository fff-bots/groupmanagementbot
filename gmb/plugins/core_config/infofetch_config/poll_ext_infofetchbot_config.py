from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.datastructs.infofetchbot_config import InfoFetchConfig
from gmb.permissions import only_botadmins
from gmb.custom_message.utils import send_custom_reply


@register_pollingbot_extension
class InfoFetchConfigExt(PollBotExt):
    def get_handlers(self: InfoFetchConfigExt) -> List[PriorityHandler]:
        return [
            PriorityHandler(
                CommandHandler('set_update_time', self.set_update_time),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('reset_update_time', self.reset_update_time),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('set_prio_standard', self.set_prio_standard),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('reset_prio_standard',
                               self.reset_prio_standard),
                HandlerPriority.NORMAL)
        ]

    @only_botadmins
    def set_update_time(self: InfoFetchConfigExt, bot: Bot,
                        update: Update) -> None:
        new_time_tmp = " ".join(update.message.text.split(" ")[1:])
        try:
            new_time = int(new_time_tmp)
        except ValueError:
            msg = "Not updated! New update time is not a number."
            send_custom_reply(msg, bot, update)
            return
        entry = InfoFetchConfig.get_entry()
        entry.update_time = new_time
        entry.save()
        msg = "Update time updated."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def reset_update_time(self: InfoFetchConfigExt, bot: Bot,
                          update: Update) -> None:
        update_time = InfoFetchConfig.update_time.default
        entry = InfoFetchConfig.get_entry()
        entry.update_time = update_time
        entry.save()
        msg = "Update time resetted."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def set_prio_standard(self: InfoFetchConfigExt, bot: Bot,
                          update: Update) -> None:
        new_prio_tmp = " ".join(update.message.text.split(" ")[1:])
        try:
            new_prio = int(new_prio_tmp)
        except ValueError:
            msg = "Not updated! New prio standard is not a number."
            send_custom_reply(msg, bot, update)
            return
        entry = InfoFetchConfig.get_entry()
        entry.prio_standard = new_prio
        entry.save()
        msg = "Prio standard updated."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def reset_prio_standard(self: InfoFetchConfigExt, bot: Bot,
                            update: Update) -> None:
        prio_standard = InfoFetchConfig.prio_standard.default
        entry = InfoFetchConfig.get_entry()
        entry.prio_standard = prio_standard
        entry.save()
        msg = "Prio standard resetted."
        send_custom_reply(msg, bot, update)
