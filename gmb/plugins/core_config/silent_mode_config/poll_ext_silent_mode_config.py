from __future__ import annotations

from typing import List, Optional

from telegram import Bot, Chat, Update, User
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.permissions import only_botadmins
from gmb.custom_message.utils import send_custom_reply
from gmb.silent_mode import SilentMode


@register_pollingbot_extension
class SilentModeConfigExt(PollBotExt):
    def get_handlers(self: SilentModeConfigExt) -> List[PriorityHandler]:
        return [
            PriorityHandler(CommandHandler('silent_mode_on', self.silent_mode_on),
                            HandlerPriority.NORMAL),
            PriorityHandler(CommandHandler('silent_mode_off', self.silent_mode_off),
                            HandlerPriority.NORMAL)
        ]

    @only_botadmins
    def silent_mode_on(self: SilentModeConfigExt, bot: Bot,
                       update: Update) -> None:
        sm = SilentMode.get_entry()
        sm.silent = True
        sm.save()
        msg = "Silent mode is turned on."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def silent_mode_off(self: SilentModeConfigExt, bot: Bot,
                        update: Update) -> None:
        sm = SilentMode.get_entry()
        sm.silent = False
        sm.save()
        msg = "Silent mode is turned off."
        send_custom_reply(msg, bot, update)
