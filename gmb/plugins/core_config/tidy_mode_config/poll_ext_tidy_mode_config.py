from __future__ import annotations

from typing import List, Optional

from telegram import Bot, Chat, Update, User
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.permissions import only_botadmins
from gmb.custom_message.utils import send_custom_reply
from gmb.custom_message.tidy_mode import TidyMode


@register_pollingbot_extension
class TidyModeConfigExt(PollBotExt):
    def get_handlers(self: TidyModeConfigExt) -> List[PriorityHandler]:
        return [
            PriorityHandler(CommandHandler('tidy_mode_on', self.tidy_mode_on),
                            HandlerPriority.NORMAL),
            PriorityHandler(CommandHandler('tidy_mode_off', self.tidy_mode_off),
                            HandlerPriority.NORMAL)
        ]

    @only_botadmins
    def tidy_mode_on(self: TidyModeConfigExt, bot: Bot,
                       update: Update) -> None:
        tidy_mode = TidyMode.get_entry()
        tidy_mode.tidy = True
        tidy_mode.save()
        msg = "Tidy mode is turned on."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def tidy_mode_off(self: TidyModeConfigExt, bot: Bot,
                        update: Update) -> None:
        tidy_mode = TidyMode.get_entry()
        tidy_mode.tidy = False
        tidy_mode.save()
        msg = "Tidy mode is turned off."
        send_custom_reply(msg, bot, update)
