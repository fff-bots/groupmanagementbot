"""Module for Leave Chat extension."""

from __future__ import annotations

from typing import List

import telegram
from telegram import (Bot, InlineKeyboardButton, InlineKeyboardMarkup, Message,
                      Update)
from telegram.ext import BaseFilter, MessageHandler

from gmb.api.datastructs import AbstractChatsConfig
from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.api.resource import Chats
from gmb.telegram_util import build_menu
from gmb.custom_message.utils import send_custom_reply

from .schema_leave_chat_config import LeaveChatConfig


class LeaveChatFilter(BaseFilter):
    """Filter for leaving a chat.

    This class contains a filter that states if the bot should leave a chat or
    not.
    """

    def filter(self: LeaveChatFilter, message: Message) -> bool:
        """The filter to leave a chat.

        If this method returns true, the bot should leave a chat.
        """
        if not LeaveChatConfig.get_entry().leave_unknown_chats:
            # unknown chat, but don't leave
            return False
        chats: List[AbstractChatsConfig] = Chats.get_chats_configs()
        m_chat = message.chat
        if m_chat.type == "private":
            # private chat
            return False
        for chat in chats:
            if int(m_chat.id) == int(chat.id):
                return False
        return True


@register_pollingbot_extension
class LeaveChatExt(PollBotExt):
    """Extension to leave unconfigured chats.

    If this extension is active, it leaves all chats that are not in the chats
    config.
    """

    def get_handlers(self: LeaveChatExt) -> List[PriorityHandler]:
        """Returns a list of Handlers that are added to the bot."""
        filt = LeaveChatFilter()
        return [
            PriorityHandler(MessageHandler(filt, self.leave_groups),
                            HandlerPriority.VERY_HIGH, 1)
        ]

    def leave_groups(self: LeaveChatExt, bot: Bot, update: Update) -> None:
        """Sends the leave message with it's buttons."""
        chat = update.effective_chat
        p_msg = "Notice: Leaving chat {} ({})"
        print(p_msg.format(str(chat.title), str(chat.id)))
        text = LeaveChatConfig.get_entry().leave_message
        btn_txt1 = LeaveChatConfig.get_entry().leave_button_1_text
        btn_link1 = LeaveChatConfig.get_entry().leave_button_1_link
        btn_txt2 = LeaveChatConfig.get_entry().leave_button_2_text
        btn_link2 = LeaveChatConfig.get_entry().leave_button_2_link
        button_list = [
            InlineKeyboardButton(btn_txt1, url=btn_link1),
            InlineKeyboardButton(btn_txt2, url=btn_link2)
        ]
        reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=1))
        try:
            send_custom_reply(text, bot, update, reply_markup=reply_markup)
        except telegram.error.Unauthorized as error:
            print("Notice in chat: {} ({}): {}".format(chat.id, chat.title,
                                                       error.message))
        try:
            bot.leave_chat(chat_id=chat.id)
        except telegram.error.Unauthorized as error:
            print("Notice in chat: {} ({}): {}".format(chat.id, chat.title,
                                                       error.message))
