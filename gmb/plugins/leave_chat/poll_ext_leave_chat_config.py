from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.permissions import only_botadmins
from gmb.telegram_util import (reset_config_member, set_config_member)
from gmb.custom_message.utils import send_custom_reply

from .schema_leave_chat_config import LeaveChatConfig


@register_pollingbot_extension
class LeaveChatConfigExt(PollBotExt):
    def get_handlers(self: LeaveChatConfigExt) -> List[PriorityHandler]:
        return [
            PriorityHandler(
                CommandHandler('toggle_leave_chats', self.toggle_leave_chats),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('reset_leave_chats', self.reset_leave_chats),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('set_leave_message', self.set_leave_message),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('reset_leave_message',
                               self.reset_leave_message),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('set_leave_button_1_text',
                               self.set_leave_button_1_text),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('reset_leave_button_1_text',
                               self.reset_leave_button_1_text),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('set_leave_button_1_link',
                               self.set_leave_button_1_link),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('reset_leave_button_1_link',
                               self.reset_leave_button_1_link),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('set_leave_button_2_text',
                               self.set_leave_button_2_text),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('reset_leave_button_2_text',
                               self.reset_leave_button_2_text),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('set_leave_button_2_link',
                               self.set_leave_button_2_link),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('reset_leave_button_2_link',
                               self.reset_leave_button_2_link),
                HandlerPriority.NORMAL)
        ]

    @only_botadmins
    def toggle_leave_chats(self: LeaveChatConfigExt, bot: Bot,
                           update: Update) -> None:
        entry = LeaveChatConfig.get_entry()
        entry.leave_unknown_chats = not entry.leave_unknown_chats
        entry.save()
        if entry.leave_unknown_chats:
            msg = "Leave chats is enabled."
        else:
            msg = "Leave chats is disabled."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def reset_leave_chats(self: LeaveChatConfigExt, bot: Bot,
                          update: Update) -> None:
        # TODO remove command from message?
        val = reset_config_member(LeaveChatConfig, "leave_unknown_chats")
        if val:
            msg = "Leave chats is enabled."
        else:
            msg = "Leave chats is disabled."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def set_leave_message(self: LeaveChatConfigExt, bot: Bot,
                          update: Update) -> None:
        set_config_member(LeaveChatConfig, "leave_message", update)
        msg = "Leave message updated."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def reset_leave_message(self: LeaveChatConfigExt, bot: Bot,
                            update: Update) -> None:
        reset_config_member(LeaveChatConfig, "leave_message")
        msg = "Start message resetted."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def set_leave_button_1_text(self: LeaveChatConfigExt, bot: Bot,
                                update: Update) -> None:
        set_config_member(LeaveChatConfig, "leave_button_1_text", update)
        msg = "Leave button 1 text updated."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def reset_leave_button_1_text(self: LeaveChatConfigExt, bot: Bot,
                                  update: Update) -> None:
        reset_config_member(LeaveChatConfig, "leave_button_1_text")
        msg = "Leave button 1 text resetted."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def set_leave_button_1_link(self: LeaveChatConfigExt, bot: Bot,
                                update: Update) -> None:
        set_config_member(LeaveChatConfig, "leave_button_1_link", update)
        msg = "Leave button 1 link updated."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def reset_leave_button_1_link(self: LeaveChatConfigExt, bot: Bot,
                                  update: Update) -> None:
        reset_config_member(LeaveChatConfig, "leave_button_1_link")
        msg = "Leave button 1 link resetted."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def set_leave_button_2_text(self: LeaveChatConfigExt, bot: Bot,
                                update: Update) -> None:
        set_config_member(LeaveChatConfig, "leave_button_2_text", update)
        msg = "Leave button 2 text updated."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def reset_leave_button_2_text(self: LeaveChatConfigExt, bot: Bot,
                                  update: Update) -> None:
        reset_config_member(LeaveChatConfig, "leave_button_2_text")
        msg = "Leave button 2 text resetted."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def set_leave_button_2_link(self: LeaveChatConfigExt, bot: Bot,
                                update: Update) -> None:
        set_config_member(LeaveChatConfig, "leave_button_2_link", update)
        msg = "Leave button 2 link updated."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def reset_leave_button_2_link(self: LeaveChatConfigExt, bot: Bot,
                                  update: Update) -> None:
        reset_config_member(LeaveChatConfig, "leave_button_2_link")
        msg = "Leave button 2 link resetted."
        send_custom_reply(msg, bot, update)
