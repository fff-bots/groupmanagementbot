from __future__ import annotations

from typing import List, Optional

from telegram import (Bot, Chat, InlineKeyboardButton, InlineKeyboardMarkup,
                      Message, Update)
from telegram.ext import BaseFilter, MessageHandler

from gmb.api.datastructs import AbstractChat
from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.api.resource import Chats, ChatType
from gmb.telegram_util import build_menu


class SendInviteLinkFilter(BaseFilter):
    """Filter to lookup if a chat contains a #chatname.

    This filter looks up a message and returns true if it contains a #chatname
    that matches the chatname of one of the chats.
    """

    def filter(self: SendInviteLinkFilter, message: Message) -> bool:
        """Filters the message if it contains a #chatname.

        If this method returns true, the bot should send an invite link.
        Ignores sending invite links from an extern chat to another extern
        chat.

        Args:
            message: The message that has triggered the bot.

        Returns:
            bool: If the bot should send an invite link.
        """
        externs = Chats.get_chats(ChatType.EXTERN)
        chat_extern = find_chat_by_message(message, externs)
        if chat_extern is not None:
            # target chat is extern
            current_chat = get_chat_info_by_chat(message.chat, externs)
            if current_chat is not None:
                # current chat is also extern
                return False
            return True
        chat_ = find_chat_by_message(message)
        if chat_ is not None:
            return True
        return False


@register_pollingbot_extension
class SendInviteLinkExt(PollBotExt):
    """Extension send invite links on #chatname."""

    def get_handlers(self: SendInviteLinkExt) -> List[PriorityHandler]:
        """Returns a list of Handlers that are added to the bot."""
        filt = SendInviteLinkFilter()
        return [
            PriorityHandler(MessageHandler(filt, self.send_chat_invite_link),
                            HandlerPriority.LOW)
        ]

    def send_chat_invite_link(self: SendInviteLinkExt, bot: Bot,
                              update: Update) -> None:
        """Sends the chat invite link of a chat.

        It sends a message with the chat invite link of the chat described by #chatname
        into the chat where the update comes from.

        Args:
            bot: The bot that sends the message.
            update: The update on that the command was triggered.
        """
        message = update.message
        chat_ = find_chat_by_message(message)
        if chat_ is not None:
            link = chat_.invite_link
            text = str(chat_.title)
            if link is not None and message.text != str(chat_.title):
                link = str(link)
                button_list = [InlineKeyboardButton("Join", url=link)]
                reply_markup = InlineKeyboardMarkup(
                    build_menu(button_list, n_cols=1))
                bot.send_message(chat_id=message.chat_id,
                                 text=text,
                                 reply_markup=reply_markup)


def find_chat_by_message(message: Message,
                         chats: Optional[List[AbstractChat]] = None
                         ) -> Optional[AbstractChat]:
    """Finds a chat that contains the #chatname of the message.

    Gets a message and a list of NetworkChats. Searches the message for
    #chatname of all chats and returns the matching chat.

    Args:
        message: The message that contains the #chatname.
    """
    if chats is None:
        chats = Chats.get_chats()
    chat_ = None
    for chat in chats:
        if chat.name is None:
            continue
        if message.text is None:
            continue
        chat_name = ("#" + chat.name).lower()
        if chat_name in message.text.lower():
            # if chat has a title, don't send the link to the current chat
            if message.chat.title is not None:
                if chat_name not in message.chat.title.lower():
                    if chat_ is None or len(chat_.name) < len(chat.name):
                        chat_ = chat
            else:
                if chat_ is None or len(chat_.name) < len(chat.name):
                    chat_ = chat
    return chat_


def get_chat_info_by_chat(chat: Chat,
                          chats: Optional[List[AbstractChat]] = None
                          ) -> Optional[AbstractChat]:
    """Finds the chat info that represents the telegram chat.

    Args:
        message: The message that contains the #chatname.
        chats: A list of chats that are searched.
    """
    if chats is None:
        chats = Chats.get_chats()
    for current_chat in chats:
        if current_chat.id == chat.id:
            return current_chat
    return None
