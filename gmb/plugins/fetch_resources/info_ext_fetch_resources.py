from __future__ import annotations

from typing import List, Type

import telegram
from telegram import Bot

from gmb.api.datastructs import AbstractChatsConfig
from gmb.api.infofetch import (InfoFetchExt, InfoFetchExtensions,
                               register_infofetch_extension)
from gmb.api.resource import ResourceExt, ResourceExtensions, fetch_all
from gmb.environment import Environment
from gmb.status import DestructionStatus


@register_infofetch_extension
class GetResourcesExtension(InfoFetchExt):
    @staticmethod
    def run(bot: Bot):
        fetch_all(bot)
