from __future__ import annotations

from typing import List

from telegram import (Bot, InlineKeyboardButton, InlineKeyboardMarkup,
                      InlineQueryResultArticle, Update)
from telegram.ext import InlineQueryHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.api.resource import Chats
from gmb.telegram_util import build_menu


@register_pollingbot_extension
class InlineChatlinkExt(PollBotExt):
    """Extension providing chatlinks via inline queries"""

    def get_handlers(self: InlineChatlinkExt) -> List[PriorityHandler]:
        """Returns a list of Handlers that are added to the bot."""
        return [
            PriorityHandler(InlineQueryHandler(self.handleInlineRequest),
                            HandlerPriority.NORMAL),
        ]

    def handleInlineRequest(self: InlineChatlinkExt, bot: Bot, update: Update):
        chats = Chats.get_chats()
        chats = sorted(chats, key=lambda chat: chat.name)
        chats = sorted(chats, key=lambda chat: chat.prio)
        query = update["inline_query"]
        chats = [
            c for c in chats if c.invite_link is not None
            and query["query"].lower() in c.name.lower()
        ]
        results = map(self.chatToResult, chats)
        bot.answerInlineQuery(query["id"], results)

    def chatToResult(self: InlineChatlinkExt, chat):
        button_list = [InlineKeyboardButton("Join", url=chat.invite_link)]
        reply_markup = InlineKeyboardMarkup(build_menu(button_list, n_cols=1))
        return InlineQueryResultArticle(
            chat.id,
            title="#" + chat.name,
            input_message_content={"message_text": chat.title},
            reply_markup=reply_markup)
