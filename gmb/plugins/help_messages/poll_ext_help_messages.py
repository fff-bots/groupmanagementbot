from __future__ import annotations

from dataclasses import dataclass
from typing import List, Optional

from telegram import Bot, ReplyKeyboardRemove, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.environment import Environment
from gmb.permissions import (only_botadmins, only_groupadmins, only_superadmins, only_networkadmins)
from gmb.custom_message.utils import send_custom_reply
from gmb.utils_design import separation_line
from gmb.silent_mode import silent_mode
from gmb.custom_message.utils import send_tidy_long_reply

from .schema_help_messages_config import HelpMessagesConfig


@dataclass
class CommandDescription:
    command: str
    description: str


def read_command_file(filename: str) -> List[Optional[CommandDescription]]:
    lines = None
    with open(filename, "r") as file:
        lines = file.read()
    cds: List[Optional[CommandDescription]] = []
    for line in lines.split("\n"):
        tmp = line.split("-", 1)
        if len(tmp) != 2:
            cds.append(None)
            continue
        cmd = tmp[0].strip()  # remove trailing spaces
        desc = tmp[1].strip()
        cm_desc = CommandDescription(cmd, desc)
        cds.append(cm_desc)
    return cds


@register_pollingbot_extension
class HelpMessagesExt(PollBotExt):
    """Extension for help messages."""

    def get_handlers(self: HelpMessagesExt) -> List[PriorityHandler]:
        return [
            PriorityHandler(CommandHandler('help', self.help),
                            HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('help_botadmin', self.help_botadmin),
                HandlerPriority.NORMAL),
            PriorityHandler(CommandHandler('help_admin', self.help_admin),
                            HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('help_superadmin', self.help_superadmin),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('help_groupadmin', self.help_groupadmin),
                HandlerPriority.NORMAL)
        ]

    @staticmethod
    def __build_help_string(cds: List[Optional[CommandDescription]]) -> str:
        msg = "\n{}".format(separation_line())
        content = False
        for cm_desc in cds:
            if not cm_desc:
                msg += "\n{}".format(separation_line())
                continue
            content = True
            tmp = "\n/{cmd} - <i>{desc}</i>"
            msg += tmp.format(cmd=cm_desc.command, desc=cm_desc.description)
        if content:
            return msg
        return "\n<i>Keine Befehle vorhanden.</i>"

    @silent_mode
    def help(self: HelpMessagesExt, bot: Bot, update: Update) -> None:
        """Sends the help message.

        Args:
            bot: The bot that sends the message.
            update: The update on that the command was triggered.
        """
        config = Environment.get().static_config
        filename = config.doc_telegram_commands_file
        cds = read_command_file(filename)
        msg: str = HelpMessagesConfig.get_entry().pre_help_message
        msg += HelpMessagesExt.__build_help_string(cds)
        msg += "\n" + HelpMessagesConfig.get_entry().past_help_message + "\n"
        msg = msg.strip()
        send_tidy_long_reply(msg, bot, update)

    @only_botadmins
    def help_botadmin(self: HelpMessagesExt, bot: Bot, update: Update) -> None:
        config = Environment.get().static_config
        filename = config.doc_telegram_commands_botadmins_file
        cds = read_command_file(filename)
        msg: str = HelpMessagesConfig.get_entry().pre_help_message_botadmin
        msg += HelpMessagesExt.__build_help_string(cds)
        msg += "\n" + HelpMessagesConfig.get_entry(
        ).past_help_message_botadmin + "\n"
        msg = msg.strip()
        send_custom_reply(msg, bot, update)

    @only_networkadmins
    def help_admin(self: HelpMessagesExt, bot: Bot, update: Update) -> None:
        config = Environment.get().static_config
        filename = config.doc_telegram_commands_admins_file
        cds = read_command_file(filename)
        msg: str = HelpMessagesConfig.get_entry().pre_help_message_admin
        msg += HelpMessagesExt.__build_help_string(cds)
        msg += "\n" + HelpMessagesConfig.get_entry(
        ).past_help_message_admin + "\n"
        msg = msg.strip()
        send_custom_reply(msg, bot, update)

    @only_superadmins
    def help_superadmin(self: HelpMessagesExt, bot: Bot,
                        update: Update) -> None:
        config = Environment.get().static_config
        filename = config.doc_telegram_commands_superadmins_file
        cds = read_command_file(filename)
        msg: str = HelpMessagesConfig.get_entry().pre_help_message_superadmin
        msg += HelpMessagesExt.__build_help_string(cds)
        msg += "\n" + HelpMessagesConfig.get_entry(
        ).past_help_message_superadmin + "\n"
        msg = msg.strip()
        send_custom_reply(msg, bot, update)

    @only_groupadmins
    def help_groupadmin(self: HelpMessagesExt, bot: Bot,
                        update: Update) -> None:
        config = Environment.get().static_config
        filename = config.doc_telegram_commands_groupadmins_file
        cds = read_command_file(filename)
        msg: str = HelpMessagesConfig.get_entry().pre_help_message_groupadmin
        msg += HelpMessagesExt.__build_help_string(cds)
        msg += "\n" + HelpMessagesConfig.get_entry(
        ).past_help_message_groupadmin + "\n"
        msg = msg.strip()
        send_custom_reply(msg, bot, update)
