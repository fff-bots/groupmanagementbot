from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.permissions import only_botadmins, only_superadmins
from gmb.telegram_util import reset_config_member, set_config_member
from gmb.custom_message.utils import send_custom_reply

from .schema_help_messages_config import HelpMessagesConfig


@register_pollingbot_extension
class HelpMessagesConfigExt(PollBotExt):
    def get_handlers(self: HelpMessagesConfigExt) -> List[PriorityHandler]:
        out = []
        for perm in ["", "_botadmin", "_admin", "_superadmin", "_groupadmin"]:
            for meth in ["set", "reset"]:
                for pos in ["pre", "past"]:
                    func_name = meth + "_" + pos + "_help_msg" + perm
                    func = self.abstract_message(meth, pos, perm)
                    out.append(
                        PriorityHandler(CommandHandler(func_name, func),
                                        HandlerPriority.NORMAL))
        return out

    def abstract_message(self: HelpMessagesConfigExt, method: str, pos_: str,
                         perm_name: str):
        def intern(bot: Bot, update: Update) -> None:
            # set_pre_help_message_admin/reset_past_help_message
            db_member = "{pos_}_help_message{perm_name}".format(
                pos_=pos_, perm_name=perm_name)
            # generate output message
            if method == "reset":
                meth_desc = "resetted"
                reset_config_member(HelpMessagesConfig, db_member)
            elif method == "set":
                set_config_member(HelpMessagesConfig, db_member, update)
                meth_desc = "updated"
            pos = pos_[0].upper() + pos_[1:]
            name = ""
            if perm_name:
                name = "-" + perm_name[1].upper() + perm_name[2:]
            msg = "{pos}{name}-Help message {meth_desc}.".format(
                name=name, meth_desc=meth_desc, pos=pos)
            send_custom_reply(text=msg, bot=bot, update=update)

        if "_superadmin" == perm_name:
            return only_superadmins(intern)
        return only_botadmins(intern)
