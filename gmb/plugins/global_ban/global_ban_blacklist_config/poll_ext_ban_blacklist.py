from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.permissions import only_networkadmins
from gmb.custom_message.utils import send_tidy_error, send_custom_reply
from gmb.utils_design import usr_to_str
from gmb.datastructs.factories import get_user_info

from gmb.plugins.global_ban.global_ban_network.exceptions import WrongFormatException
from gmb.plugins.global_ban.global_ban_network.utils_telegram import ParseMessages

from gmb.plugins.global_ban.global_ban_blacklist.schema_ban_blacklist import BanBlacklisted


@register_pollingbot_extension
class BanBlacklistExt(PollBotExt):

    def get_handlers(self: BanBlacklistExt) -> List[PriorityHandler]:
        return [
            PriorityHandler(CommandHandler('add_to_ban_blacklist', self.add_to_ban_blacklist),
                            HandlerPriority.NORMAL),
            PriorityHandler(CommandHandler('remove_from_ban_blacklist', self.remove_from_ban_blacklist),
                            HandlerPriority.NORMAL),
        ]

    @staticmethod
    def send_error_add(bot: Bot, update: Update) -> None:
        msg = "Wrong format. Reply to a message of the user you like to add"
        msg += " to the ban blacklist."
        send_tidy_error(msg, bot, update)

    @staticmethod
    def send_error_remove(bot: Bot, update: Update) -> None:
        msg = "Wrong format. Reply to a message of the user you like to remove"
        msg += " from the ban blacklist."
        send_tidy_error(msg, bot, update)

    @staticmethod
    def send_error_remove_not_found(bot: Bot, update: Update) -> None:
        msg = "User is not in the ban blacklist."
        send_tidy_error(msg, bot, update)

    @only_networkadmins
    def add_to_ban_blacklist(self, bot: Bot, update: Update) -> None:
        """."""
        msg = update.effective_message
        try:
            users = ParseMessages.get_users_from_message(bot, msg)
        except WrongFormatException:
            BanBlacklistExt.send_error_add(bot, update)
            return
        for user in users:
            user_info = get_user_info(bot, user)
            ban_blacklisted = BanBlacklisted.update_or_create("id", **user_info)
            msg = "Added {} to ban blacklist.".format(usr_to_str(ban_blacklisted))
            send_custom_reply(msg, bot, update)

    @only_networkadmins
    def remove_from_ban_blacklist(self, bot: Bot, update: Update) -> None:
        """."""
        msg = update.effective_message
        try:
            users = ParseMessages.get_users_from_message(bot, msg)
        except WrongFormatException:
            print("error")
            BanBlacklistExt.send_error_remove(bot, update)
            return
        for user in users:
            ban_blacklisted = BanBlacklisted.try_get_by_id(user.id)
            if not ban_blacklisted:
                BanBlacklistExt.send_error_remove_not_found(bot, update)
                return
            user_str = usr_to_str(ban_blacklisted)
            query = BanBlacklisted.delete().where(
                BanBlacklisted.id == user.id)
            query.execute()
            msg = "Removed {} from ban blacklist.".format(user_str)
            send_custom_reply(msg, bot, update)
