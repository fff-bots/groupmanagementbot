from peewee import BigIntegerField, CharField, IntegerField, TextField

from gmb.api.database import BaseModel, register_plugin_config


@register_plugin_config
class NetworkBannedUsers(BaseModel):
    bannedid = IntegerField(primary_key=True, null=False)
    bannedname = CharField(null=True)
    bannedusername = CharField(null=True)
    chatid = BigIntegerField(null=False)
    chatname = CharField(null=False)
    chattype = CharField(null=False)
    fromid = IntegerField(null=False)
    fromname = CharField(null=True)
    fromusername = CharField(null=True)
    messageid = BigIntegerField(null=False)
    messagereplyid = BigIntegerField(null=True)
    reason = CharField(null=True)
    date = CharField(null=False)
    replytext = TextField(null=True)
