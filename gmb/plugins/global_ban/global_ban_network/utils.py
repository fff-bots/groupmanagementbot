"""This module contains utilities for the plugin."""

from enum import Enum, auto


class ActionStatus(Enum):
    """The status of an action."""
    SUCCESSFULL = auto()
    NOT_POSSIBLE = auto()
