"""This module contains methods to wrap the telegram api."""

from __future__ import annotations

import re
from typing import List, Optional, Tuple
from enum import Enum

from telegram import Bot, Message, TelegramError, User
from telegram.error import BadRequest, Unauthorized

from gmb.permissions import Admin
from gmb.environment import Environment
from gmb.telegram_util import ChatMemberStatus

from gmb.plugins.global_ban.global_ban_blacklist.schema_ban_blacklist import BanBlacklisted

from .exceptions import WrongFormatException
from .utils import ActionStatus


class TelegramBan:
    """Methods to ban and unban a user."""

    @staticmethod
    def delete_message(bot: Bot, chat_id: int,
                       message_id: int) -> Tuple[ActionStatus, Optional[str]]:
        """Deletes a message."""
        try:
            bot.delete_message(chat_id, message_id)
        except TelegramError as ex:
            return (ActionStatus.NOT_POSSIBLE, str(ex))
        except Exception as ex:
            print(
                "ERROR: uncatched exception occurred while deleting a message:"
            )
            print("{}: {}".format(ex.__class__, ex))
            return (ActionStatus.NOT_POSSIBLE, str(ex))
        return (ActionStatus.SUCCESSFULL, None)

    @staticmethod
    def concrete_ban(bot: Bot, chatid: int,
                     userid: int) -> Tuple[ActionStatus, Optional[str]]:
        """Bans a user in a chat."""
        try:
            chat_member = bot.get_chat_member(chatid, userid)
            if chat_member == ChatMemberStatus.ADMINISTRATOR.value:
                return (ActionStatus.NOT_POSSIBLE, "User is group admin.")
            if chat_member == ChatMemberStatus.CREATOR.value:
                return (ActionStatus.NOT_POSSIBLE, "User is creator of the group.")
            bot.kick_chat_member(chatid, userid)
        except BadRequest as ex:
            return (ActionStatus.NOT_POSSIBLE, str(ex))
        except Unauthorized as ex:
            return (ActionStatus.NOT_POSSIBLE, str(ex))
        except Exception as ex:
            print("ERROR: uncatched exception occurred while banning a user:")
            print("{}: {}".format(ex.__class__, ex))
            return (ActionStatus.NOT_POSSIBLE, str(ex))
        return (ActionStatus.SUCCESSFULL, None)

    @staticmethod
    def concrete_unban(bot: Bot, chatid: int,
                       userid: int) -> Tuple[ActionStatus, Optional[str]]:
        """Unbans a user in a chat."""
        try:
            chat_member = bot.get_chat_member(chatid, userid)
            # except TelegramError as e:
            if chat_member.status == ChatMemberStatus.KICKED.value:
                bot.unban_chat_member(chatid, userid)
            else:
                print("Don't unban user because he was not kicked in this group.")
        except Exception as ex:
            print(
                "ERROR: uncatched exception occurred while unbanning a user:")
            print("{}: {}".format(ex.__class__, ex))
            return (ActionStatus.NOT_POSSIBLE, str(ex))
        return (ActionStatus.SUCCESSFULL, None)


class ParseMessages:
    """Class to extract information from telegram messages."""

    @staticmethod
    def get_reason(msg: Message) -> Optional[str]:
        """Splits the reason out of the message."""
        reason: Optional[str] = None
        split = msg.text.split(" ")
        if len(split) > 1:
            reason = " ".join(split[1:])
        else:
            reason = None
        return reason

    @staticmethod
    def get_starts_with_id(start: str, text: str) -> Optional[int]:
        """Splits the user id out of the message."""
        match = re.search("/{}([0-9]+)".format(start), text)
        if match:
            return int(match.groups()[0])
        return None

    @staticmethod
    def __remove_admins_filter(bot: Bot):
        admin_ids = [a.id for a in Admin.get_all()]
        static_config = Environment.get().static_config
        admin_ids.append(static_config.master_admin_id)
        bot_user = bot.get_me()
        admin_ids.append(bot_user.id)

        def intern(user: User) -> bool:
            if user.id in admin_ids:
                return False
            return True
        return intern

    @staticmethod
    def __remove_blacklisted_filter():
        blacklisted_ids = [a.id for a in BanBlacklisted.get_all()]

        def intern(user: User) -> bool:
            if user.id in blacklisted_ids:
                return False
            return True
        return intern

    @staticmethod
    def remove_admins_from_list(bot: Bot, users: List[User]) -> List[User]:
        """Removes admins from the user list.

        Removes all network admins, bot admins and the master admin and the bot from
        the users list. The outgoing users are only normal users or groupadmins.
        """
        filt = ParseMessages.__remove_admins_filter(bot)
        users_out: List[User] = list(filter(filt, users))
        return users_out

    @staticmethod
    def remove_blacklisted_from_list(users: List[User]) -> List[User]:
        """Removes admins from the user list.

        Removes all network admins, bot admins and the master admin and the bot from
        the users list. The outgoing users are only normal users or groupadmins.
        """
        filt = ParseMessages.__remove_blacklisted_filter()
        users_out: List[User] = list(filter(filt, users))
        return users_out

    @staticmethod
    def clean_ban_list(bot: Bot, users: List[User]):
        """Removes admins and blacklisted users from the list."""
        users = ParseMessages.remove_admins_from_list(bot, users)
        users = ParseMessages.remove_blacklisted_from_list(users)
        return users

    @staticmethod
    def get_users_from_message(bot: Bot, msg: Message) -> List[User]:
        """Returns a list with all users from the message.

        The list contains the user, who wrote the replied to message. If the replied to
        message is a join-message, all users added with this join message are removed. If
        the replied to message is a leave message, the user removed with this leave
        message is removed.
        """
        if msg.reply_to_message is None:
            raise WrongFormatException()
        users: List[User] = []
        if msg.reply_to_message.new_chat_members:
            # reply auf joined message
            for tg_user in msg.reply_to_message.new_chat_members:
                users.append(tg_user)
        if msg.reply_to_message.left_chat_member:
            # reply auf leaved message
            users = [msg.reply_to_message.left_chat_member]
        if not users:
            # reply auf Nachricht von Nutzer
            users = [msg.reply_to_message.from_user]
        return users

    @staticmethod
    def get_clean_users_from_message(bot: Bot, msg: Message) -> List[User]:
        """Returns a list with all users that should be banned.

        The list contains the user, who wrote the replied to message. If the replied to
        message is a join-message, all users added with this join message are removed. If
        the replied to message is a leave message, the user removed with this leave
        message is removed. Network admins, superadmins and bot admins are not added to
        this list, so that all users can be banned.
        """
        users = ParseMessages.get_users_from_message(bot, msg)
        users = ParseMessages.clean_ban_list(bot, users)
        return users

    @staticmethod
    def get_banned_users_from_message(message: Message,
                                      userids: List[int]) -> List[User]:
        """Returns a list of the user of the message, or the joined users."""
        users: List[User] = []
        user = message.from_user
        if user and user.id in userids:
            users.append(user)
        if message.new_chat_members:
            # reply auf joined message
            for tg_user in message.new_chat_members:
                if tg_user.id in userids:
                    users.append(tg_user)
        return users
