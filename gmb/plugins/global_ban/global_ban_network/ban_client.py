"""This module contains methods to ban users and add and remove them to the ban list."""

from __future__ import annotations

from typing import Dict, List, Optional, Tuple

from telegram import Bot, Message, User

from gmb.api.datastructs import AbstractChat
from gmb.api.resource import Chats
from gmb.plugins.network.network.schema_network import NetworkChat
from gmb.utils_design import chat_to_str, usr_to_str, list_char

from .exceptions import WrongFormatException
from .schema_network_ban import NetworkBannedUsers
from .utils import ActionStatus
from .utils_telegram import TelegramBan

ChatReasonList = List[Tuple[AbstractChat, Optional[str]]]


class BanStatusList:
    """A list with bans in chats and it's status."""

    def __init__(self, unban: bool = False):
        self.__status: Dict[ActionStatus, ChatReasonList] = {}
        self.already_banned = False
        self.unban = unban

    def add(self,
            chat: AbstractChat,
            ban_status: ActionStatus,
            reason: Optional[str] = None):
        """Add the status of a ban in a chat to the list."""
        if ban_status not in self.__status.keys():
            self.__status[ban_status] = []
        if chat not in self.__status[ban_status]:
            self.__status[ban_status].append((chat, reason))

    def get_stats(self, ban_status: ActionStatus) -> Optional[ChatReasonList]:
        """Get all chats that has a specific status."""
        if ban_status in self.__status.keys():
            return self.__status[ban_status]
        return None

    def get_all_stats(self) -> Dict[ActionStatus, ChatReasonList]:
        """Get all stats with chats."""
        return self.__status

    def to_string_banned(self, user: User) -> str:
        msg = ""
        chats = self.get_stats(ActionStatus.SUCCESSFULL)
        if chats:
            if self.unban:
                tmp = "\nUnbanned:"
            else:
                tmp = "\nBanned:"
            msg += tmp.format(usr_to_str(user))
            for chat, _ in chats:
                msg += "\n{} {}".format(list_char(), chat_to_str(chat))
        return msg

    def to_string_not_possible(self, user: User) -> str:
        msg = ""
        chats = self.get_stats(ActionStatus.NOT_POSSIBLE)
        if chats:
            if self.unban:
                tmp = "\nNot unbanned:"
            else:
                tmp = "\nNot banned:"
            msg += tmp.format(usr_to_str(user))
            for chat, reason in chats:
                msg += "\n{} {}".format(list_char(), chat_to_str(chat))
                if reason:
                    msg += " ({})".format(reason)
        return msg

    def to_string_already_banned(self, user: User) -> str:
        if self.already_banned:
            if self.unban:
                msg = "\nAlready unbanned User:"
            else:
                msg = "\nAlready banned User:"
            msg += "\n{} {}".format(list_char(), usr_to_str(user))
            return msg
        return ""

    def to_string(self, user: User) -> str:
        msg = "<strong>User </strong>{}".format(usr_to_str(user))
        msg += "\n" + self.to_string_banned(user)
        msg += "\n" + self.to_string_not_possible(user)
        return msg


class UsersBanStats:
    """The ban stats of users.

    A user is banned or not banned (or other) in chats. This is a collection of
    users with it's dependent ban stats in chats.
    """

    def __init__(self, unban: bool = False):
        self.ban_stats: Dict[User, BanStatusList] = {}
        self.unban = unban

    def to_string_test(self) -> str:
        """Returns a string representation of all banned users."""

    def to_string_already_banned(self) -> str:
        """Returns a string representation of the users, that are already
        banned."""
        msgs: List[str] = []
        for user, ban_status_list in self.ban_stats.items():
            if ban_status_list.already_banned:
                msg = "{}".format(usr_to_str(user))
                msgs.append(msg)
        if not msgs:
            return ""
        if len(msgs) == 1:
            if self.unban:
                msg = "\nAlready unbanned user:"
            else:
                msg = "\nAlready banned user:"
            for tmp in msgs:
                msg += "\n{} {}".format(list_char(), tmp)
            return msg
        if self.unban:
            msg = "\nAlready unbanned users:"
        else:
            msg = "\nAlready banned users:"
        for tmp in msgs:
            msg += "\n{} {}".format(list_char(), tmp)
        return msg

    def to_string(self) -> str:
        """Returns a string representation of the object."""
        msg = ""
        msg += self.to_string_already_banned()
        for user, ban_status_list in self.ban_stats.items():
            msg += "\n\n" + ban_status_list.to_string(user)
        return msg


class BanList:
    """Methods to add and remove users to ban list."""

    @staticmethod
    def add_user(user: User, msg: Message, reason: Optional[str]) -> bool:
        """Adds a user to the banned-users database.

        Returns False if the user is already added, otherwise True.
        """
        if msg.reply_to_message is not None:
            reply_to_message_id = msg.reply_to_message.message_id
            replytext = msg.reply_to_message.text
        else:
            reply_to_message_id = None
            replytext = None
        chat = msg.chat
        from_user = msg.from_user
        if NetworkBannedUsers.try_get_by_id(user.id):
            # already banned
            return False
        NetworkBannedUsers.create(bannedid=user.id,
                                  bannedname=user.full_name,
                                  bannedusername=user.username,
                                  chatid=chat.id,
                                  chatname=chat.title,
                                  chattype=chat.type,
                                  fromid=from_user.id,
                                  fromname=from_user.full_name,
                                  fromusername=from_user.username,
                                  messageid=msg.message_id,
                                  messagereplyid=reply_to_message_id,
                                  reason=reason,
                                  date=msg.date.isoformat(),
                                  replytext=replytext)
        return True

    @staticmethod
    def remove_user(userid: int) -> bool:
        """Removes a user from the banned-users database.

        Returns True if the user is removed, otherwise False.
        """
        if not NetworkBannedUsers.try_get_by_id(userid):
            return False
        query = NetworkBannedUsers.delete().where(
            NetworkBannedUsers.bannedid == userid)
        query.execute()
        return True


class BanClient:
    """Class to perform bans in the network and to blacklist users."""

    @staticmethod
    def network_ban(bot: Bot, userid: int,
                    unban: bool = False) -> BanStatusList:
        """Banns a user in all network groups.

        If unban is true, the user will be unbanned in all chats.
        """
        ban_status_list = BanStatusList(unban)
        chats = []
        chats_all = Chats.get_chats_configs()
        for chat in chats_all:
            if Chats.subscribed_global_ban(chat.id):
                chats.append(chat)
        for chat in chats:
            if unban:
                ban_status, reason = TelegramBan.concrete_unban(
                    bot, chat.id, userid)
            else:
                ban_status, reason = TelegramBan.concrete_ban(
                    bot, chat.id, userid)
            ban_status_list.add(chat, ban_status, reason)
        return ban_status_list

    @staticmethod
    def ban_user(bot: Bot,
                 user: User,
                 msg: Message,
                 reason: Optional[str] = None,
                 unban: bool = False) -> BanStatusList:
        """Bans a user in all network groups and adds the user to the ban list."""
        if unban:
            already_banned = not BanList.remove_user(user.id)
        else:
            already_banned = not BanList.add_user(user, msg, reason)
        ban_status_list = BanClient.network_ban(bot, user.id, unban)
        ban_status_list.already_banned = already_banned
        return ban_status_list

    @staticmethod
    def ban_users(bot: Bot,
                  users: List[User],
                  msg: Message,
                  reason: Optional[str] = None,
                  unban: bool = False) -> UsersBanStats:
        """Adds all users to the ban list and then banns them in all network groups."""
        users_ban_stats = UsersBanStats(unban)
        for user in users:
            ban_status_list = BanClient.ban_user(bot, user, msg, reason, unban)
            users_ban_stats.ban_stats[user] = ban_status_list
        return users_ban_stats
