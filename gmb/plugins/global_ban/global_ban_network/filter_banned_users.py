"""This module contains a filter that can be used to find out if a user that writes a
message or joins is a banned user."""

from typing import List, Optional

from telegram import Message
from telegram.ext import BaseFilter

from gmb.api.resource import Chats

from .schema_network_ban import NetworkBannedUsers
from .utils_telegram import ParseMessages


class BannedUserFilter(BaseFilter):
    """Returns True if the user is banned.

    Has two modes. The only_join_messages mode, than it will only be triggered on join
    messages. And the second mode, if only_join_messages is False, it will be triggered
    on every message. It also Caches the list of banned users and updates the cache on
    every join message, so only if only_join_messages is activated. This causes that
    if only_join_messages is False, it will only use the cached banned users list.
    """
    banned_users: Optional[List[int]] = None

    def __init__(self, only_join_messages: bool = False):
        self.update_filter = False
        self.only_join_messages = only_join_messages

    @staticmethod
    def get_all_userids() -> List[int]:
        """Updates the banned users cache and returns it."""
        users = [user.bannedid for user in NetworkBannedUsers.get_all()]
        BannedUserFilter.banned_users = users
        return BannedUserFilter.banned_users

    def filter(self, update: Message) -> bool:
        if self.only_join_messages:
            if not update.new_chat_members:
                return False
        chat_id = update.chat_id
        chat_subscribed = Chats.subscribed_global_ban(chat_id)
        if not chat_subscribed:
            return False
        if self.only_join_messages or BannedUserFilter.banned_users is None:
            # cache banned user ids till someone joins, or startup of the program
            userids = BannedUserFilter.get_all_userids()
        else:
            userids = BannedUserFilter.banned_users
        users = ParseMessages.get_banned_users_from_message(update, userids)
        if users:
            return True
        return False
