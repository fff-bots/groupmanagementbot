from __future__ import annotations

from typing import List
from datetime import datetime

from telegram import Bot, Message, Update
from telegram.ext import CommandHandler, MessageHandler

from gmb.api.filter import StartWithFilter
from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.api.resource import Chats
from gmb.permissions import only_networkadmins
from gmb.telegram_util import create_message_link
from gmb.custom_message.utils import (send_tidy_short_reply, send_tidy_error,
                                      send_tidy_reply, send_custom_reply)
from gmb.utils_design import chat_to_str, usr_to_str, list_char

from .ban_client import BanClient, BanList
from .error_messages import *
from .exceptions import WrongFormatException
from .filter_banned_users import BannedUserFilter
from .schema_network_ban import NetworkBannedUsers
from .utils import ActionStatus
from .utils_telegram import ParseMessages, TelegramBan



@register_pollingbot_extension
class BanExt(PollBotExt):
    """This extension contains all handlers that are used to ban users in the network.

    Ban eines Users:
    1. User in DB eintragen
    2. User in allen Gruppen bannen
    3. "User wurde erfolgreich gebannt."/"Beim Bannen traten in folgenden Gruppen Fehler
       auf: "
    """

    def get_handlers(self: BanExt) -> List[PriorityHandler]:
        return [
            PriorityHandler(CommandHandler('gban', self.gban),
                            HandlerPriority.NORMAL),
            PriorityHandler(CommandHandler('gban_debug', self.gban_debug),
                            HandlerPriority.NORMAL),
            PriorityHandler(CommandHandler('gunban', self.gunban),
                            HandlerPriority.NORMAL),
            PriorityHandler(CommandHandler('gunban_debug', self.gunban_debug),
                            HandlerPriority.NORMAL),
            PriorityHandler(CommandHandler('banned_users', self.banned_users),
                            HandlerPriority.NORMAL),
            PriorityHandler(
                MessageHandler(StartWithFilter('gunban_'),
                               self.gunban_start_with),
                HandlerPriority.LOW),  # after gunban_debug
            PriorityHandler(
                MessageHandler(StartWithFilter('ban_info_'), self.ban_info),
                HandlerPriority.NORMAL),
            PriorityHandler(
                MessageHandler(StartWithFilter('ban_info_replied_to_message_'),
                               self.ban_info_replied_to_message),
                HandlerPriority.HIGH),
            PriorityHandler(
                MessageHandler(BannedUserFilter(True), self.ban_user),
                HandlerPriority.VERY_HIGH, 2),
            PriorityHandler(
                MessageHandler(BannedUserFilter(False), self.delete_message),
                HandlerPriority.HIGH, 2)
        ]

    def delete_message(self: BanExt, bot: Bot, update: Update) -> None:
        """Deletes all new send messages from banned users."""
        msg = update.message
        TelegramBan.delete_message(bot, msg.chat.id, msg.message_id)

    def ban_user(self: BanExt, bot: Bot, update: Update) -> None:
        """Bans the joined user if he is in the ban list."""
        userids = BannedUserFilter.banned_users
        if not userids:
            print("An error occurred. Can't ban users, if there are no users.")
        else:
            users = ParseMessages.get_banned_users_from_message(
                update.message, userids)
            for user in users:
                msg = update.message
                # ban user
                c_ban = TelegramBan.concrete_ban(bot, msg.chat.id, user.id)
                # delete join message
                TelegramBan.delete_message(bot, msg.chat.id, msg.message_id)
                # print error messages
                if c_ban[0] == ActionStatus.SUCCESSFULL:
                    print("Removed user: {}".format(usr_to_str(user)))
                else:
                    msg = "Error removing banned user: {}".format(
                        usr_to_str(user))
                    send_custom_reply(msg, bot, update,
                                      disable_web_page_preview=True)

    @only_networkadmins
    def gban(self: BanExt, bot: Bot, update: Update) -> None:
        """Bans the user replied to.

        If the replied to message is a join message, all joined users will be banned.
        """
        # TODO: dont ban admins
        msg = update.message
        reason = ParseMessages.get_reason(msg)
        try:
            users = ParseMessages.get_clean_users_from_message(bot, msg)
            if not users:
                msg = "You can't ban admins or the bot."
                send_tidy_error(msg, bot, update)
                return
            BanClient.ban_users(bot, users, msg, reason)
        except WrongFormatException:
            send_error_ban(bot, update)
            return
        if len(users) == 1:
            msg = "Banned user:"
        else:
            msg = "Banned users:"
        for user in users:
            msg += "\n{} {}".format(list_char(), usr_to_str(user))
        send_tidy_short_reply(msg, bot, update, disable_web_page_preview=True)

    @only_networkadmins
    def gban_debug(self: BanExt, bot: Bot, update: Update) -> None:
        """Bans the user replied to with status message.

        If the replied to message is a join message, all joined users will be banned.
        """
        # dont ban yourself
        msg = update.message
        reason = ParseMessages.get_reason(msg)
        try:
            users = ParseMessages.get_clean_users_from_message(bot, msg)
            if not users:
                msg = "You can't ban admins or the bot."
                send_tidy_error(msg, bot, update)
                return
            users_ban_stats = BanClient.ban_users(bot, users, msg, reason)
        except WrongFormatException:
            send_error_ban(bot, update)
            return
        msg = users_ban_stats.to_string()
        send_tidy_reply(msg, bot, update, disable_web_page_preview=True)

    @only_networkadmins
    def gunban(self: BanExt, bot: Bot, update: Update) -> None:
        """Unbans the user replied to.

        If the replied to message is a join message, all joined users will be unbanned.
        """
        msg = update.message
        try:
            users = ParseMessages.get_clean_users_from_message(bot, msg)
            if not users:
                msg = "You can't unban admins or the bot."
                send_tidy_error(msg, bot, update)
                return
            BanClient.ban_users(bot, users, msg, unban=True)
        except WrongFormatException:
            send_error_unban(bot, update)
            return
        if len(users) == 1:
            msg = "Unbanned user:"
        else:
            msg = "Unbanned users:"
        for user in users:
            msg += "\n{} {}".format(list_char(), usr_to_str(user))
        send_tidy_short_reply(msg, bot, update, disable_web_page_preview=True)

    @only_networkadmins
    def gunban_debug(self: BanExt, bot: Bot, update: Update) -> None:
        """Unbans the user replied to with status message.

        If the replied to message is a join message, aall joined users will be unbanned.
        """
        msg = update.message
        try:
            users = ParseMessages.get_clean_users_from_message(bot, msg)
            if not users:
                msg = "You can't unban admins or the bot."
                send_tidy_error(msg, bot, update)
                return
            users_unban_stats = BanClient.ban_users(bot,
                                                    users,
                                                    msg,
                                                    unban=True)
        except WrongFormatException:
            send_error_unban(bot, update)
            return
        text = users_unban_stats.to_string()
        send_tidy_reply(text, bot, update, disable_web_page_preview=True)

    @only_networkadmins
    def gunban_start_with(self: BanExt, bot: Bot, update: Update) -> None:
        """Unbans the user with the given id."""
        uid_tmp = ParseMessages.get_starts_with_id("gunban_",
                                                   update.message.text)
        try:
            uid = int(uid_tmp)
        except (ValueError, TypeError):
            send_error_unban_with(bot, update)
            return
        user = NetworkBannedUsers.try_get_by_id(uid)
        if not user:
            send_error_unban_wrongid(bot, update)
            return
        BanList.remove_user(uid)
        try:
            BanClient.network_ban(bot, uid, unban=True)
        except WrongFormatException:
            send_error_unban_with(bot, update)
            return
        msg = "Unbanned user:"
        msg += "\n{} {}".format(list_char(), usr_to_str(user.bannedid, user.bannedname))
        send_tidy_short_reply(msg, bot, update, disable_web_page_preview=True)

    @only_networkadmins
    def banned_users(self: BanExt, bot: Bot, update: Update) -> None:
        """Lists all banned users."""
        users = NetworkBannedUsers.get_all()
        users = sorted(users, key=lambda user: datetime.fromisoformat(user.date),
                       reverse=True)
        if not users:
            msg = "No users banned."
            send_custom_reply(msg, bot, update, disable_web_page_preview=True)
            return
        msg = "Banned Users:"
        for user in users:
            msg += "\n/ban_info_{} ".format(user.bannedid)
            msg += usr_to_str(user.bannedid, user.bannedname)
        send_custom_reply(msg, bot, update, disable_web_page_preview=True)

    @only_networkadmins
    def ban_info(self: BanExt, bot: Bot, update: Update) -> None:
        """Prints information about a banned user."""
        uid = ParseMessages.get_starts_with_id("ban_info_",
                                               update.message.text)
        if not uid:
            send_error_ban_info(bot, update)
            return
        banned = NetworkBannedUsers.try_get_by_id(uid)
        if not banned:
            send_error_ban_info_wrongid(bot, update)
            return
        user = [banned.bannedid, banned.bannedname, banned.bannedusername]
        from_user = [banned.fromid, banned.fromname, banned.fromusername]
        msg = "User {}".format(usr_to_str(*user))
        msg += "\n\nStatus: Banned"
        msg += "\nUnban: <code>/gunban_{}</code>".format(banned.bannedid)
        msg += "\nDate: {}".format(banned.date)
        chat = Chats.try_get_chat_by_id(banned.chatid)
        if chat:
            msg += "\nChat: {}".format(chat_to_str(chat))
        else:
            chat = [banned.chatid, banned.chatname]
            msg += "\nChat: {}".format(chat_to_str(*chat))
        tmp = create_message_link(banned.chatid, banned.messageid)
        if tmp:
            msg += '\nMessage: <a href="{}">Link</a>'.format(tmp)
        else:
            msg += '\nMessage: Link not possible'
        tmp2 = create_message_link(banned.chatid, banned.messagereplyid)
        if tmp2:
            msg += '\nReply: <a href="{}">Link</a>'.format(tmp2)
        else:
            msg += '\nReply: Link not possible'
        msg += "\nFrom: {}".format(usr_to_str(*from_user))
        msg += "\nReason: {}".format(banned.reason)
        if banned.replytext:
            msg += "\nText: /ban_info_replied_to_message_{}".format(banned.bannedid)
        else:
            msg += "\nText: None"
        send_custom_reply(msg, bot, update, disable_web_page_preview=True)

    @only_networkadmins
    def ban_info_replied_to_message(self: BanExt, bot: Bot, update: Update) -> None:
        uid = ParseMessages.get_starts_with_id("ban_info_replied_to_message_",
                                               update.message.text)
        if not uid:
            send_error_ban_info_replied_to_message(bot, update)
            return
        banned = NetworkBannedUsers.try_get_by_id(uid)
        if not banned:
            send_error_ban_info_wrongid(bot, update)
            return
        msg = "The following message is the content of the message replied to during ban:"
        ret_msg: Message = send_custom_reply(msg, bot, update,
                                             disable_web_page_preview=True)
        msg = "<code>{}</code>".format(banned.replytext)
        send_custom_reply(msg, bot, update, disable_web_page_preview=True,
                          reply_to_message_id=ret_msg.message_id)
