"This module contains some methods to send error messages."

from __future__ import annotations

from telegram import Bot, Update

from gmb.custom_message.utils import send_tidy_error


def send_error_ban(bot: Bot, update: Update) -> None:
    """Sends an error message to the user, that describes how to use this command."""
    msg = "Wrong format. Reply to a message of the user you like to ban, or to it's "
    msg += "leave or join message. Optionally you can add a reason:\n"
    msg += "/gban [reason]"
    send_tidy_error(msg, bot, update)


def send_error_unban(bot: Bot, update: Update) -> None:
    """Sends an error message to the user, that describes how to use this command."""
    msg = "Wrong format. Reply to a message of the user you like to ban, or to it's "
    msg += "leave or join message.\n"
    msg += "/gunban"
    send_tidy_error(msg, bot, update)


def send_error_unban_with(bot: Bot, update: Update) -> None:
    """Sends an error message to the user, that describes how to use this command."""
    msg = "Wrong format. Use this format:"
    msg += "\n/gunban_[userid]"
    msg += "\n\nExample:"
    msg += "\n/gunban_1989274"
    send_tidy_error(msg, bot, update)


def send_error_ban_info(bot: Bot, update: Update) -> None:
    """Sends an error message to the user, that describes how to use this command."""
    msg = "Wrong format. Use this format:"
    msg += "\n/ban_info_[userid]"
    msg += "\n\nExample:"
    msg += "\n/ban_info_1989374"
    send_tidy_error(msg, bot, update)


def send_error_unban_wrongid(bot: Bot, update: Update) -> None:
    """Sends an error message to the user, that describes how to use this command."""
    msg = "Wrong user id. The user with this id is currently not banned."
    send_tidy_error(msg, bot, update)


def send_error_ban_info_wrongid(bot: Bot, update: Update) -> None:
    """Sends an error message to the user, that describes how to use this command."""
    msg = "Wrong user id. The user with this id is not banned."
    send_tidy_error(msg, bot, update)


def send_error_ban_info_replied_to_message(bot: Bot, update: Update) -> None:
    """Sends an error message to the user, that describes how to use this command."""
    msg = "Wrong format. Use this format:"
    msg += "\n/ban_info_replied_to_message_[userid]"
    msg += "\n\nExample:"
    msg += "\n/ban_info_replied_to_message_1989374"
    send_tidy_error(msg, bot, update)

