from __future__ import annotations

from typing import List, Optional

from telegram import Bot, Message, Update, User
from telegram.ext import BaseFilter, CommandHandler, MessageHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.plugins.global_ban.global_ban_network.ban_client import BanClient
from gmb.plugins.global_ban.global_ban_network.exceptions import WrongFormatException


class IslamIstFriedenFilter(BaseFilter):
    """Filters a user by its name and username if it is islam ist frieden."""

    @staticmethod
    def filter_user(user: User) -> bool:
        """Filters a user by its name and username if it is islam ist frieden."""
        name = user.full_name.lower()
        if "islam" in name and "frieden" in name:
            print("IslamIstFrieden: match by fullname")
            return True
        if user.username is not None:
            username = user.username.lower()
            if "merkeljugend" in username:
                print("IslamIstFrieden: match by username 1")
                return True
            if "terrorsekte" in username:
                print("IslamIstFrieden: match by username 2")
                return True
        return False

    def filter(self: IslamIstFriedenFilter, message: Message) -> bool:
        if IslamIstFriedenFilter.filter_user(message.from_user):
            return True
        for user in message.new_chat_members:
            if IslamIstFriedenFilter.filter_user(user):
                return True
        return False


@register_pollingbot_extension
class BanIslamIstFrieden(PollBotExt):
    def get_handlers(self: BanIslamIstFrieden) -> List[PriorityHandler]:
        filt = IslamIstFriedenFilter()
        return [
            PriorityHandler(MessageHandler(filt, self.gban_islam_ist_frieden),
                            HandlerPriority.HIGH)
        ]

    def gban_islam_ist_frieden(self: BanIslamIstFrieden, bot: Bot, update: Update) -> None:
        users = update.effective_message.new_chat_members
        print("Banning islam ist frieden")
        ban_users: List[User] = []
        if users:
            for user in users:
                if IslamIstFriedenFilter.filter_user(user):
                    ban_users.append(user)
        else:
            ban_users.append(update.effective_user)
        msg = update.message
        reason = "Islam ist Frieden"
        try:
            BanClient.ban_users(bot, ban_users, msg, reason)
        except WrongFormatException:
            print("Error banning user.")
            return
