from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.permissions import only_networkadmins
from gmb.custom_message.utils import send_tidy_error, send_custom_reply
from gmb.utils_design import usr_to_str

from gmb.plugins.global_ban.global_ban_network.schema_network_ban import NetworkBannedUsers


@register_pollingbot_extension
class BanSearchExt(PollBotExt):
    def get_handlers(self) -> List[PriorityHandler]:
        return [
            PriorityHandler(CommandHandler('search_banned', self.search_banned),
                            HandlerPriority.NORMAL),
        ]

    @staticmethod
    def send_error_wrong_format(bot: Bot, update: Update) -> None:
        """Sends an error message to the user, that describes how to use this command."""
        msg = "Wrong user id. The user with this id is not banned."
        send_tidy_error(msg, bot, update)

    @only_networkadmins
    def search_banned(self, bot: Bot, update: Update) -> None:
        print("hi")
        split = update.effective_message.text.split(" ")
        if len(split) < 2:
            BanSearchExt.send_error_wrong_format(bot, update)
            return
        search = " ".join(split[1:])
        query1 = NetworkBannedUsers.select().where(
            NetworkBannedUsers.bannedname.contains(search))
        query2 = NetworkBannedUsers.select().where(
            NetworkBannedUsers.bannedusername.contains(search))
        found_users: List[NetworkBannedUsers] = []
        for user in query1:
            if user not in found_users:
                found_users.append(user)
        for user in query2:
            if user not in found_users:
                found_users.append(user)
        msg = "Found this banned Users:"
        for user in found_users:
            msg += "\n/ban_info_{} ".format(user.bannedid)
            msg += usr_to_str(user.bannedid, user.bannedname)
        send_custom_reply(msg, bot, update, disable_web_page_preview=True)
