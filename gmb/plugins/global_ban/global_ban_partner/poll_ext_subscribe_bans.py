from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.permissions import only_groupadmins
from gmb.custom_message.utils import send_tidy_error, send_custom_reply

from gmb.plugins.partner.schema_partner import PartnerChatsConfig


@register_pollingbot_extension
class SubscribeBanExt(PollBotExt):

    def get_handlers(self: SubscribeBanExt) -> List[PriorityHandler]:
        return [
            PriorityHandler(CommandHandler('subscribe_bans', self.subscribe_bans),
                            HandlerPriority.NORMAL),
            PriorityHandler(CommandHandler('unsubscribe_bans', self.unsubscribe_bans),
                            HandlerPriority.NORMAL),
        ]

    @staticmethod
    def send_error_not_a_partner(bot: Bot, update: Update) -> None:
        msg = "This chat is not a partner. Only partner chats can subscribe the ban list."
        send_tidy_error(msg, bot, update)

    @staticmethod
    def send_error_already_subscribed(bot: Bot, update: Update) -> None:
        msg = "Your chat has already subscribed the global ban list."
        send_tidy_error(msg, bot, update)

    @staticmethod
    def send_error_not_subscribed(bot: Bot, update: Update) -> None:
        msg = "Your chat has not subscribed the global ban list."
        send_tidy_error(msg, bot, update)

    @only_groupadmins
    def subscribe_bans(self, bot: Bot, update: Update) -> None:
        chat = update.effective_chat
        partner = PartnerChatsConfig.try_get_by_id(chat.id)
        if not partner:
            SubscribeBanExt.send_error_not_a_partner(bot, update)
            return
        if partner.subscribed_global_bans:
            SubscribeBanExt.send_error_already_subscribed(bot, update)
            return
        partner.subscribed_global_bans = True
        partner.save()
        msg = "Successfully subscribed global ban list. Now every ban in the network"
        msg += " will also be executed in your chat."
        send_custom_reply(msg, bot, update)

    @only_groupadmins
    def unsubscribe_bans(self, bot: Bot, update: Update) -> None:
        chat = update.effective_chat
        partner = PartnerChatsConfig.try_get_by_id(chat.id)
        if not partner:
            SubscribeBanExt.send_error_not_a_partner(bot, update)
            return
        if not partner.subscribed_global_bans:
            SubscribeBanExt.send_error_not_subscribed(bot, update)
        partner.subscribed_global_bans = False
        partner.save()
        msg = "Successfully unsubscribed global ban list. Now the bot won't"
        msg += " ban users in your chat anymore."
        send_custom_reply(msg, bot, update)
