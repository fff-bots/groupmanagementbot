from peewee import BigIntegerField, CharField, IntegerField

from gmb.api.database import register_plugin_config
from gmb.api.datastructs import AbstractUser


@register_plugin_config
class BanBlacklisted(AbstractUser):
    """Users that shouldn't be banned."""
