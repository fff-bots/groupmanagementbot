from __future__ import annotations

from typing import List

from telegram import Bot, ReplyKeyboardRemove, Update, Message, Chat
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.telegram_util import only_private_groups
from gmb.custom_message.utils import send_custom_reply
from gmb.silent_mode import silent_mode
from gmb.custom_message.utils import send_tidy_reply

from .schema_small_command_config import SmallCommandConfig


@register_pollingbot_extension
class SmallCommandExt(PollBotExt):
    """Extension for small commands that just sends a single message."""

    def get_handlers(self: SmallCommandExt) -> List[PriorityHandler]:
        """Returns a list of Handlers that are added to the bot."""
        return [
            PriorityHandler(CommandHandler('start', self.start),
                            HandlerPriority.NORMAL),
            PriorityHandler(CommandHandler('license', self.license),
                            HandlerPriority.NORMAL),
            PriorityHandler(CommandHandler('privacy', self.privacy),
                            HandlerPriority.NORMAL),
        ]

    @only_private_groups
    def start(self: SmallCommandExt, bot: Bot, update: Update) -> None:
        """Sends the start message.

        Args:
            bot: The bot that sends the message.
            update: The update on that the command was triggered.
        """
        msg = str(SmallCommandConfig.get_entry().start_message)
        send_custom_reply(msg, bot, update, reply_markup=ReplyKeyboardRemove())

    @silent_mode
    def license(self: SmallCommandExt, bot: Bot, update: Update) -> None:
        """Sends the license message.

        Args:
            bot: The bot that sends the message.
            update: The update on that the command was triggered.
        """
        msg = str(SmallCommandConfig.get_entry().license_message)
        send_tidy_reply(msg, bot, update)

    @silent_mode
    def privacy(self: SmallCommandExt, bot: Bot, update: Update) -> None:
        """Sends the privacy message.

        Args:
            bot: The bot that sends the message.
            update: The update on that the command was triggered.
        """
        msg = str(SmallCommandConfig.get_entry().dsgvo_message)
        send_tidy_reply(msg, bot, update)
