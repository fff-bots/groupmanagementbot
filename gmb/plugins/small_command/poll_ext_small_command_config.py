from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.permissions import only_botadmins
from gmb.telegram_util import (reset_config_member, set_config_member)
from gmb.custom_message.utils import send_custom_reply

from .schema_small_command_config import SmallCommandConfig


@register_pollingbot_extension
class SmallCommandConfigExt(PollBotExt):
    def get_handlers(self: SmallCommandConfigExt) -> List[PriorityHandler]:
        return [
            PriorityHandler(
                CommandHandler('set_start_message', self.set_start_message),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('set_license_message',
                               self.set_license_message),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('set_privacy_message',
                               self.set_privacy_message),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('reset_start_message',
                               self.reset_start_message),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('reset_license_message',
                               self.reset_license_message),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('reset_privacy_message',
                               self.reset_privacy_message),
                HandlerPriority.NORMAL)
        ]

    @only_botadmins
    def set_start_message(self: SmallCommandConfigExt, bot: Bot,
                          update: Update) -> None:
        set_config_member(SmallCommandConfig, "start_message", update)
        msg = "Start message updated."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def reset_start_message(self: SmallCommandConfigExt, bot: Bot,
                            update: Update) -> None:
        reset_config_member(SmallCommandConfig, "start_message")
        msg = "Start message resetted."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def set_license_message(self: SmallCommandConfigExt, bot: Bot,
                            update: Update) -> None:
        set_config_member(SmallCommandConfig, "license_message", update)
        msg = "License message updated."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def reset_license_message(self: SmallCommandConfigExt, bot: Bot,
                              update: Update) -> None:
        reset_config_member(SmallCommandConfig, "license_message")
        msg = "License message resetted."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def set_privacy_message(self: SmallCommandConfigExt, bot: Bot,
                            update: Update) -> None:
        set_config_member(SmallCommandConfig, "dsgvo_message", update)
        msg = "Privacy message updated."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def reset_privacy_message(self: SmallCommandConfigExt, bot: Bot,
                              update: Update) -> None:
        reset_config_member(SmallCommandConfig, "dsgvo_message")
        msg = "Privacy message resetted."
        send_custom_reply(msg, bot, update)
