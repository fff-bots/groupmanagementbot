from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.permissions import only_botadmins
from gmb.plugins.network.network.extensions import NetworkExtensions
from gmb.plugins.network.network.schema_network import NetworkChat, NetworkChatsConfig
from gmb.custom_message.utils import send_custom_reply
from gmb.utils_design import list_char
from gmb.silent_mode import silent_and_not_private
from gmb.custom_message.utils import send_tidy_reply


@register_pollingbot_extension
class GroupsListExt(PollBotExt):
    """Extension to list our groups.

    This extension adds a command to list our chats.
    """

    def get_handlers(self: GroupsListExt) -> List[PriorityHandler]:
        """Returns a list of Handlers that are added to the bot."""
        return [
            PriorityHandler(CommandHandler('groups', self.groups),
                            HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('groups_config', self.groups_config),
                HandlerPriority.NORMAL)
        ]

    @staticmethod
    def __get_line(chat: NetworkChat) -> str:
        if chat.invite_link:
            return '<a href="{}">{}</a>'.format(chat.invite_link, chat.title)
        return '{} (missing permissions)'.format(chat.title)

    def groups(self: GroupsListExt, bot: Bot, update: Update) -> None:
        """Sends a list of all groups/chats.

        Sends a message with a list of all groups or chats that are in the chats config.

        Args:
            bot: The bot that sends the message.
            update: The update on that the command was triggered.
        """
        chats = NetworkChat.get_all()
        chats = sorted(chats, key=lambda chat: chat.name)
        chats = sorted(chats, key=lambda chat: chat.prio)
        if not chats:
            if silent_and_not_private(update):
                return
            text = "No chats added."
        else:
            text = NetworkExtensions.get_last().get_network_name()
            text += " Chats:"
            current_chat = None
            for chat in chats:
                tmp = "\n{}".format(list_char())
                tmp += ' {}'
                if chat.id == update.message.chat.id:
                    current_chat = chat
                text += tmp.format(GroupsListExt.__get_line(chat))
            if current_chat is not None:
                tmp = '\n\nAktueller Chat:\n{}'
                text += tmp.format(GroupsListExt.__get_line(current_chat))
        send_tidy_reply(text, bot, update, disable_web_page_preview=True)

    @only_botadmins
    def groups_config(self: GroupsListExt, bot: Bot, update: Update) -> None:
        """Sends a list of all partner configs.

        Sends a message with a list of all partner configs.

        Args:
            bot: The bot that sends the message.
            update: The update on that the command was triggered.
        """
        chats = NetworkChatsConfig.get_all()
        if not chats:
            text = "No chats added. Add them with the /add_network command."
        else:
            text = "Groups Config:"
            current_chat = None
            for chat in chats:
                tmp = "\n{}".format(list_char())
                tmp += '#{name} (id: {id}) (prio: {prio})'
                if chat.id == update.message.chat.id:
                    current_chat = chat
                text += tmp.format(name=chat.name, id=chat.id, prio=chat.prio)
            if current_chat is not None:
                tmp = '\n\nAktueller Chat:\n'
                tmp += '#{name} (id: {id}) (prio: {prio})'
                text += tmp.format(name=current_chat.name,
                                   id=current_chat.id,
                                   prio=current_chat.prio)
        send_custom_reply(text, bot, update, disable_web_page_preview=True)
