from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.permissions import only_botadmins
from gmb.plugins.partner.schema_partner import PartnerChat, PartnerChatsConfig
from gmb.custom_message.utils import send_custom_reply
from gmb.utils_design import list_char
from gmb.silent_mode import silent_and_not_private
from gmb.custom_message.utils import send_tidy_reply


@register_pollingbot_extension
class PartnersListExt(PollBotExt):
    """Extension to list our partners.

    This extension adds a command to list our chats.
    """

    def get_handlers(self: PartnersListExt) -> List[PriorityHandler]:
        """Returns a list of Handlers that are added to the bot."""
        return [
            PriorityHandler(CommandHandler('partners', self.partners),
                            HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('partners_config', self.partners_config),
                HandlerPriority.NORMAL)
        ]

    @staticmethod
    def __get_line(chat: PartnerChat) -> str:
        if chat.invite_link:
            out = '<a href="{}">{}</a> (#{})'
            return out.format(chat.invite_link, chat.title, chat.name)
        return '{} (#{}) (missing permissions)'.format(chat.title, chat.name)

    def partners(self: PartnersListExt, bot: Bot, update: Update) -> None:
        """Sends a list of all partners/chats.

        Sends a message with a list of all partners or chats that are in the chats config.

        Args:
            bot: The bot that sends the message.
            update: The update on that the command was triggered.
        """
        chats = PartnerChat.get_all()
        chats = sorted(chats, key=lambda chat: chat.name)
        chats = sorted(chats, key=lambda chat: chat.prio)
        if not chats:
            if silent_and_not_private(update):
                return
            text = "No chats added."
        else:
            text = "Partner Chats:"
            current_chat = None
            # sort list by prio
            for chat in chats:
                tmp = '\n{}'.format(list_char())
                tmp += ' {}'
                if chat.id == update.message.chat.id:
                    current_chat = chat
                text += tmp.format(PartnersListExt.__get_line(chat))
            if current_chat is not None:
                tmp = '\n\nAktueller Chat:\n{}'
                text += tmp.format(PartnersListExt.__get_line(current_chat))
        send_tidy_reply(text, bot, update, disable_web_page_preview=True)

    @only_botadmins
    def partners_config(self: PartnersListExt, bot: Bot,
                        update: Update) -> None:
        """Sends a list of all partner configs.

        Sends a message with a list of all partner configs.

        Args:
            bot: The bot that sends the message.
            update: The update on that the command was triggered.
        """
        chats = PartnerChatsConfig.get_all()
        if not chats:
            text = "No chats added. Add them with the /add_partner command."
        else:
            text = "Partners Config:"
            current_chat = None
            # sort list by prio
            for chat in chats:
                tmp = '\n{}'.format(list_char())
                tmp += ' #{name} (id: {id}) (prio: {prio})'
                if chat.id == update.message.chat.id:
                    current_chat = chat
                text += tmp.format(name=chat.name, id=chat.id, prio=chat.prio)
            if current_chat is not None:
                tmp = '\n\nAktueller Chat:\n'
                tmp += '#{name} (id: {id}) (prio: {prio})'
                text += tmp.format(name=current_chat.name,
                                   id=current_chat.id,
                                   prio=current_chat.prio)
        send_custom_reply(text, bot, update, disable_web_page_preview=True)
