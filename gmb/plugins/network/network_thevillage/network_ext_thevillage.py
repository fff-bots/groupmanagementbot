from gmb.plugins.network.network.extension import NetworkExt
from gmb.plugins.network.network.extensions import register_network_extension


@register_network_extension
class NetworkExtTheVillage(NetworkExt):
    @staticmethod
    def get_name_regex() -> str:
        return "(?:TheVillage #?| play #?)([^\\s]+)"

    @staticmethod
    def get_fullname_regex() -> str:
        return "(?:TheVillage #?| play #?)(.+)"

    @staticmethod
    def get_network_name() -> str:
        return "TheVillage"
