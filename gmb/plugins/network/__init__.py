from gmb.utils_plugin import PluginLoader


__PLUGIN_LOADER = PluginLoader(__file__)

if len(__PLUGIN_LOADER.dir_names) > 2:
    __PLUGIN_LOADER.import_plugins()

def import_plugins():
    """Imports all plugins from the plugin folder."""
    __PLUGIN_LOADER.import_plugins()
