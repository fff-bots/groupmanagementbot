from __future__ import annotations

from typing import Optional

import re

from telegram import Chat


class NetworkExt:
    """Regexes that are needed by the network plugin to get the name and the fullname from
    the chat title."""

    @staticmethod
    def get_name_regex() -> str:
        """Returns the regex that is used to get the name of the chat from the title.

        The name is used as tag and doesn't contain spaces.
        """
        raise NotImplementedError

    @staticmethod
    def get_fullname_regex() -> str:
        """Returns the regex that is used to get the fullname of the chat from the title."""
        raise NotImplementedError

    @staticmethod
    def get_network_name() -> str:
        """Returns the name of the network."""
        raise NotImplementedError

    @staticmethod
    def match(reg: str, res: Chat) -> Optional[str]:
        match = re.search(reg, res.title)
        if match:
            return "".join(filter(None, match.groups()))
        return None

    @classmethod
    def get_name_special(cls, res: Chat) -> Optional[str]:
        return None

    @classmethod
    def get_fullname_special(cls, res: Chat) -> Optional[str]:
        return None

    @classmethod
    def get_name(cls, res: Chat) -> str:
        """Filters the chat title by the last added network regex to get the name."""
        special = cls.get_name_special(res)
        if special:
            return special
        reg = cls.get_name_regex()
        name = cls.match(reg, res)
        if name:
            return name
        raise Exception("Name doesn't match regex pattern")

    @classmethod
    def get_fullname(cls, res: Chat) -> str:
        """Filters the chat title by the last added network regex to get the fullname."""
        special = cls.get_fullname_special(res)
        if special:
            return special
        reg = cls.get_fullname_regex()
        fullname = cls.match(reg, res)
        if fullname:
            return fullname
        raise Exception("Fullname doesn't match regex pattern")
