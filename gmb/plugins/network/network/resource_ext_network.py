from __future__ import annotations

import re
from typing import Optional, Type

from telegram import Bot, Chat

from gmb.api.datastructs import (AbstractChat, AbstractChatsConfig,
                                 create_chat_factory)
from gmb.api.resource import ChatType, ResourceExt, register_resource_extension

from .extensions import NetworkExtensions
from .schema_network import (NetworkChat, NetworkChatAdmin, NetworkChatsConfig,
                             NetworkUser)


@register_resource_extension
class NetworkResourceExt(ResourceExt):
    """Resource extension to add the chats, that belongs to a network."""

    @staticmethod
    def get_chats_config_class() -> Type[NetworkChatsConfig]:
        return NetworkChatsConfig

    @staticmethod
    def get_chat_class() -> Type[NetworkChat]:
        return NetworkChat

    @staticmethod
    def get_chat_admin_class() -> Type[NetworkChatAdmin]:
        return NetworkChatAdmin

    @staticmethod
    def get_user_class() -> Type[NetworkUser]:
        return NetworkUser

    @staticmethod
    def get_chat_type() -> ChatType:
        return ChatType.INTERN

    @staticmethod
    def factory(bot: Bot, resource_ext: Type[ResourceExt],
                chats_config: AbstractChatsConfig,
                output_dir: str) -> AbstractChat:
        chat_fac = create_chat_factory(NetworkResourceExt.get_name,
                                       NetworkResourceExt.get_fullname)
        return chat_fac(bot, resource_ext, chats_config, output_dir)

    @staticmethod
    def get_name(res: Chat, _chats_config: AbstractChatsConfig) -> str:
        """Filters the chat title by the last added network regex to get the name."""
        return NetworkExtensions.get_last().get_name(res)

    @staticmethod
    def get_fullname(res: Chat, _chats_config: AbstractChatsConfig) -> str:
        """Filters the chat title by the last added network regex to get the fullname."""
        return NetworkExtensions.get_last().get_fullname(res)

    @staticmethod
    def subscribed_global_ban(_chat_config: NetworkChatsConfig) -> bool:
        return True
