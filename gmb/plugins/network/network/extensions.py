from typing import Type

from gmb.abstract_extensions import Extensions

from .extension import NetworkExt


class NetworkExtensions(Extensions):
    """Contains all network extensions."""

    @classmethod
    def get_last(cls):
        """Returns the last network extension, that is in the extensions list."""
        ext = cls.get_extensions()
        return ext[len(ext) - 1]


def register_network_extension(cls: Type[NetworkExt]) -> Type[NetworkExt]:
    """Add a network extension to the network extensions.

    Notice:
        Only the last one added is used.
    """
    print("Registered network extension: {}".format(cls.__name__))
    NetworkExtensions.add_extension(cls)
    return cls
