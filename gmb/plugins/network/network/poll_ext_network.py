from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.datastructs import InfoFetchConfig
from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.api.resource import clean_all, fetch_all
from gmb.permissions import only_botadmins
from gmb.custom_message.utils import send_custom_reply

from .schema_network import NetworkChatsConfig


@register_pollingbot_extension
class NetworkConfigExt(PollBotExt):
    def get_handlers(self: NetworkConfigExt) -> List[PriorityHandler]:
        return [
            PriorityHandler(CommandHandler('add_network', self.add_network),
                            HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('remove_network', self.remove_network),
                HandlerPriority.NORMAL)
        ]

    @staticmethod
    def __reply_error_add_network(bot, update):
        msg = "Wrong format. Please use this format:\n"
        msg += "/add_network chatid name prio\n\n"
        msg += "chatid needs to be an integer, name a string (with only letters and"
        msg += " numbers) and prio is an optional integer."
        send_custom_reply(msg, bot, update)

    @staticmethod
    def __reply_error_remove_network(bot, update):
        msg = "Wrong format. Please use this format:\n"
        msg += "/remove_network name\n\n"
        msg += "chatid needs to be an integer, name a string and prio is an optional integer."
        send_custom_reply(msg, bot, update)

    @staticmethod
    def __check_unique(chatid: int, name: str) -> bool:
        """Returns True if id and name are not already used."""
        query = NetworkChatsConfig.select().where(
            NetworkChatsConfig.name == name)
        for network in query:
            return False
        query = NetworkChatsConfig.select().where(
            NetworkChatsConfig.id == chatid)
        for network in query:
            return False
        return True

    @only_botadmins
    def add_network(self: NetworkConfigExt, bot: Bot, update: Update) -> None:
        text_fields = update.message.text.split(" ")
        if len(text_fields) > 4 or len(text_fields) < 3:
            NetworkConfigExt.__reply_error_add_network(bot, update)
            return
        try:
            chatid = int(text_fields[1])
            name = text_fields[2]
            name = "".join(name.split("\n"))
            # check unique of name and id
            if not NetworkConfigExt.__check_unique(chatid, name):
                msg = "Chatid or name already added.\n"
                msg += "Use /groups_config to see all network configs."
                send_custom_reply(msg, bot, update)
                return
            if len(text_fields) > 3:
                prio = int(text_fields[3])
            else:
                prio = InfoFetchConfig.get_entry().prio_standard
        except ValueError:
            # values are in wrong order
            NetworkConfigExt.__reply_error_add_network(bot, update)
            return
        NetworkChatsConfig.create(id=chatid, name=name, prio=prio)
        msg = "Added network with name {name} ({id}) and priority {prio}."
        msg = msg.format(name=name, id=chatid, prio=prio)
        send_custom_reply(msg, bot, update)
        fetch_all(bot)

    @only_botadmins
    def remove_network(self: NetworkConfigExt, bot: Bot,
                       update: Update) -> None:
        text_fields = update.message.text.split(" ")
        if len(text_fields) != 2:
            NetworkConfigExt.__reply_error_remove_network(bot, update)
            return
        name = text_fields[1]
        networks = NetworkChatsConfig.select().where(
            NetworkChatsConfig.name == name)
        network = None
        for _network in networks:
            network = _network
        if network is None:
            # gibts nicht, fehler
            msg = "There is no network with this name {name}.\n"
            msg += "Use /groups_config to see all network configs."
            msg = msg.format(name=name)
            send_custom_reply(msg, bot, update)
            return
        name = network.name
        chatid = network.id
        prio = network.prio
        NetworkChatsConfig.delete().where(
            NetworkChatsConfig.name == name).execute()
        msg = "Removed network with name {name} ({id}) and priority {prio}."
        msg = msg.format(name=name, id=chatid, prio=prio)
        send_custom_reply(msg, bot, update)
        clean_all(bot)
