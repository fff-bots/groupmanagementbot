from typing import Optional

from telegram import Chat

from gmb.plugins.network.network.extension import NetworkExt
from gmb.plugins.network.network.extensions import register_network_extension


@register_network_extension
class NetworkExtFFF(NetworkExt):
    @staticmethod
    def get_name_regex() -> str:
        return "(?:FFF ([a-zA-Z0-9_äöüÄÖÜ]+)(?:[ -]([a-zA-Z0-9_äöüÄÖÜ]+))*.*)"

    @staticmethod
    def get_fullname_regex() -> str:
        return "(?:FFF ([a-zA-Z0-9_äöüÄÖÜ]+)([ -][a-zA-Z0-9_äöüÄÖÜ]+)*.*)"

    @staticmethod
    def get_network_name() -> str:
        return "Fridays for Future Germany"

    @classmethod
    def get_name_special(cls, res: Chat) -> Optional[str]:
        if NetworkExt.match(".+(FridaysForFuture Germany) .+", res):
            return "Channel"
        if NetworkExt.match(".+ (Fridays for Future Germany) .+", res):
            return "FFFde"
        if NetworkExt.match("(oFFF-Topic)", res):
            return "OffTopic"
        return None

    @classmethod
    def get_fullname_special(cls, res: Chat) -> Optional[str]:
        if NetworkExt.match(".+(FridaysForFuture Germany) .+", res):
            return res.title
        if NetworkExt.match(".+ (Fridays for Future Germany) .+", res):
            return res.title
        if NetworkExt.match("(oFFF-Topic)", res):
            return res.title
        return None
