from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.datastructs import InfoFetchConfig
from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.api.resource import clean_all, fetch_all
from gmb.permissions import only_botadmins
from gmb.custom_message.utils import send_custom_reply

from .schema_partner import PartnerChatsConfig


@register_pollingbot_extension
class PartnerConfigExt(PollBotExt):
    def get_handlers(self: PartnerConfigExt) -> List[PriorityHandler]:
        return [
            PriorityHandler(CommandHandler('add_partner', self.add_partner),
                            HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('remove_partner', self.remove_partner),
                HandlerPriority.NORMAL)
        ]

    @staticmethod
    def __reply_error_add_partner(bot, update):
        msg = "Wrong format. Please use this format:\n"
        msg += "/add_partner chatid tag prio\n\n"
        msg += "chatid needs to be an integer, tag a string (with only letters"
        msg += " and numbers) and prio is an optional integer."
        send_custom_reply(msg, bot, update)

    @staticmethod
    def __reply_error_remove_partner(bot, update):
        msg = "Wrong format. Please use this format:\n"
        msg += "/remove_partner tag\n\n"
        msg += "chatid needs to be an integer, tag a string and prio is an optional integer."
        send_custom_reply(msg, bot, update)

    @staticmethod
    def __check_unique(chatid: int, name: str) -> bool:
        """Returns True if id and name are not already used."""
        query = PartnerChatsConfig.select().where(
            PartnerChatsConfig.name == name)
        for partner in query:
            return False
        query = PartnerChatsConfig.select().where(
            PartnerChatsConfig.id == chatid)
        for partner in query:
            return False
        return True

    @only_botadmins
    def add_partner(self: PartnerConfigExt, bot: Bot, update: Update) -> None:
        text_fields = update.message.text.split(" ")
        if len(text_fields) > 4 or len(text_fields) < 3:
            PartnerConfigExt.__reply_error_add_partner(bot, update)
            return
        try:
            chatid = int(text_fields[1])
            name = text_fields[2]
            name = "".join(name.split("\n"))
            # check unique of name and id
            if not PartnerConfigExt.__check_unique(chatid, name):
                msg = "Chatid or tag already added.\n"
                msg += "Use /partners_config to see all partner configs."
                send_custom_reply(msg, bot, update)
                return
            if len(text_fields) > 3:
                prio = int(text_fields[3])
            else:
                prio = InfoFetchConfig.get_entry().prio_standard
        except ValueError:
            # values are in wrong order
            PartnerConfigExt.__reply_error_add_partner(bot, update)
            return
        PartnerChatsConfig.create(id=chatid, name=name, prio=prio)
        msg = "Added partner with tag {name} ({id}) and priority {prio}."
        msg = msg.format(name=name, id=chatid, prio=prio)
        send_custom_reply(msg, bot, update)
        fetch_all(bot)

    @only_botadmins
    def remove_partner(self: PartnerConfigExt, bot: Bot,
                       update: Update) -> None:
        text_fields = update.message.text.split(" ")
        if len(text_fields) != 2:
            PartnerConfigExt.__reply_error_remove_partner(bot, update)
            return
        name = text_fields[1]
        partners = PartnerChatsConfig.select().where(
            PartnerChatsConfig.name == name)
        partner = None
        for _partner in partners:
            partner = _partner
        if partner is None:
            # gibts nicht, fehler
            msg = "There is no partner with this tag {tag}.\n"
            msg += "Use /partners_config to see all partner configs."
            msg = msg.format(tag=name)
            send_custom_reply(msg, bot, update)
            return
        name = partner.name
        chatid = partner.id
        prio = partner.prio
        PartnerChatsConfig.delete().where(
            PartnerChatsConfig.name == name).execute()
        msg = "Removed partner with tag {name} ({id}) and priority {prio}."
        msg = msg.format(name=name, id=chatid, prio=prio)
        send_custom_reply(msg, bot, update)
        clean_all(bot)
