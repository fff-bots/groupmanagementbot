from __future__ import annotations

from typing import Callable, Type, Union

from telegram import Bot, Chat

from gmb.api.datastructs import (AbstractChat, AbstractChatsConfig,
                                 create_chat_factory)
from gmb.api.resource import ChatType, ResourceExt, register_resource_extension

from .schema_partner import (PartnerChat, PartnerChatAdmin, PartnerChatsConfig,
                             PartnerUser)


@register_resource_extension
class PartnerResourceExt(ResourceExt):
    @staticmethod
    def get_chats_config_class() -> Type[PartnerChatsConfig]:
        return PartnerChatsConfig

    @staticmethod
    def get_chat_class() -> Type[PartnerChat]:
        return PartnerChat

    @staticmethod
    def get_chat_admin_class() -> Type[PartnerChatAdmin]:
        return PartnerChatAdmin

    @staticmethod
    def get_user_class() -> Type[PartnerUser]:
        return PartnerUser

    @staticmethod
    def get_chat_type() -> ChatType:
        return ChatType.EXTERN

    @staticmethod
    def factory(bot: Bot, resource_ext: Type[ResourceExt],
                chats_config: AbstractChatsConfig,
                output_dir: str) -> AbstractChat:
        chat_fac = create_chat_factory(PartnerResourceExt.get_name,
                                       PartnerResourceExt.get_fullname)
        return chat_fac(bot, resource_ext, chats_config, output_dir)

    @staticmethod
    def get_name(_res: Chat, chats_config: AbstractChatsConfig) -> str:
        return chats_config.name

    @staticmethod
    def get_fullname(res: Chat, _chats_config: AbstractChatsConfig) -> str:
        return res["title"]

    @staticmethod
    def subscribed_global_ban(chat_config: PartnerChatsConfig) -> bool:
        return chat_config.subscribed_global_bans
