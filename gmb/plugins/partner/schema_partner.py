from peewee import BooleanField

from gmb.api.database import (register_non_persistent,
                              register_non_persistent_with_dependencies,
                              register_system_config)
from gmb.api.datastructs import (AbstractChat, AbstractChatAdmin,
                                 AbstractChatsConfig, AbstractUser,
                                 chat_admin_class)


@register_system_config
class PartnerChatsConfig(AbstractChatsConfig):
    subscribed_global_bans = BooleanField(null=False, default=False)


@register_non_persistent
class PartnerChat(AbstractChat):
    pass


@register_non_persistent
class PartnerUser(AbstractUser):
    pass


@register_non_persistent_with_dependencies(PartnerChat, PartnerUser)
@chat_admin_class(PartnerUser, PartnerChat)
class PartnerChatAdmin(AbstractChatAdmin):
    pass
