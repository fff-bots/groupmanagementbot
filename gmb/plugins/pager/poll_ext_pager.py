from __future__ import annotations

from typing import List
from dataclasses import dataclass

from telegram import Bot, Update, Message, Chat, InlineKeyboardButton, InlineKeyboardMarkup, CallbackQuery
from telegram.ext import CommandHandler, CallbackQueryHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)

from gmb.custom_message.utils import send_custom_reply
from gmb.custom_message.custom_message import CustomMessage
from gmb.pager.pager import Pager


@register_pollingbot_extension
class PagerExt(PollBotExt):
    """Extension for small commands that just sends a single message."""

    def get_handlers(self: PagerExt) -> List[PriorityHandler]:
        """Returns a list of Handlers that are added to the bot."""
        return [
            PriorityHandler(CallbackQueryHandler(self.current_page,
                                                 pattern='^pager;([0-9]+);current_page$'),
                            HandlerPriority.NORMAL),
            PriorityHandler(CallbackQueryHandler(self.next_page,
                                                 pattern='^pager;([0-9]+);next_page$'),
                            HandlerPriority.NORMAL),
            PriorityHandler(CallbackQueryHandler(self.prev_page,
                                                 pattern='^pager;([0-9]+);prev_page$'),
                            HandlerPriority.NORMAL),
        ]

    @staticmethod
    def current_page(bot: Bot, update: Update):
        pager = Pager.get_pager_by_update(update)
        pager.current_page(bot, update)

    @staticmethod
    def next_page(bot: Bot, update: Update):
        pager = Pager.get_pager_by_update(update)
        pager.next_page(bot, update)

    @staticmethod
    def prev_page(bot: Bot, update: Update):
        pager = Pager.get_pager_by_update(update)
        pager.prev_page(bot, update)
