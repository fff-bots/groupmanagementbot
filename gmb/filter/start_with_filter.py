from __future__ import annotations

from telegram import Message
from telegram.ext import BaseFilter


class StartWithFilter(BaseFilter):
    """A filter that checks if a message starts with a specific part.

    You can use this to make commands that contains variable parameters,
    like '/ban_1234' or '/ban_5555'. Then you just make a `StartWithFilter('/ban_')`
    and extract the parameter later in the command.

    Attributes:
        command (str): The text, the message should start with.
    """

    def __init__(self: StartWithFilter, command: str):
        self.command = command
        self.update_filter = False

    def filter(self: StartWithFilter, update: Message) -> bool:
        if not update.text:
            return False
        if update.text.startswith("/{}".format(self.command)):
            return True
        return False
