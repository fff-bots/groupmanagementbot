from __future__ import annotations

from telegram import Bot
from telegram.ext import Updater
from typing import Optional, Union

from gmb.config.config import StaticConfig, create_static_config


class Environment:

    __env: Optional['Environment'] = None

    def __init__(self: Environment, static_config: StaticConfig):
        # config
        self.static_config = static_config
        # telegram
        self.__updater: Optional[Updater] = None
        self.__bot: Optional[Bot] = None

    def set_bot(self, bot: Bot) -> None:
        """Sets the bot."""
        self.__bot = bot

    def set_updater(self, updater: Updater) -> None:
        """Sets the updater."""
        self.__updater = updater

    def get_bot(self) -> Bot:
        """Returns the bot.

        Raises: Exception, if bot is None.
        """
        if self.__bot is None:
            raise Exception("Bot is None")
        return self.__bot

    def get_updater(self) -> Updater:
        """Returns the updater.

        Raises: Exception, if updater is None.
        """
        if self.__updater is None:
            raise Exception("Updater is None")
        return self.__updater

    @staticmethod
    def get() -> Environment:
        if not Environment.__env:
            Environment.init()
        if not Environment.__env:
            raise Exception("Unknown error: Environment not initialized.")
        return Environment.__env

    @staticmethod
    def init() -> Environment:
        config = create_static_config()
        Environment.__env = Environment(config)
        return Environment.__env
