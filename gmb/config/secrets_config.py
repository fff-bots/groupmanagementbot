import json
from dataclasses import dataclass
from typing import Any, Dict

from gmb.util import read_json_file

from .secrets_and_env_config import SecretsAndEnv
from .util import create_class_from_dict


@dataclass(frozen=True)
class SecretsConfig(SecretsAndEnv):
    """Configurations that are available in the configuration file.

    Takes all keys from SecretsAndEnv.
    """
    pass


def create_secrets_config(filename: str):
    # load from file
    try:
        j = read_json_file(filename)
    except FileNotFoundError:
        print("ERROR: Config file {} does not exist.".format(filename))
        return SecretsConfig()
    if isinstance(j, dict):
        return create_class_from_dict(SecretsConfig, j)
    msg = "Config file {} has wrong format."
    raise Exception(msg.format(filename))
