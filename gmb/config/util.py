from typing import Any, Dict, List


def create_class_from_dict(cls, dic: Dict[str, Any], upper: bool = False):
    """Creates an object of the dataclass class cls.

    Takes all keys from dic, that are members in cls and creates an object of
    the class. cls needs to be an dataclass class.
    """
    values = {}
    for key in cls.__dataclass_fields__:
        try:
            if upper:
                values[key] = dic[str.upper(key)]
            else:
                values[key] = dic[key]
        except KeyError:
            pass
    return cls(**values)


def merge_attributes(cls, dicts: List[Dict[str, Any]]):
    """Merges all dicts into one dict.

    Merges only the key-value pairs that are members in the class cls.
    """
    merged: Dict[str, Any] = {}
    for dic in dicts:
        for key in dic:
            value = dic[key]
            if key in cls.__dataclass_fields__:
                default = cls.__dataclass_fields__[key].default
                if key not in merged:
                    merged[key] = value
                else:
                    if value and value != default:
                        merged[key] = value
    return merged
