from __future__ import annotations

from dataclasses import dataclass
from typing import Any, Dict, List, Union

from .args_config import ArgsConfig, create_args_config
from .env_config import EnvConfig, create_env_config
from .secrets_config import SecretsConfig, create_secrets_config
from .util import merge_attributes


@dataclass(frozen=True)
class StaticConfig(ArgsConfig, EnvConfig, SecretsConfig):
    """Configurations of the bot."""
    pass


def merge_configs(args: ArgsConfig, env: EnvConfig, secrets: SecretsConfig):
    # args > env > secrets
    dicts = [secrets.__dict__, env.__dict__, args.__dict__]
    return merge_attributes(StaticConfig, dicts)


def create_static_config():
    args: ArgsConfig = create_args_config()
    env: EnvConfig = create_env_config()
    secrets: SecretsConfig = create_secrets_config(args.config_file)
    merged = merge_configs(args, env, secrets)
    return StaticConfig(**merged)
