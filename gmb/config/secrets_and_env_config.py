from dataclasses import dataclass
from typing import Optional


@dataclass(frozen=True)
class SecretsAndEnv:
    token: Optional[str] = None
    master_admin_id: Optional[str] = None
    mysql_db_host: Optional[str] = None
    mysql_db_username: Optional[str] = None
    mysql_db_password: Optional[str] = None
    mysql_db_database: Optional[str] = None
    mysql_db_port: Optional[str] = None
    mysql_test_db_host: Optional[str] = None
    mysql_test_db_username: Optional[str] = None
    mysql_test_db_password: Optional[str] = None
    mysql_test_db_database: Optional[str] = None
    mysql_test_db_port: Optional[str] = None
    plugin_blacklist: Optional[str] = None
    api_users: Optional[str] = None
    debug_mode: bool = False
