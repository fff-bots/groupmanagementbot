import argparse
from dataclasses import dataclass
from typing import Optional

import gmb.version

from .util import create_class_from_dict


@dataclass(frozen=True)
class ArgsConfig:
    output_dir: Optional[str] = None
    status_dir: Optional[str] = None
    config_file: Optional[str] = None
    infofetch_status_fetching_filename: Optional[str] = None
    doc_telegram_commands_file: Optional[str] = None
    doc_telegram_commands_botadmins_file: Optional[str] = None
    doc_telegram_commands_admins_file: Optional[str] = None
    doc_telegram_commands_superadmins_file: Optional[str] = None
    doc_telegram_commands_groupadmins_file: Optional[str] = None
    flask_host: Optional[str] = None
    flask_port: Optional[int] = None
    stop_after_init: bool = False
    without_api: bool = False


def _parse_args():
    parser = argparse.ArgumentParser(
        description="GroupManagementBot is a telegram bot to manage networks.")
    parser.add_argument("-v",
                        "--version",
                        help="show version",
                        action="version",
                        version=gmb.version.version)
    # configs
    parser.add_argument("--output-dir",
                        nargs="?",
                        help="output dir of group images",
                        action="store",
                        default="out")
    parser.add_argument("--config-file",
                        nargs="?",
                        help="location of config file",
                        action="store",
                        default="config/config.json")
    parser.add_argument("--status-dir",
                        nargs="?",
                        help="output dir of status files",
                        action="store",
                        default="status")
    parser.add_argument(
        "--infofetch-status-fetching-filename",
        nargs="?",
        help=
        "filename where the fetching status of infofetch bot should be saved",
        action="store",
        default="update")
    # docs
    parser.add_argument("--doc-telegram-commands-file",
                        nargs="?",
                        help="location of doc dir",
                        action="store",
                        default="doc/telegram_commands.txt")
    parser.add_argument("--doc-telegram-commands-botadmins-file",
                        nargs="?",
                        help="location of doc dir",
                        action="store",
                        default="doc/telegram_commands_botadmins.txt")
    parser.add_argument("--doc-telegram-commands-admins-file",
                        nargs="?",
                        help="location of doc dir",
                        action="store",
                        default="doc/telegram_commands_admins.txt")
    parser.add_argument("--doc-telegram-commands-superadmins-file",
                        nargs="?",
                        help="location of doc dir",
                        action="store",
                        default="doc/telegram_commands_superadmins.txt")
    parser.add_argument("--doc-telegram-commands-groupadmins-file",
                        nargs="?",
                        help="location of doc dir",
                        action="store",
                        default="doc/telegram_commands_groupadmins.txt")
    parser.add_argument("--flask-host",
                        nargs="?",
                        help="host of flask server",
                        action="store",
                        default="127.0.0.1")
    parser.add_argument("--flask-port",
                        nargs="?",
                        help="port of flask server",
                        action="store",
                        default="5000")
    parser.add_argument("--stop-after-init",
                        help="only initialises the environment, and than exits",
                        action="store_true")
    parser.add_argument("--without-api",
                        help="don't start the api",
                        action="store_true")
    return parser.parse_args()


def create_args_config():
    """Factory method to create a ArgsConfig."""
    args = _parse_args()
    return create_class_from_dict(ArgsConfig, args.__dict__)
