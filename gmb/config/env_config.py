import os
from dataclasses import dataclass

from .secrets_and_env_config import SecretsAndEnv
from .util import create_class_from_dict


@dataclass(frozen=True)
class EnvConfig(SecretsAndEnv):
    """Configurations that are available through environment variables.

    Takes all keys from SecretsAndEnv.
    """
    pass


def create_env_config():
    return create_class_from_dict(EnvConfig, os.environ, True)
