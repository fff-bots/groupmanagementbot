"""Module to extend send messages with tidy mode and self destructing messages."""

from typing import Union

from telegram import Update, Message

from peewee import BooleanField

from gmb.api.database import register_system_config, BaseModel
from gmb.telegram_util import is_private_group, MessageLifetime, delete_timered

from .extension import (SendMessageParameters, CustomMessageContext,
                        ICustomMessageExtension)


@register_system_config
class TidyMode(BaseModel):
    """Tidy mode is to have cleaner chats.

    It's a mode in which some commands and it's replies from the bot, in group
    chats are deleted after a time. This should prevent spam from commands and
    make the backlog more readable.
    """
    tidy = BooleanField(null=False, default=False)


def tidy() -> bool:
    """Returns if tidy mode is tuned on."""
    return TidyMode.get_entry().tidy


def tidy_and_not_private(update: Update) -> bool:
    """Returns if silent mode is turned on and the group is not private."""
    return tidy() and not is_private_group(update)


class TimedMessagesExtension(ICustomMessageExtension):
    """Adds timed messages and tidy mode to send message."""

    def __init__(self):
        # destruction and tidy
        self.tidy_activated = False
        self.self_destruction_activated = False
        self.timer: Union[MessageLifetime, int] = 0
        self.delete_replied_to = True

    def pre_send(self, parameters: SendMessageParameters,
                 context: CustomMessageContext) -> None:
        pass

    def post_send(self, message: Message, parameters: SendMessageParameters,
                  context: CustomMessageContext) -> None:
        if self.__if_delete_timered(context.update):
            if isinstance(self.timer, MessageLifetime):
                timer: int = self.timer.value
            else:
                timer = self.timer
            delete_timered(message, timer, self.delete_replied_to)

    def __if_delete_timered(self, update: Update) -> bool:
        if self.tidy_activated and tidy_and_not_private(update):
            return True
        if self.self_destruction_activated:
            return True
        return False

    def activate_self_destruction_timer(self, timer: Union[MessageLifetime, int]
                                        = MessageLifetime.MIDDLE):
        """Messages deletes it self after the given time.

        It's recommended to use tidy mode and not destruction timer. If tidy is activated
        on this message, the tidy mode is used instead of destruction mode. That means,
        that the message is only deleted if tidy mode setting is turned on by the user.
        """
        self.self_destruction_activated = True
        self.timer = timer

    def activate_tidy_special(self, timer: Union[MessageLifetime, int]):
        """Activated tidy custom mode on this message."""
        self.tidy_activated = True
        self.timer = timer

    def activate_tidy(self):
        """Activated tidy default mode on this message."""
        self.tidy_activated = True
        self.timer = MessageLifetime.MIDDLE

    def activate_tidy_error(self):
        """Activated tidy error mode on this message."""
        self.tidy_activated = True
        self.timer = MessageLifetime.SHORT

    def activate_tidy_long(self):
        """Activated tidy long mode on this message."""
        self.tidy_activated = True
        self.timer = MessageLifetime.LONG

    def activate_tidy_short(self):
        """Activated tidy short mode on this message."""
        self.tidy_activated = True
        self.timer = MessageLifetime.SHORT
