"""Module to extend send messages with a pager."""

from typing import Type, Optional

from telegram import Message

from gmb.pager.pager import Pager
from gmb.pager.paged_text import AbstractPagedText, CharLengthPagedText

from .extension import (SendMessageParameters, CustomMessageContext,
                        ICustomMessageExtension, NoneException)


class PagerExtension(ICustomMessageExtension):
    """Adds a pager to the send message."""

    def __init__(self):
        self.pager_activated = True
        self.paged_text_cls: Type[AbstractPagedText] = CharLengthPagedText
        self.paged_text: Optional[AbstractPagedText] = None
        self.pager: Optional[Pager] = None

    def __if_pager(self) -> bool:
        if self.paged_text is None:
            raise NoneException()
        return self.pager_activated and len(self.paged_text) > 1

    def pre_send(self, parameters: SendMessageParameters,
                 context: CustomMessageContext) -> None:
        self.paged_text = self.paged_text_cls(parameters.text, parameters.parse_mode)
        if self.__if_pager():
            self.pager = Pager(self.paged_text)
            keyboard = self.pager.gen_keyboard(0)
            if parameters.reply_markup is None:
                parameters.reply_markup = keyboard
        parameters.text = self.paged_text[0]

    def post_send(self, message: Message, parameters: SendMessageParameters,
                  context: CustomMessageContext) -> None:
        if self.pager is None:
            return
        if self.__if_pager():
            self.pager.add_send_message(message, **parameters.__dict__)
