from typing import Any, Union
from telegram import Bot, Update, Message

from gmb.telegram_util import MessageLifetime

from .custom_message import CustomMessage


def send_custom_reply(text: str, bot: Bot, update: Update, **extra: Any):
    """Sends a text message with HTML markup and replaces some vars.

    All keywords args that are passed into the var extra are passed
    into bot.send_message.

    Args:
        bot: The bot that sends this message.
        update: The update that has triggered the command.
        text: The text that should be send, after replacing variables.
        **extra: Arbitrary keyword arguments, that are passed to the send_message.
    """
    msg = CustomMessage(text, bot, update, **extra)
    return msg.send()


def send_custom_reply_timered(text: str, bot: Bot, update: Update,
                              timer: Union[MessageLifetime, int],
                              delete_replied_to: bool = True,
                              **extra: Any) -> Message:
    """Sends a message with HTML markup, replaces vars and deletes it after a time."""
    msg = CustomMessage(text, bot, update, **extra)
    msg.timer.activate_self_destruction_timer(timer)
    msg.delete_replied_to = delete_replied_to
    return msg.send()


def send_tidy_error(text: str, bot: Bot, update: Update,
                    **extra: Any) -> Message:
    """Sends a tidy reply with an error message."""
    msg = CustomMessage(text, bot, update, **extra)
    msg.timer.activate_tidy_error()
    return msg.send()


def send_tidy_special_reply(text: str, bot: Bot, update: Update,
                            timer: int, **extra: Any) -> Message:
    """Sends a tidy reply for special messages."""
    msg = CustomMessage(text, bot, update, **extra)
    msg.timer.activate_tidy_special(timer)
    return msg.send()


def send_tidy_reply(text: str, bot: Bot, update: Update,
                    **extra: Any) -> Message:
    """Sends a tidy reply for normal messages with the middle time."""
    msg = CustomMessage(text, bot, update, **extra)
    msg.timer.activate_tidy()
    return msg.send()


def send_tidy_short_reply(text: str, bot: Bot, update: Update,
                          **extra: Any) -> Message:
    """Sends a tidy reply for messages with the short time."""
    msg = CustomMessage(text, bot, update, **extra)
    msg.timer.activate_tidy_short()
    return msg.send()


def send_tidy_long_reply(text: str, bot: Bot, update: Update,
                         **extra: Any) -> Message:
    """Sends a tidy reply for messages with the long time."""
    msg = CustomMessage(text, bot, update, **extra)
    msg.timer.activate_tidy_long()
    return msg.send()
