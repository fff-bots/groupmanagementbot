"""Module to extend custom messages."""

from __future__ import annotations

from dataclasses import dataclass
from typing import Union, Optional

from telegram import Bot, Update, Message, ParseMode, ReplyMarkup


class NoneException(Exception):
    """Raised if object is unexpected none."""


@dataclass
class SendMessageParameters:
    """Parameters passed to send_message."""
    chat_id: Union[str, int]
    text: str
    parse_mode: ParseMode = ParseMode.HTML
    disable_web_page_preview: Optional[bool] = None
    disable_notification: Optional[bool] = None
    reply_to_message_id: Optional[int] = None
    reply_markup: Optional[ReplyMarkup] = None


@dataclass
class CustomMessageContext:
    """Context of the custom message."""
    bot: Bot
    update: Update


class ICustomMessageExtension:
    """Adds features to the custom message."""

    def pre_send(self, parameters: SendMessageParameters,
                 context: CustomMessageContext) -> None:
        """Is executed before send_message."""
        raise NotImplementedError()

    def post_send(self, message: Message, parameters: SendMessageParameters,
                  context: CustomMessageContext) -> None:
        """Is executed after send_message."""
        raise NotImplementedError()
