"""Module for custom messages."""

from typing import Any, Optional, List

from telegram import Bot, Update, Message

from gmb.telegram_util import replace_text

from .extension import (SendMessageParameters,
                        CustomMessageContext,
                        ICustomMessageExtension)
from .pager import PagerExtension
from .tidy_mode import TimedMessagesExtension


class ReplaceTextExtension(ICustomMessageExtension):
    """Extension that replaces parts of the text."""

    def pre_send(self, parameters: SendMessageParameters,
                 context: CustomMessageContext) -> None:
        parameters.text = replace_text(context.update, parameters.text)

    def post_send(self, message: Message, parameters: SendMessageParameters,
                  context: CustomMessageContext) -> None:
        pass


class CustomMessage:
    """A custom message is a message that has some more features."""

    def __init__(self, text: str, bot: Bot, update: Update, **extra: Any):
        extra["text"] = text
        if "chat_id" not in extra:
            extra["chat_id"] = update.effective_chat.id
        if "reply_to_message_id" not in extra:
            extra["reply_to_message_id"] = update.effective_message.message_id
        self.parameters = SendMessageParameters(**extra)
        self.context = CustomMessageContext(bot, update)
        # message
        self.message: Optional[Message] = None
        self.replacer = ReplaceTextExtension()
        self.pager = PagerExtension()
        self.timer = TimedMessagesExtension()
        self.extensions: List[ICustomMessageExtension] = [self.replacer, self.pager,
                                                          self.timer]

    def pre_send_methods(self):
        """Executes all pre send methods of the extensions."""
        for extension in self.extensions:
            extension.pre_send(self.parameters, self.context)

    def post_send_methods(self, message: Message):
        """Executes all post send methods of the extensions."""
        for extension in self.extensions:
            extension.post_send(message, self.parameters, self.context)

    def send(self):
        """Sends the tidy message."""
        self.pre_send_methods()
        self.message = self.context.bot.send_message(**self.parameters.__dict__)
        self.post_send_methods(self.message)
        return self.message
